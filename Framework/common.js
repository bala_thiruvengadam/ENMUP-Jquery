/**
 * customPagechange()
 * <function use to Back/change a page using History>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function goBack()
{   
    var activePage = $(".ui-page-active").attr("id");
	
    switch (activePage)
    {
        case "myMessagesPage":{
            $.mobile.changePage("#loggedInDashboardPage",{transition:"none"});
            break;
        }
        default :{
            history.back();
        }
    }   
}

/**
 * pageChange()
 * <function use to Back/change a page using History>
 * @param {string} _pageId Variable holding Page ID
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function pageChange(_pageId)
{   
    $.mobile.changePage("index.html#"+_pageId);
}


/**
 * doNothing()
 * <fn-description>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function doNothing()
{
}
/**
 * startActivitySpinner()
 * <function is use to Start Spinner>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function startActivitySpinner()
{
    
    openSpinnerNew();
}

/**
 * stopActivitySpinner()
 * <function is use to Stop Spinner>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function stopActivitySpinner()
{
    //$("#maskSpinner").fadeOut(2000);
    //$("#dialogSpinner").fadeOut(2000);
    $("#LoadingScreen").hide();
    
}


function hideSpinner()
{
    setTimeout(function(){
               $("#maskSpinner").hide();
               $("#dialogSpinner").hide();    
               
               },3000);
    
}

/**
 * generateTGT()
 * <fn-description>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */

function generateTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent)
{
    var url_server = serverName+'/cas/v1/tickets';
    var tempDCryptUname = _userNameEnc;
    var tempDCryptPwd =_passwordEnc;
    
    console.log("inside tgt"+url_server+" "+tempDCryptUname+" "+tempDCryptPwd);
    
    var xhr = $.ajax({
                     url : url_server,
                     type:"POST",
                     async:true,
                     cache:false,
                     data :({server:url_server,username:tempDCryptUname,password:tempDCryptPwd,math:Math.random()}),
                     beforeSend:function (){
                     
                     setTimeout(function()
                                {
                                xhr.abort();
                                },60000);
                     },
                     error:function(response)
                     {
                     console.log('E:Response Status '+response.status);
                     if(response.status == 500)
                     {alertServerError(response.status);}
                     },
                     success:function(data){
                     //console.log("Success"+JSON.stringify(data));
                     },
                     complete:function(response,textStatus){
                     if (response.status == 0)
                     {
                     //Handling done in error
                     if($.mobile.activePage.attr("id") == "loggedInDashboardPage") // fill the form the correct values
                     {
                     $.mobile.changePage("#loggedInPage");
                     if (window.localStorage.getItem("LS_RememberMe")== "true")
                     {
                     $("#loginUsername").val(tempDCryptUname);
                     $("#loginPassword").val("");
                     }
                     }
                     stopActivitySpinner();
                     }
                     
                     else
                     {
                     
                     if(textStatus == "success" && response.status == 201)
                     {
                     var str = response.getResponseHeader('Location');
                     tgtTicket = str.substr(str.lastIndexOf('/')+1);
                     console.log(tgtTicket);
                     window.sessionStorage.setItem("LS_tgtTicket",tgtTicket);
                     window.sessionStorage.setItem("LS_TryAutoLogin","true");
                     
                     
                     if(_userNameEnc != window.localStorage.getItem("LS_lastUsername"))
                     {
                     window.localStorage.removeItem("LS_usernameEncrypted");
                     window.localStorage.removeItem("LS_passwordEncrypted");
                     window.localStorage.removeItem("LS_firstTimeUser");
                     window.localStorage.removeItem("LS_lastAlertTS");
                     window.localStorage.removeItem("LS_soundAlert");
                     window.localStorage.removeItem("LS_vibrateAlert");
                     window.localStorage.removeItem("LS_autoRefreshTimer");
                     
                     }
                     
                     if (window.localStorage.getItem("LS_firstTimeUser") == null)
                     {
                     window.localStorage.setItem("LS_lastUsername", _userNameEnc); // Used to keep track of who logged in last
                     
                     if (window.localStorage.getItem("LS_RememberMe") == "true")
                     {
                     window.localStorage.setItem("LS_usernameEncrypted",_userNameEnc.toUpperCase());
                     window.localStorage.setItem("LS_passwordEncrypted",_passwordEnc);
                     }
                     else
                     {
                     usernameEncrypted = _userNameEnc;
                     passwordEncrypted = _passwordEnc;
                     window.localStorage.removeItem("LS_autoRefreshTimer");
                     autoRefreshTimer = 600000;
                     
                     }
                     
                     window.localStorage.setItem("LS_firstTimeUser", "no"); //
                     }
                     //generateServiceTicket(_usernameEnc, _passwordEnc, _silent);
                     if(service_Request != null)
                     {
                     generateST(_userNameEnc, _passwordEnc,service_Request,return_Where,_silent);
                     }
                     else
                     {
                     window.sessionStorage.setItem("_checkMsgCount",true);
                     roleBaseSecurity();
                     }
                     }
                     else
                     {
                     if($.mobile.activePage.attr("id")!= "loggedInDashboardPage" && $.mobile.activePage.attr("id")!= "loggedInPage")
                     {
                     if(!_silent)navigator.notification.alert("Oops! Authentication failed. Please login again to continue.",doNothing,'Error','Ok');
                     }
                     else
                     {
                     if($.mobile.activePage.attr("id") == "loggedInDashboardPage") // fill the form the correct values
                     {
                     $.mobile.changePage("#loggedInPage");
                     if (window.localStorage.getItem("LS_RememberMe")== "true")
                     {
                     $("#loginUsername").val(tempDCryptUname);
                     $("#loginPassword").val("");
                     }
                     }
                     
                     if ($.mobile.activePage.attr("id") == "loggedInPage") $("#loginPassword").val("");
                     if(!_silent)
                     {
                     /*if(response.status == 500) navigator.notification.alert("Oops! An internal server error has occured. Please try again later",doNothing,'Error','Ok');
                     else navigator.notification.alert("Oops! Can't log you in. Please check your login details.",doNothing,'Error','Ok');*/
                     }
                     }
                     
                     
                     stopActivitySpinner();
                     }
                     
                     }
                     }
                     });	
    
}


function generateMsgTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent)
{
    
    var url_server = serverNameMsg+'/cas/v1/tickets';
    // var key = device.uuid;
    var tempDCryptUname = _userNameEnc;
    var tempDCryptPwd = _passwordEnc;
    console.log(url_server+" "+tempDCryptUname+" "+tempDCryptPwd);
    console.log("inside tgt"+url_server);
    var xhr = $.ajax({
                     url : url_server,
                     type:"POST",
                     async:true,
                     data :({server:url_server,username:tempDCryptUname,password:tempDCryptPwd}),
                     beforeSend:function (){
                     // alert(globalAjaxTimer);
                     setTimeout(function()
                                {
                                xhr.abort();
                                },60000);
                     },
                     error:function(response)
                     {
                     console.log('E:Response Status '+JSON.stringify(response));
                     if(response.status == 500)
                     {alertServerError(response.status);}
                     
                     },
                     success:function(data){
                     console.log("msgLogin="+data);
                     //alert(data);
                     },
                     complete:function(response,textStatus){
                     //alert(response.status+" "+textStatus);
                     //console.log(JSON.stringify(response));
                     if (response.status == 0)
                     {
                     //Handling done in error
                     if($.mobile.activePage.attr("id") == "loggedInDashboardPage") // fill the form the correct values
                     {
                     $.mobile.changePage("#loggedInPage");
                     if (window.localStorage.getItem("LS_RememberMe")== "true")
                     {
                     $("#loginUsername").val(tempDCryptUname);
                     $("#loginPassword").val("");
                     }
                     }
                     }
                     
                     else
                     {
                     
                     if(textStatus == "success" && response.status == 201)
                     {
                     var str = response.getResponseHeader('Location');
                     MsgtgtTicket = str.substr(str.lastIndexOf('/')+1);
                     window.sessionStorage.setItem("LS_MsgtgtTicket",MsgtgtTicket);
                     window.sessionStorage.setItem("LS_TryAutoLogin","true");
                     
                     oldMsgs=0;
                    $(".msgcount").hide();
                     if(window.sessionStorage.getItem("_checkMsgCount") == "true")
                     {
                     url_service="/Alerts/AlertsService/AlertsInfoService/inbox";
                     return_Where=getAlertsJSON;
                     request_MsgST(url_service,return_Where,false);
                     }
                     
                     //TimeoutRefreshAlerts();
                     
                     if(window.localStorage.getItem("LS_autoRefreshTimer") == null)
                     {
                     msgInterval=setInterval(function(){TimeoutRefreshAlerts();},300000);
                     }
                     else{
                     msgInterval=setInterval(function(){TimeoutRefreshAlerts();},parseInt(window.localStorage.getItem("LS_autoRefreshTimer")));
                     }
                     
                     
                     
                     if(_userNameEnc != window.localStorage.getItem("LS_lastUsername"))
                     {
                     window.localStorage.removeItem("LS_usernameEncrypted");
                     window.localStorage.removeItem("LS_passwordEncrypted");
                     window.localStorage.removeItem("LS_firstTimeUser");
                     window.localStorage.removeItem("LS_lastAlertTS");
                     window.localStorage.removeItem("LS_soundAlert");
                     window.localStorage.removeItem("LS_vibrateAlert");
                     window.localStorage.removeItem("LS_autoRefreshTimer");
                     
                     }
                     
                     if (window.localStorage.getItem("LS_firstTimeUser") == null)
                     {
                     window.localStorage.setItem("LS_lastUsername", _userNameEnc); // Used to keep track of who logged in last
                     
                     if (window.localStorage.getItem("LS_RememberMe") == "true")
                     {
                     window.localStorage.setItem("LS_usernameEncrypted",_userNameEnc);
                     window.localStorage.setItem("LS_passwordEncrypted",_passwordEnc);
                     }
                     else
                     {
                     usernameEncrypted = _userNameEnc;
                     passwordEncrypted = _passwordEnc;
                     window.localStorage.removeItem("LS_autoRefreshTimer");
                     autoRefreshTimer = 600000;
                     
                     }
                     
                     window.localStorage.setItem("LS_firstTimeUser", "no"); //
                     }
                     
                     //generateServiceTicket(_usernameEnc, _passwordEnc, _silent);
                     if(service_Request != null)
                     {
                     generateST(_userNameEnc, _passwordEnc,service_Request,return_Where,_silent);
                     }
                     else
                     {
                     window.sessionStorage.setItem("_checkMsgCount",true);
                     WebServiceIconGenerate();
                     //roleBaseSecurity();
                     }
                     }
                     else
                     {
                     if($.mobile.activePage.attr("id")!= "loggedInDashboardPage" && $.mobile.activePage.attr("id")!= "loggedInPage")
                     {
                     if(!_silent)navigator.notification.alert("Oops! Authentication failed. Please login again to continue.",doNothing,'Error','Ok');
                     }
                     else
                     {
                     if($.mobile.activePage.attr("id") == "loggedInDashboardPage") // fill the form the correct values
                     {
                     $.mobile.changePage("#loggedInPage");
                     if (window.localStorage.getItem("LS_RememberMe")== "true")
                     {
                     $("#loginUsername").val(tempDCryptUname);
                     $("#loginPassword").val("");
                     }
                     }
                     
                     if ($.mobile.activePage.attr("id") == "loggedInPage") $("#loginPassword").val("");
                     if(!_silent)
                     {
                     /*if(response.status == 500) navigator.notification.alert("Oops! An internal server error has occured. Please try again later",doNothing,'Error','Ok');
                     else navigator.notification.alert("Oops! Can't log you in. Please check your login details.",doNothing,'Error','Ok');*/
                     }
                     }
                     
                     
                     stopActivitySpinner();
                     }
                     
                     }
                     }
                     });
    
    
}

/**
 * collegeInfoShow()
 * Function use to College Info Page 
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function collegeInfoShow()
{   
    $.mobile.changePage("#collegeinfo_Page",{transition:"slideup"});
    
}


/**
 * sendFeedbackEmail()
 * <function is use to sendMail>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */

function sendFeedbackEmail()
{
    var toRecipient = (UIFeedbackObject.FeedbackTo || "");
    var ccRecipient = (UIFeedbackObject.FeedbackCc || "");    
    
    var sub="myENMU Mobile Feedback";
    var body="\n\n\n\n\n\n\n\n\n"+device.platform+", O.S:"+device.version+", App version="+appVersion;
    sendMail(body,sub,toRecipient,ccRecipient,null,null); 
}

/**
 * sendMail()
 * <function is use to sendMail>
 * @param {string} _body Variable holding Message
 * @param {string} _subject Variable holding Subject
 * @param {string} _to Variable holding To Email Id
 * @param {string} _cc Variable holding Cc Email Id
 * @param {boolean} _html Variable holding Boolean
 * @param {string} _bcc Variable holding Bcc Email Id
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */


function sendMail(_body, _subject, _to, _cc, _html, _bcc)
{		
    var body = (_body||"");
    var subject = (_subject||"");
    var toRecipient = (_to||"");
    var ccRecipient = (_cc||"");
    var bccRecipient = (_bcc||"");
    var isHTML = (_html||false);
    
    var option={subject:subject,body:body,to:toRecipient,cc:ccRecipient}
    window.plugin.email.open({
                             to:      [toRecipient],
                             cc:      [ccRecipient],
                             bcc:     [bccRecipient],
                             subject: subject,
                             body:    body,
                             isHtml:  true
                             });
        
}


/**
 * listRefreshHelper()
 * <function is use to list referesh>
 * @param {string} _listID Variable holding Page Id
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function listRefreshHelper(_listID)
{
	$('#'+ _listID).each(function() 
                         {
                         $(this).listview();
                         $(this).listview("refresh");
                         }); 
}

/**
 * checkConnection()
 * <function is use to Check Connection>
 * @param {string} _silent Variable holding boolean
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function checkConnection(_silent)
{
    var silent = (_silent || false);    
    var networkState = navigator.network.connection.type;    
    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.NONE]     = 'No network connection';
    
    if (silent)
    {
		// Do Nothing
    }
    else 
    {
        if(states[networkState]==states[Connection.NONE] || states[networkState]==states[Connection.UNKNOWN])
        {
            navigator.notification.alert("Oops! You are not connected to Internet. Please check your settings and try again",doNothing,'No Network','Ok');
        }
        else
         {
         navigator.notification.alert("Oops! Our Web Service seems to be down. Please try again later.",doNothing,'Oops!','Ok');
         }
    }
    
	stopActivitySpinner();    
}

/**
 * openWebpage()
 * <function is use to Open Web Page Within App>
 * @param {string} _url Variable holding URL
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */
function openWebpage(_url)
{
      try
    {
    var checkNetwork = navigator.connection.type;
    if(checkNetwork != "none" && checkNetwork != "unknown")
    { 
        window.open(_url, '_blank');
    }
    else
    {
    navigator.notification.alert("Oops! You are not connected to Internet. Please check your settings and try again",doNothing,'No Network','Ok');
    stopActivitySpinner();
    }
    }catch(e){}
    
}
    
    

function alertServerError(_status) {
    
    var status = (_status || false);
    switch(status)
    {
        case 0:
        {
            checkConnection();
            break;
        }
        case 500:
        {
            checkConnection();
            break;
        }
        default:
        {
            navigator.notification.alert("We seem to be unable to connect to MyENMU right now. Please try again later.",doNothing,'Oops!','Ok');
        }
    }
    
    stopActivitySpinner(); 
}


function openSpinnerNew(_forceXposValue)
{
    
   var xPosOrig = (_forceXposValue || false);
    
    var id = $("#dialogSpinner");
    var maskHeight = $(document).height();
    var maskWidth  = $(document).width();
    $("#maskSpinner").css({"height":maskHeight,"width":maskWidth});
    $("#maskSpinner").fadeIn(100);
    $("#maskSpinner").fadeTo("slow",0.5);
    $("#maskSpinner").show();
    
    //Postion Control
    //var dialogSpinnerTop = ($(document).height()/2-40)
    //$(id).css({'top':$(window).scrollTop()+dialogSpinnerTop});
    if(!xPosOrig) {$(id).css({'top':$(window).scrollTop()+195});}
    else{$(id).css({'top':_forceXposValue});}
        
    $(id).css({'left':$(window).width()/2-125+'px'});

    
    $(id).fadeIn(200);
    $("#LoadingScreen").show();
	
}


function formatCurrency(num) {
try{num = num.toString().replace(/\$|\,/g,'');
if(isNaN(num))
num = "0";
sign = (num == (num = Math.abs(num)));
num = Math.floor(num*100+0.50000000001);
cents = num%100;
num = Math.floor(num/100).toString();
if(cents<10)
cents = "0" + cents;
for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
num = num.substring(0,num.length-(4*i+3))+','+
num.substring(num.length-(4*i+3));
return (((sign)?'':'-') + '$' + num + '.' + cents);
}catch(e){}
}

