/**
 * Child browser helper.
 * Opens up a childbrowser link within the application.
 * Options success & error callback (if the childbrowser object needs to be monitored)
 * options;{
 *            url:"string",
 *            location_change:callback,
 *            on_close:callback,
 *            on_external:callback
 *          }
 */

//Store all PG related objects here
var App=App || {};
App.phoneGap = App.phoneGap || {};
App.phoneGap.ChildBrowserOpen = App.phoneGap.ChildBrowserOpen || false;


//App.phoneGap.childBrowser will hold the instantiated object

var PG_childBrowser = function(options) {

  var url, //holds the url to be opened
  onCbLocChange, // function thats called when url changes in cb
  onCbClose, // function thats called when the child browser closes,
  onCbExternal;
  // function called when safari is invoked from CB

  //Install CB if needed
  if(!App.phoneGap.childBrowser) {
    App.phoneGap.childBrowser = ChildBrowser.install();
  }

  //Set options hash correctly
  if(!options) {var options = {}}
  url = options.url || "www.google.com";
  onCbLocChange = options.location_change ||function() {};
  onCbClose = options.on_close ||function() { App.phoneGap.ChildBrowserOpen=false; };
  onCbExternal = options.on_external ||function() { App.phoneGap.ChildBrowserOpen=false;};

  //Open & link CB object
  if(App.phoneGap.childBrowser != null) {
    App.phoneGap.childBrowser.onLocationChange = onCbLocChange;
    App.phoneGap.childBrowser.onClose = onCbClose;
    App.phoneGap.childBrowser.onOpenExternal = onCbExternal;
      if(!App.phoneGap.ChildBrowserOpen)
      {
          App.phoneGap.ChildBrowserOpen=true;
       
          window.plugins.childBrowser.showWebPage(url);
          
          
  }
      else
      {
        
                
      }
}

  return this;
}