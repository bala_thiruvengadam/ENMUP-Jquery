/**
 * @fileoverview This file contains all the function calls related to the core application functionality
 */

/* Documentation Example */

/**
 * <fn-name>
 * <fn-description>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 */



/**
 * Global Varibales List
 * All the application variables are listed here.
 */

/**
 * @fileoverview This file contains all the function calls related to the core application functionality
 */


var statusBarTile;
var statusBarMessage;

var client_browser; //Used for opening safari browser in UI Webview

var alertsJSON; // Holds the current message JSON
var tgtTicket; // Holds the current TGT
var usernameEncrypted ; // Holds the ECrypted username
var passwordEncrypted ; // Holds the ECrypted password
var id_req ; //Holds the individual alert id
var lastAlertTS; // Holds the value of last time stamp
var autoRefreshTimer = 600000; // Default Value of Auto Refresh Timer. Will always come into play unless LS is set.

var timer;
var firstTimeSupress = true; // used for supressing autorefresh, for the first time
var isApplicationForeground = true; // Flag that tells other application functions whether application is inBackground or Foreground. /* Runs with a delay of 500ms so for Verbose functions check only after 500 ms of app being resumed,*/
var globalAjaxTimer = 35000; // COntrols the time the Ajax call runs for. Given a reasonable value to prevent spinner from going on for a long time in case of server trickle
var groupName; //name of community selected
//var  userHasLoggedIn = 'Yes';

var url_service;
var return_Where;
var iconScroll;
var loginiconScroll;
var bannericonScroll;
var FaviconScroll;
var LoggedObject;
var googleAnalytics = null;
var rolesArray=new Array();
var taphold=false;
var lognavCount=1;
var lastPage="mainDashboardPage";
var previousPage;
var directoryentries=new Array();
var currentLocation;
var currentLocation;
var watchID;
var directionsDisplay;
var viewMarkers=false;
var mapMarkers =[];
var markersArray = [];
var checkLocationFlag=false;
var usertravelMode="WALKING";
var classroomAddDataTicketCount = 0;
var serviceTicketCount = 0;

var CurrentMonth=new Array(12);
CurrentMonth[0]="January";
CurrentMonth[1]="Febuary";
CurrentMonth[2]="March";
CurrentMonth[3]="April";
CurrentMonth[4]="May";
CurrentMonth[5]="June";
CurrentMonth[6]="July";
CurrentMonth[7]="August";
CurrentMonth[8]="September";
CurrentMonth[9]="October";
CurrentMonth[10]="November";
CurrentMonth[11]="December";

/*var wkday=new Array(7);
weekday[0]="Sunday";
weekday[1]="Monday";
weekday[2]="Tuesday";
weekday[3]="Wednesday";
weekday[4]="Thursday";
weekday[5]="Friday";
weekday[6]="Saturday";*/

var CalEvententries;
var activeDayId=null;
var prevDayId=null;
var tdyDate;
var daysToNextMonth;
var daysToPrevMonth;
var NextMonthCounter=0;
var PrevMonthCounter=0;
var myMsgs;
var msgClick=false;
var msgInterval;
var oldMsgs=0;
var newMsgs;

$("a").live("click",function(){
var href = $(this).attr('href');
if(href.indexOf("tel:") != -1)
{
}
else{
return false;
}
});


$("#msg_Content a").live("click",function(){
            var href = $(this).attr('href');
                         if(href.indexOf('mail') != -1){
                         window.open(href,'_system');
                         }else{
                         openWebpage(href);
                         }
                         
            });

$("#Indivisualarticles_News_Display a").live("click",function(){
                         var href = $(this).attr('href');
                                             if(href.indexOf('mail') != -1){
                                             window.open(href,'_system');
                                             }else{
                                             openWebpage(href);
                                             }
                         });

$("#event_details_container a").live("click",function(){
                                             var href = $(this).attr('href');
                                     if(href.indexOf('mail') != -1){
                                     window.open(href,'_system');
                                     }else{
                                     openWebpage(href);
                                     }
                                             });

/**
 * Global Variables List for Local Storage.
 * All the local Storage variables are listed here.
 */

/**
 * Global Event Handler List
 * All the application events are listed here
 */

//window.localStorage.removeItem("favInfo");


$("#acknow_test").live("click",function()
                       {                       
                       navigator.notification.confirm('Are you sure you want to acknowledge?',acknowledgeConfirm,'Acknowledge',['Ok','Cancel']);
                       });

$(".logoutclass").live("click",function()
                       {
                       if(myCampusObject.settings.myCampusAppsBuild == true)
                       {
                       navigator.notification.confirm('Are you sure you want to log out?',checkLogout,'Logout',['Yes','No']);
                       }
                       
                       
                       });

$("#alertsRefresh").live("click",function()
                         {
                         startActivitySpinner();
                         refreshAlerts(false);
                         });

$('#notification').live('click',function(){
                        if($('#notification').is(':checked')) 
                        {
                        
                        notification_Control("true");
                        }
                        else
                        {
                        
                        notification_Control("false");
                        }
                        });

$("#settingsDiv").live('click',function(){
                              
                              // check_push_status();
                               
                               });

document.addEventListener("backbutton", backButtonHandler, false);


/*<CheckCollegeBuild>
 *<Used to show and hide server listview>
 */

function CheckCollegeBuild()
{
    if(myCampusObject.settings.isCollegeBuild == false)
	{        
        $("#serverServicelistview").show();
    }
    else
    {
        $("#campusEaiLogo").addClass("campuseailogoclass");
        $("#serverServicelistview").hide();
    }
}


/*
 <Controls the page events on loading a page>
 
 
 */
/*
 <loaded()>
 <apply iscoll object on the logged icons>
 */
document.addEventListener('DOMContentLoaded', loaded, false);

function loaded() {
	iconScroll = new iScroll('wrapper', {
                             snap: true,
                             checkDomChanges:true,
                             momentum: false,
                             hScrollbar: false,
                             onScrollEnd: function () {
                             document.querySelector('#indicator > li.active').className = '';
                             document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
                             }
                             });
    
}





$( ":jqmData(role='page')").live( "pageshow", function(event) 
                                 {
                                 try{ if(myCampusObject.settings.iphone)
                                 {
                                 var a = location.hash.replace("#", "/");
                                 if ((googleAnalytics != null) && a != "") {
                                 googleAnalytics.trackPageview(a);
                                 
                                 }
                                 }
                                 }catch(e){}
                                 try{ if(myCampusObject.settings.androidphone)
                                 {
                                 var a = location.hash.replace("#", "/");
                                 if (a != "") {
                                 
                                 window.plugins.analytics.trackPageView(a);
                                 }
                                 }
                                 }catch(e){}
                                 
                                 
                                 
                                 var activepageid=$(".ui-page-active").attr("id"); 
                                 previousPage=lastPage;
                                 lastPage=activepageid;
                                 $("footer-icon").removeClass("footer-tab-selected");                                 
                                 //alert(activepageid);
                                 switch (activepageid)
                                 {
                                 case "mainDashboardPage":{
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 setTimeout(function(){ iconScroll.refresh();});
                                 
                                 break;
                                 }
                                 case "emergency_Numbers":{
                                // document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }
                                 case "directory_listing":{ 
                                 $("#searchdiectory").val("");
                                 $("#searchdiectory").focus(); 
                                 break;
                                 }
                                 case "mapPage":{
                                 var headerH=$(this).find( ":jqmData(role='header')").height() + 2;
                                 var documentH=$(document).height();
                                 $("#map-canvas").css("height",(documentH-headerH));
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }
                                 case "collegeinfo_Page":{
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 } 
                                 case "demoPage":{
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 } 
                                 case "radioPage":{
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 } 
                                 case "loggedInDashboardPage":{ 
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 setTimeout(function(){ loginiconScroll.refresh(); });
                                 
                                 break;
                                 }
                                 case "favorites":{ //$(this).find(".footer-icon-fav").addClass("footer-tab-selected"); 
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 setTimeout(function(){ FaviconScroll.refresh(); });
                                 
                                 
                                 break;
                                 }
                                 case "myMessagesPage":{ $(this).find(".footer-icon-msg").addClass("footer-tab-selected");
                                 
                                 break;
                                 }
                                 case "settings":{ 
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }                                 
                                 case "loggedInPage":
                                 {
                                 window.sessionStorage.setItem("LS_TryAutoLogin","false");
                                 document.addEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }  
                                 }
                                 });

/*
 <Controls the page events on unloading a page>
 
 
 */

$( ":jqmData(role='page')").live( "pagebeforehide", function(event, ui) 
                                 {   
                                 var activepageid=$(".ui-page-active").attr("id");                                                            
                                 switch(activepageid)                                 
                                 {
                                 case "mainDashboardPage":{
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }
                                 case "emergency_Numbers":{
                                // document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }
                                 case "mapPage":{
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 if(watchID){
                                 window.clearInterval(watchID);}
                                 break;
                                 }
                                 case "loggedInDashboardPage":{
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 
                                 break;
                                 }
                                 case "LaunchPad":{
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 } 
                                 case "favorites":{
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }  
                                 case "loggedInPage":
                                 {
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }  
                                 case "collegeinfo_Page":
                                 {
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 }  
                                 case "myMessagesPage":
                                 {
                                 msgClick = false;
                                 window.sessionStorage.setItem("_checkMsgCount", true);
                                 break;
                                 }  
                                 case "radioPage":
                                 {
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 break;
                                 } 
                                 case "settings":{ 
                                 document.removeEventListener("touchmove",preventDefaultBehavior,false);
                                 
                                 break;
                                 } 
                                 default :
                                 {
                                 }
                                 }                                 
                                 });

/*
 Prevents the default behavior
 */
function preventDefaultBehavior(e)
{
    e.preventDefault()
}



$(document).ready(function(){
                  $("#mapRadio").find("input[type='radio']").each(function(){
                                                                  
                                                                  $(this).change(function(){
                                                                                 $("#mapRadio").find("input[type='radio']").attr("checked",false).checkboxradio("refresh");
                                                                                 $(this).attr("checked",true).checkboxradio("refresh");
                                                                                 switch(this.id){
                                                                                 case "walk":{
                                                                                 getDirection(locationLat,locationLong,"WALKING");
                                                                                 break;}
                                                                                 case "cycle":{
                                                                                 getDirection(locationLat,locationLong,"BICYCLING");
                                                                                 
                                                                                 break;}
                                                                                 case "drive":{
                                                                                 getDirection(locationLat,locationLong,"DRIVING");
                                                                                 
                                                                                 break;}
                                                                                 }
                                                                                 //makeDirectionMarker=false;
                                                                                 });
                                                                  
                                                                  });
});

/**
 * backButtonHandler()
 * <function is use to back Button Handling>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {boolean}
 *
 
 }*/
function backButtonHandler()
{
	var activePage = $(".ui-page-active").attr("id");
	
    
    switch (activePage)
    {
        case "collegeinfo_Page" :{
            if(window.sessionStorage.getItem("LS_tgtTicket") != null)
            {
                $.mobile.changePage("#loggedInDashboardPage",{transition:"slidedown"});
            }
            else if(previousPage == "loggedInPage")
            {
                $.mobile.changePage("#loggedInPage",{transition:"slidedown"});
            }
            else
            {
                $.mobile.changePage("#mainDashboardPage",{transition:"slidedown"});
            }
            break;
        }
        case "loggedInDashboardPage":{
            if(myCampusObject.settings.myCampusAppsBuild == true)
            {
                navigator.notification.confirm('Are you sure you want to log out?',checkLogout,'Log Out',['Yes','No']);
            }
            break;
        }
        case "mainDashboardPage":{
      			navigator.notification.confirm("Are you sure you want to exit?",exitHandler,'Just confirming..',['Yes','No']);
      			break;
        }
        case "myMessagesPage":{            
      			$.mobile.changePage("#loggedInDashboardPage",{transition:"none"});
      			break;
      	} 
      	case "set_emergency_contacts":{            
            $.mobile.changePage("#emergency_contacts",{transition:"none"});
            break;
        }
        case "emergency_contacts":{            
            $.mobile.changePage("#loggedInDashboardPage",{transition:"none"});
            break;
        }
        case "setAddress":{            
            $.mobile.changePage("#addressDisplay",{transition:"none"});
            break;
        }
        case "addressDisplay":{            
            $.mobile.changePage("#loggedInDashboardPage",{transition:"none"});
            break;
        } 
        default :{
            history.back();
        }
    }
    previousPage=lastPage;
    lastPage=activePage;
    
}
/*<exitHandler()>
 <exitHandler controls the exit from the app>
 */


function exitHandler(Button)
{
    
    if(Button == 1)
    {
        navigator.app.exitApp();
    }
}	

if(localStorage['favInfo']==null){
    
    
    var storedData = {items: []};
    
    
    
}



function NASort(a, b) {
    if (a.innerHTML == 'NA') {
        return 1;
    }
    else if (b.innerHTML == 'NA') {
        return -1;
    }
    return (a.innerHTML > b.innerHTML) ? 1 : -1;
};


