var lengthCal;
var countryUSA = '157';   //here's you set usa country code to enable disable states ->in case mycampuseai use- 157
var countryCanada = '27';   //set country code for canada country to set zip code regex ->in case mycampuseai use- 27
var classroomAddDataTicketCount = 0;
var serviceTicketCount = 0;
var regTermCode; //TODO Need to find other way to assign term code
var bannerAppAutoCompleter=false;
/**
 *to check postal code against canada
 */
function isValidPostalCode(postalCode, countryCode) {
    
    switch (countryCode) {
        case countryUSA:
            postalCodeRegex = /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/;
            break;
        case countryCanada:
            postalCodeRegex = /^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$/;
            break;
        default:
            postalCodeRegex = /^(?:[A-Z0-9]+([- ]?[A-Z0-9]+)*)?$/;
    }
    return postalCodeRegex.test(postalCode);
}