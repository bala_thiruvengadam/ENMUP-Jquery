/**
 * onPreLoginBodyLoad()
 * <This Fun is use to Deviceready>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message
 * @return {boolean}
 */

function onPreLoginBodyLoad()
{
    
    if(myCampusObject.settings.myCampusAppsBuild == true)
    {
        document.addEventListener("deviceready", onPreLoginDeviceReady, false);
    }
}
/**
 * onPreLoginDeviceReady()
 * <checks the device platform and make changes in the app>
 */
var wrapperWidth,wrapperHeight;
function onPreLoginDeviceReady()
{
    //startSpinner();
    wrapperWidth=parseInt($(document).width()*0.92);
    wrapperHeight=parseInt($(document).height()*0.69);
    var devicePlatform = device.platform;
    var txtBox = document.getElementById("searchdiectory");
    txtBox.onkeydown=DisplayKeyCode;
    
    
    $('#locationList').change(function()
                              {
                              //alert("here");
                              if($(this).val() == 'sel'){}else{
                              var latLong=($(this).val().split(","));
                              checkPosition(latLong[0],latLong[1]);
                              }
                              });
    var mapLocations="";
    
    
    $.each(mapList, function(key,val)
           {
           mapLocations +="<option value="+val.lat+","+val.long+">"+val.desc+"</option>";
           });
    
    $("#locationList").html(mapLocations);
    
    $('#locationList option').sort(NASort).appendTo('#locationList');
    
    setTimeout(function() {
               $("#locationList").prepend("<option value='sel'>Select a building</option>");
               },2000);
    
    
    
    $("#loginUsername").focusin(function(){
                                
                                $(".clg-info-slider").hide();
                                });
    
    $("#loginUsername").focusout(function(){
                                 
                                 $(".clg-info-slider").show();
                                 });
    
    $("#loginPassword").focusin(function(){
                                $(".clg-info-slider").hide();
                                });
    
    $("#loginPassword").focusout(function(){
                                 $(".clg-info-slider").show();
                                 });
    
    switch (devicePlatform)
    {
            
        case "Android":{
            
            myCampusObject.settings.androidphone=true;
            $(".custom-button").hide();
            try{ var myGoogleAnalyticsAccountId ="UA-33747260-1"; // Get your account id from http://www.google.com/analytics/
                window.plugins.analytics.start(myGoogleAnalyticsAccountId);
            }catch(e){}
            break;
        }
        case "iOS":{
            StatusBar.overlaysWebView(false);
            StatusBar.backgroundColorByHexString("#000000");
            myCampusObject.settings.iphone=true;
            setTimeout(function() {
                       navigator.splashscreen.hide();
                       },2000);
            var childBrowser;
            //childBrowser = ChildBrowser.install();
            
            try{
                googleAnalytics = window.plugins.googleAnalyticsPlugin;
                googleAnalytics.startTrackerWithAccountID("UA-33747260-1");//initiate google analytic
                
            }catch(e){}
            break;
        }
            
        case "BlackBerry":{
            
            myCampusObject.settings.blackberry=true;
            break;
        }
    }
    
    
    preLoginDasdboardIcon();
    
    CheckCollegeBuild();
    preCheckAutoLogin();
    IndexPageChanges();
    
    
    
    //$(".msgcount").html(window.localStorage.getItem("LS_total_Msg"));
    $(".headerH1Class").html(collegeName);
    $("#universityName").html("<b>"+universityName+"</b>");
    
    
}

function DisplayKeyCode(e)
{
    
    if(e.keyCode == 13)
    {
        searchHandler('searchdiectory')
    }
    //document.getElementById("searchdiectory").();
}

/**
 * preCheckAutoLogin()
 * <This Fun is used to Check autologin>
 */
function preCheckAutoLogin()
{
    var mapLocations="";
    /*$.each(mapList, function(key,val)
     {
     mapLocations +="<option value="+val.lat+","+val.long+">"+val.desc+"</option>";
     });
     
     $("#locationList").append(mapLocations);*/
    if( window.localStorage.getItem("LS_RememberMe")=="true" && window.localStorage.getItem("LS_usernameEncrypted")!=null)
    {
        if(window.sessionStorage.getItem("LS_tgtTicket")!=null)
        {
            stopActivitySpinner();
            
        }
        else
        {
            if(window.sessionStorage.getItem("LS_TryAutoLogin")=="false")
            {
                
            }
            else{
                
                CheckAutoLogin();
            }
        }
    }
    else
    {
        stopActivitySpinner();
        
    }
    
}

/**
 * CheckAutoLogin()
 * <Auto login the user>
 * return { none }
 */
function CheckAutoLogin()
{
    if(myCampusObject.settings.isCollegeBuild == false)
    {
        if (window.localStorage.getItem("LS_serviceDetails") != null && window.localStorage.getItem("LS_serverDetails") != null)
        {
            serviceName = window.localStorage.getItem("LS_serviceDetails");
            serverName = window.localStorage.getItem("LS_serverDetails");
            $("#LoginServiceUrl").val(serviceName);
            $("#LoginServerUrl").val(serverName);
        }
    }
    if( window.localStorage.getItem("LS_RememberMe")=="true" && window.localStorage.getItem("LS_usernameEncrypted")!=null)
    {
        var tempECryptUname = window.localStorage.getItem("LS_usernameEncrypted");
        var tempECryptPwd = window.localStorage.getItem("LS_passwordEncrypted");
        
        startActivitySpinner();
        
        
        
        if(window.sessionStorage.getItem("LS_tgtTicket") != null)
        {
            
            roleBaseSecurity();
        }
        else
            
        {
            generateTGT(tempECryptUname,tempECryptPwd,null,null,false);
            //generateMsgTGT(tempECryptUname,tempECryptPwd,null,null,false);
        }
    }
    else
    {
        
        if(window.sessionStorage.getItem("LS_tgtTicket") != null)
        {
            
            roleBaseSecurity();
        }
        else
        {
            
            
            $.mobile.changePage("#loggedInPage");
            
        }
        stopActivitySpinner();
    }
}

/**
 * checkLogin()
 * <Checks the credentials of the users input and login the user if valid>
 * return { none }
 */
function checkLogin()
{
    
    if ($("#loginUsername").val().length == 0)
    {
        navigator.notification.alert('Please enter your username.',doNothing,'Username Required','Ok');
        return false;
    }
    if ($("#loginPassword").val().length == 0)
    {
        navigator.notification.alert('Please enter your password.',doNothing,'Password Required','Ok');
        return false;
    }
    
    if($("#loginRememberMe").is(":checked"))
    {
        window.localStorage.setItem("LS_RememberMe","true");
    }
    else
    {
        window.localStorage.setItem("LS_RememberMe","false");
    }
    
    
    setTimeout(function(){startActivitySpinner();},500);
    var key = device.uuid;
    var tempECryptUname = $("#loginUsername").val();
    var tempECryptPwd = $("#loginPassword").val();
    var silent = false;
    
    generateTGT(tempECryptUname,tempECryptPwd,null,null,null,silent);
    //generateMsgTGT(tempECryptUname,tempECryptPwd,null,null,false);
    
}

/**
 * preloginChange()
 * <changes the screen to loginpage>
 */

function preloginChange()
{
$.mobile.changePage("#loggedInPage");    
}


/**
 * IconGenerateWebService()
 * <Displays all the icons after the user successfully logs in>
 */


function IconGenerateWebService()
{
    // console.log("inside icon web service")
    url_service="/QuickLaunch/api/list/applications";
    return_Where=FetchIconUrl;
    request_ST(url_service,return_Where,false);
}


/**
 * preLoggedinPageChange()
 * <Controls the click event of the prelogin services>
 * @param {string} _pageID Variable holding Page ID
 * @param {string} _servername name of the server
 * @return {boolean}
 */
function preLoggedinPageChange(_servername, _pageID)
{
    if(_pageID == "Emergency")
    {
        displayEmergencyPage(_servername);
    }
    else{
        
        var networkState = navigator.connection.type;
        //networkState="wifi";
        if(networkState!="none" && networkState!="unknown")
            
            
        {
            switch(_pageID)
            {
                case "ImpEmergency":{
                    displayImpEmergencyPage(_servername);
                    break;
                }
                case "Courses":{
                    CoursesList(_servername);
                    $("#coursesmiddlebox").show();
                    $("#courses").hide();
                    break;
                }
                case "Events":{
                    getCalender(_servername);
                    break;
                }
                case "News":{
                    GetNewsList(_servername);
                    break;
                }
                case "Directory":{
                    $("#searchdiectory").val();
                    collegeDirectory(_servername);
                    $("#directorymiddlebox").show();
                    $("#directory").hide();
                    break;
                }
                case "Maps":{
                    mapinitialize();
                    break;
                }
                case "Videos":{
                    //openWebpage(_servername);
                    //alert("in case");
                    startActivitySpinner();
                    youtubeInit();
                    break;
                }
                case "Facebook":{
                    //Facebook.init();
                    openWebpage(_servername);
                    break;
                }
                case "Twitter":{
                    openWebpage(_servername);
                    break;}
                case "athletic":{
                    openWebpage(_servername);
                    break;
                }
                case "catalog":{
                    openWebpage(_servername);
                    
                    break;
                }
                case "flickr":{
                    getAlbums();
                    break;
                }
                case "Donate":{
                    openWebpage(_servername);
                    break;
                }
                    
                case "Parking":{
                    openWebpage(_servername);
                    break;
                }
                case "Shuttles":{
                    openWebpage(_servername);
                    break;
                }
                case "Dining":{
                    openWebpage(_servername);
                    break;
                }
                case "Library":{
                    openWebpage(_servername);
                    break;
                }
                case "Tour":{
                    if(myCampusObject.settings.iphone)
                    {
                        pageChange("CampusTour");
                        $("#campusYouTube").html("<div style='-webkit-border-radius:10px'><iframe style='-webkit-border-radius:10px' width='290' height='385' src='http://www.youtube.com/embed/MlM_Q34fdRc' frameborder='0' allowfullscreen></iframe></div>");
                    }
                    if(myCampusObject.settings.androidphone)
                    {
                        try{VideoPlayer.prototype.play("http://www.youtube.com/watch?v=MlM_Q34fdRc");}catch(e){}
                    }
                    break;}
                case "submit":{
                    openWebpage(_servername);
                    break;}
                case "deposit":{
                    openWebpage(_servername);
                    break;}
                default :{
                    //pageChange("demoPage");
                }
            }
        }
        else
        {
            navigator.notification.alert("Oops! You are not connected to Internet. Please check your settings and try again",doNothing,'No Network','Ok');
            
        }
    }
}


/**
 * appRaterCall()
 * <function handles the result of the user Rate>
 * @param {string} result Variable holding json
 * @param {string} strMsg Error Message
 * @return {boolean}
 */
function appRaterCall()
{
    if(myCampusObject.settings.iphone)
    {
        window.plugins.RatePlugin.promptRating();
        
    }
    if(myCampusObject.settings.androidphone)
    {
        window.plugins.AppRaterPlugin.start();
    }
    if(myCampusObject.settings.blackberry)
    {
        
    }
}

/**
 * searchHandler()
 * <function handles the Search Events>
 * @param {string} searchTextboxID Variable holding search Query Text
 * @param {string} strMsg Error Message
 * @return {none}
 */
function searchHandler(searchTextboxID)
{
    var querytext = $("#"+searchTextboxID).val();
    
    switch(searchTextboxID)
    {
        case "searchdiectory":{
            directorySearch(querytext);
            break;
        }
        case "searchCourses":{
            CourseSearch(querytext);
        }
        case "searcheventCalender":{
            getSearchEvents(querytext,10,1);
            break;
        }
        case "searcheventList":{
            getSearchEvents(querytext,10,1);
            break;
        }
        case "searchschools":{
            getSchoolSearch(querytext);
            break;
        }
        case "searchDepartments":{
            getDepartmentSearch(querytext,querytext);
            break;
        }
        case "searchrelevantcourse":{
            getCourseSectionSearch(querytext);
            break;
        }
        case "searchrelevantcourseSearch":{
            getCourseSectionSearch(querytext);
            break;
        }
        default :{
            
        }
    }
    
}


/**
 * addToBookmarksConfirm()
 * <function is use to Bookmarks>
 * @param {string} _subjectName Variable holding Subject Name
 * @param {string} _subjectCode Variable holding Subject Code
 * @param {string} _courseTitle Variable holding Course Title
 * @param {string} _class_section Variable holding Class Section
 * @param {string} _term_code Variable holding Term Code
 * @param {string} _servername name of the server
 * @return {none}
 */
function addToBookmarksConfirm(_subjectName, _subjectCode, _courseTitle, _class_section, _term_code, _servername)
{
    
    var subjectCode=_subjectCode;
    var flag = 0;
    
    if(localStorage["bookmarksListObjects"]!=null)
    {
        var storedData=JSON.parse(localStorage["bookmarksListObjects"]);
        /*checking existing bookmarks starts*/
        for(i=0;i<storedData.bookmarksitems.length;i++)
        {
            if(storedData.bookmarksitems[i].subject_code==subjectCode)
            {
                flag = 1;
                var pos = i;
                break;
            }
        }
    }
    else
    {
        var storedData = {"bookmarksitems": []};
        localStorage.setItem("bookmarksListObjects",JSON.stringify(storedData));
        
    }
    /*checking existing Bookmarks page ends*/
    if(flag==0)
    {
        sessionStorage["subjectName"] = _subjectName;
        sessionStorage["subjectCode"] = _subjectCode;
        sessionStorage["courseTitle"] = _courseTitle;
        sessionStorage["class_section"] = _class_section;
        sessionStorage["term_code"] = _term_code;
        sessionStorage["servername"] = _servername;
        
        navigator.notification.confirm("Are you sure you want to add this to Bookmarks?",addToBookmarks,'confirm',['Yes','No'])
    }
    else if(flag==1)
    {
        sessionStorage["pos"] = pos;
        navigator.notification.confirm("Are you sure you want to remove it from Bookmarks?",removeToBookmarks,'confirm',['Yes','No'])
    }
}

/**
 * addToBookmarks()
 * <function is Adding Bookmarks on local storage>
 * @param {string} button var is holding true/false
 * @return {boolean}
 */

function addToBookmarks(button)
{
    if(button==1)
    {
        var _subjectName = sessionStorage["subjectName"];
        var _subjectCode = sessionStorage["subjectCode"];
        var _courseTitle = sessionStorage["courseTitle"];
        var _class_section = sessionStorage["class_section"];
        var _term_code = sessionStorage["term_code"];
        var _servername = sessionStorage["servername"];
        
        var storedData=JSON.parse(localStorage["bookmarksListObjects"]);
        storedData.bookmarksitems.push({"subject_name": _subjectName, "subject_code": _subjectCode, "course_title": _courseTitle, "class_section": _class_section, "term_code": _term_code, "servername": _servername});
        localStorage.setItem("bookmarksListObjects",JSON.stringify(storedData));
        sessionStorage.clear();
        $("#plusBookmarkShow").attr("src", "images/bookmarkremove.png");
        
    }
}

/**
 * removeToBookmarks()
 * <function is Removing Bookmarks from local storage>
 * @param {int} button var is holding true/false
 * @return {boolean}
 */

function removeToBookmarks(button)
{
    if(button==1)
    {
        var storedData=JSON.parse(localStorage["bookmarksListObjects"]);
        storedData.bookmarksitems.splice(sessionStorage["pos"], 1);
        localStorage.setItem("bookmarksListObjects",JSON.stringify(storedData));
        sessionStorage.clear();
        $("#plusBookmarkShow").attr("src", "images/bookmarkplus.png");
        $("#plusBookmarkShow .ui-icon").addClass("ui-icon-plus").removeClass("ui-icon-minus");
    }
}

/**
 * addToBookmarksConfirmInfo()
 * <function is use to Bookmarks>
 * @param {string} _subjectName Variable holding Subject Name
 * @param {string} _subjectCode Variable holding Subject Code
 * @param {string} _courseTitle Variable holding Course Title
 * @param {string} _class_section Variable holding Class Section
 * @param {string} _term_code Variable holding Term Code
 * @param {Int} _position Variable holding Position
 * @return {none}
 */
function addToBookmarksConfirmInfo(_subjectName, _subjectCode, _courseTitle, _class_section, _term_code, _position)
{
    var subjectCode=_subjectCode;
    var position = _position;
    var flag = 0;
    
    if(localStorage["bookmarksListObjectsInfo"]!=null)
    {
        var storedDataInfo=JSON.parse(localStorage["bookmarksListObjectsInfo"]);
        /*checking existing bookmarks starts*/
        for(i=0;i<storedDataInfo.bookmarksitemsInfo.length;i++)
        {
            if(storedDataInfo.bookmarksitemsInfo[i].subject_code==subjectCode)
            {
                if(storedDataInfo.bookmarksitemsInfo[i].position == position)
                {
                    flag = 1;
                    var pos = i;
                    break;
                }
            }
        }
    }
    else
    {
        var storedDataInfo = {"bookmarksitemsInfo": []};
        //storedDataInfo.bookmarksitemsInfo.push({"subject_name": _subjectName, "subject_code": _subjectCode, "course_title": _courseTitle, "class_section": _class_section, "term_code": _term_code,"position":_position});
        localStorage.setItem("bookmarksListObjectsInfo",JSON.stringify(storedDataInfo));
        
    }
    /*checking existing Bookmarks page ends*/
    if(flag==0)
    {
        sessionStorage["subjectName"] = _subjectName;
        sessionStorage["subjectCode"] = _subjectCode;
        sessionStorage["courseTitle"] = _courseTitle;
        sessionStorage["class_section"] = _class_section;
        sessionStorage["term_code"] = _term_code;
        sessionStorage["position"] = _position;
        
        navigator.notification.confirm("Are you sure you want to add this to Bookmarks?",addToBookmarksInfo,'confirm',['Yes','No'])
    }
    else if(flag==1)
    {
        sessionStorage["pos"] = pos;
        navigator.notification.confirm("Are you sure you want to remove it from Bookmarks?",removeToBookmarksInfo,'confirm',['Yes','No'])
    }
}

/**
 * addToBookmarksInfo()
 * <function is Adding Bookmarks on local storage>
 * @param {string} button var is holding true/false
 * @param {string} strMsg Error Message
 * @return {boolean}
 */

function addToBookmarksInfo(button)
{
    if(button==1)
    {
        var _subjectName = sessionStorage["subjectName"];
        var _subjectCode = sessionStorage["subjectCode"];
        var _courseTitle = sessionStorage["courseTitle"];
        var _class_section = sessionStorage["class_section"];
        var _term_code = sessionStorage["term_code"];
        var _position = sessionStorage["position"];
        
        var storedDataInfo=JSON.parse(localStorage["bookmarksListObjectsInfo"]);
        storedDataInfo.bookmarksitemsInfo.push({"subject_name": _subjectName, "subject_code": _subjectCode, "course_title": _courseTitle, "class_section": _class_section, "term_code": _term_code, "position":_position});
        localStorage.setItem("bookmarksListObjectsInfo",JSON.stringify(storedDataInfo));
        sessionStorage.clear();
        $("#plusBookmarkShow1").data("icon", "minus");
        $("#plusBookmarkShow1 .ui-icon").addClass("ui-icon-minus").removeClass("ui-icon-plus");
        
    }
}

/**
 * removeToBookmarksInfo()
 * <function is Removing Bookmarks from local storage>
 * @param {int} button var is holding true/false
 * @param {string} strMsg Error Message
 * @return {boolean}
 */

function removeToBookmarksInfo(button)
{
    if(button==1)
    {
        var storedDataInfo=JSON.parse(localStorage["bookmarksListObjectsInfo"]);
        storedDataInfo.bookmarksitemsInfo.splice(sessionStorage["pos"], 1);
        localStorage.setItem("bookmarksListObjectsInfo",JSON.stringify(storedDataInfo));
        sessionStorage.clear();
        localStorage.clear();
        $("#plusBookmarkShow1").data("icon", "plus");
        $("#plusBookmarkShow1 .ui-icon").addClass("ui-icon-plus").removeClass("ui-icon-minus");
    }
}


/**
 * mapinitialize()
 * <function is Shows Map>
 * @param {int} _latitude var is holding latitude
 * @param {int} _longitude var is holding longitude
 * @param {string} text var is holding text
 * @return {none}
 */


var infowindow;
(function () {
 
 google.maps.Map.prototype.markers = new Array();
 
 google.maps.Map.prototype.addMarker = function(marker) {
 this.markers[this.markers.length] = marker;
 };
 
 google.maps.Map.prototype.getMarkers = function() {
 return this.markers;
 };
 
 google.maps.Map.prototype.clearMarkers = function() {
 if(infowindow) {
 infowindow.close();
 }
 
 for(var i=0; i<this.markers.length; i++){
 this.markers[i].set_map(null);
 }
 };
 })();


var PG_getLocation = function(options) {
    
    var successFn, //success callback
    errorFn, // error callback
    options = options || {};
    
    if(options) {
        successFn = options.success ||
        function() {
        };
        
        errorFn = options.error ||
        function() {
        };
        
    }
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(successCallback,errorCallback,{ timeout:10000, frequency: 3000, enableHighAccuracy: true });
        watchID = null;
        watchID=navigator.geolocation.watchPosition(function(result) {
                                                    successCallback(result);
                                                    
                                                    }, function(error) {
                                                    //Error
                                                    errorCallback(error);
                                                    
                                                    }, { frequency: 3000, enableHighAccuracy: true });
    }
    else
    {
        navigator.notification.alert("In order to use this feature, GPS must be enabled.",doNothing,'GPS Error','Ok');
    }
    
};


function errorCallback(error) {
    stopActivitySpinner();
    switch(error.code){
        case 0:{
            navigator.notification.alert("Unable to locate your location.",doNothing,'Alert','Ok');
            break;}
        case 1:{
            navigator.notification.alert("In order to use this feature, GPS must be enabled.",doNothing,'GPS Error','Ok');
            break;}
        case 2:{
            navigator.notification.alert("Oops! Your Position is not available.",doNothing,'Alert','Ok');
            break;}
    }
  
    
    if(watchID){
        window.clearInterval(watchID);}
    
}
//alert("Error"+error.code);
var currentLocation;
function successCallback(result) {
    
    var latitudeVal = result.coords.latitude;
    var longitudeVal = result.coords.longitude;
    
    //var latitudeVal = 34.181887;
    //var longitudeVal =-103.351666;
    
    currentLocation = {
        latitude : latitudeVal,
        longitude : longitudeVal
    };
    
    var latlng = new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude);
    var myOptions = {
    zoom: 18,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    
    mapDisplay = document.getElementById("map-canvas");
    stopActivitySpinner();
    if(currentLocation)
    {
        
        /*var strictBounds = new google.maps.LatLngBounds(
         new google.maps.LatLng(36.101319, -86.802056),
         new google.maps.LatLng(36.107140, -86.795876)
         );
         
         userPosition=new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude);
         try{
         if (strictBounds.contains(userPosition)){
         $.mobile.changePage("#mapPage");
         map = new google.maps.Map(mapDisplay, myOptions);
         
         map.addMarker(createMarker("You are here.", latlng,"Settings/images/mylocation.png"));
         if(checkLocationFlag){
         */
        getDirection(locationLat,locationLong,usertravelMode);
        /*}
         else{}
         }
         else
         {
         var latlng = new google.maps.LatLng(36.106139,-86.799758);
         $.mobile.changePage("#mapPage");
         
         
         var myOptions = {
         zoom: 16,
         center: latlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         };
         map = new google.maps.Map(mapDisplay, myOptions);
         map.addMarker(createMarker("You are here.", latlng,"Settings/images/mylocation.png"));
         if(checkLocationFlag){getDirection(locationLat,locationLong);}
         else{}
         //    getDirection(locationLat,locationLong);
         navigator.notification.alert("Oops! You are not within the campus. Map application only work inside the campus area.",doNothing,'Alert','Ok');
         
         }
         }catch(e){}*/
    }
    else
    {
        navigator.notification.alert("Oops! Your device doesn't have GPS enabled",doNothing,'Alert','Ok');
        if(watchID){
            window.clearInterval(watchID);}
    }
}

function mapinitialize()
{
    startActivitySpinner()
    try{
        var options = { timeout: 15000, enableHighAccuracy: true, maximumAge: 5000 };
        navigator.geolocation.getCurrentPosition(currentLocationSuccess,PreerrorCallback,options);
        
    }catch(e){
        stopActivitySpinner();
    }
}

function PreerrorCallback()
{
    //var options = { timeout: 31000, enableHighAccuracy: true, maximumAge: 90000 };
    navigator.geolocation.getCurrentPosition(currentLocationSuccess,errorCallback);
}


function currentLocationSuccess(result)
{
    stopActivitySpinner();
    var latitudeVal = result.coords.latitude;
    var longitudeVal = result.coords.longitude;
    //var latitudeVal =34.181887;
    //var longitudeVal =-103.351666;
    
    var latlng = new google.maps.LatLng(latitudeVal,longitudeVal);
    
    var strictBounds = new google.maps.LatLngBounds(
                                                    new google.maps.LatLng(41.151410,-73.256840),
                                                    new google.maps.LatLng(41.166068,-73.256515)
                                                    );
    
    userCurrentPosition=new google.maps.LatLng(latitudeVal,longitudeVal);
    //if (strictBounds.contains(userCurrentPosition)){
    $(".mapRefresh").hide();
    $("#directionDetail").hide();
    $("#mapTitle").html(collegeName+" Map");
    
    var latlng = new google.maps.LatLng(latitudeVal,longitudeVal);
    var myOptions = {
    zoom: 16,
    center: latlng,
        
    mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    mapDisplay = document.getElementById("map-canvas")
    $.mobile.changePage("#mapPage");
    map = new google.maps.Map(mapDisplay, myOptions);
    map.addMarker(createMarker("You are here.",latlng,"Settings/images/mylocation.png"));
    
    /*var latlng = new google.maps.LatLng(lat, lng);
     var myOptions = {
     zoom: 16,
     center: latlng,
     
     mapTypeId: google.maps.MapTypeId.ROADMAP
     };
     mapDisplay = document.getElementById("map-canvas");
     $.mobile.changePage("#mapPage");
     map = new google.maps.Map(mapDisplay, myOptions);*/
    
    /* $.each(mapBuildingsList, function(key,val){
     var latlng = new google.maps.LatLng(val.lat, val.long);
     var markerDesc="<div style='padding:10px'>";
     if(val.imgurl != ""){
     markerDesc +="<div style='width:100px'><img src=Settings/images/"+val.imgurl+" /></div>";
     }
     markerDesc +="<div style='color:#DD4747;font-weight:bold'>"+val.desc + "</div><div style='color:#DD4747;'>"+val.add + "</div>";
     if(val.webAdd != ""){
     markerDesc +="<a onclick=openWebpage('"+val.webAdd+"')>View WebPage</a></div>";
     }
     createMarker(markerDesc,latlng,"Settings/images/mapIcon.png");
     });
     
     */
    /* }
     else
     {
     navigator.notification.alert("Oops! You are not within the campus. Maps only work inside the campus area.",doNothing,'Alert','Ok');
     
     }*/
    
    
    $("#locationList").val("");
    $("#locationList").selectmenu("refresh");
    
}


var previousMarker=null;
var newMarker=null;
function createMarker(text,latlng,icon)
{
    var marker = new google.maps.Marker({
                                        animation: null,
                                        //draggable:true,
                                        position:latlng,
                                        map:map,
                                        icon:icon});
    
    google.maps.event.addListener(marker, "click", function() {
                                  if (infowindow){
                                  infowindow.close();
                                  }
                                  infowindow = new google.maps.InfoWindow({content: text});
                                  infowindow.open(map, marker);
                                  try{previousMarker.setAnimation(null)}catch(e){}
                                  if (marker.getAnimation() != null) {
                                  marker.setAnimation(null);
                                  } else {
                                  marker.setAnimation(null);
                                  }
                                  google.maps.event.addListener(infowindow,'closeclick',function(){
                                                                if (marker.getAnimation() != null) {
                                                                marker.setAnimation(null);
                                                                
                                                                }});
                                  newMarker=marker;
                                  previousMarker=newMarker;
                                  
                                  
                                  });
    mapMarkers.push(marker);
    viewMarkers=true;
    return marker;
}
function loactionMarkers()
{
    
}

function checkPosition(lat,long)
{
    checkLocationFlag=true;
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    }
    if(directionsDisplay){
        directionsDisplay.setMap(null);
    }
    makeDirectionMarker=true;
    locationLat=lat;
    locationLong=long;
    if(watchID){navigator.geolocation.clearWatch(watchID);}
    watchID = null;
    navigator.geolocation.getCurrentPosition(successCallback,errorCallback,{ timeout:10000, frequency: 3000, enableHighAccuracy: true });
    //getDirection(lat,long,usertravelMode);
}

function PositionInterval()
{
    watchID = null;
    watchID=window.setInterval(function(){navigator.geolocation.getCurrentPosition(successCallback,errorCallback,{ timeout:10000, frequency: 3000, enableHighAccuracy: true })},20000);
}

function getDirection(lat,long,travelModeOption)
{
    startActivitySpinner();
    if(directionsDisplay){
        directionsDisplay.setMap(null);
    }
    if (mapMarkers) {
        for (i in mapMarkers) {
            mapMarkers[i].setMap(null);
        }
    }
    
    
    directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true});
    
    /*Code for Customized Markers*/
    
    var icons = {
    start:new google.maps.MarkerImage(
                                      
                                      // URL
                                      'images/start-pin.png',
                                      // (image width,height)
                                      new google.maps.Size( 32, 32 ),
                                      //Point for the Image
                                      new google.maps.Point(0,0 )
                                      ),
    end:new google.maps.MarkerImage(
                                    // URL
                                    'images/end-pin.png',
                                    // (image width,height)
                                    new google.maps.Size( 32, 32 ),
                                    //Point for the Image
                                    new google.maps.Point( 0, 0 )
                                    )
    };
    directionsDisplay.setMap(null);
    directionsDisplay.setMap(map);
    try{
        
        var directionsService = new google.maps.DirectionsService();
        //var start = new google.maps.LatLng(34.252045,-103.245397);
        var start = new google.maps.LatLng(currentLocation.latitude,currentLocation.longitude);
        var end = new google.maps.LatLng(lat,long);
        switch(travelModeOption){
            case "WALKING":{
                usertravelMode="WALKING";
                var request = {
                origin:start,
                destination:end,
                travelMode:google.maps.TravelMode.WALKING,
                unitSystem: google.maps.UnitSystem.METRIC
                };
                break;}
            case "BICYCLING":{
                usertravelMode="BICYCLING";
                var request = {
                origin:start,
                destination:end,
                travelMode:google.maps.TravelMode.BICYCLING,
                unitSystem: google.maps.UnitSystem.METRIC
                };
                
                break;}
            case "DRIVING":{
                usertravelMode="DRIVING";
                var request = {
                origin:start,
                destination:end,
                travelMode:google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC
                };
                
                break;}
            default :{
                $("#mapRadio").find("input[type='radio']").attr("checked",false).checkboxradio("refresh");
                $("#mapRadio").find("input[name='walking']").attr("checked",true).checkboxradio("refresh");
                usertravelMode="WALKING";
                var request = {
                origin:start,
                destination:end,
                travelMode:google.maps.TravelMode.WALKING,
                unitSystem: google.maps.UnitSystem.METRIC
                };
                break;}
        }
    }catch(e){alert(e)}
    directionsService.route(request, function(result, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                            try{
                            //$(".mapRefresh").show();
                            console.log(JSON.stringify(result));
                            
                            $("#directionDetail").show();
                            var len=result.routes[0].legs[0].distance.text.length-3;
                            var miles=result.routes[0].legs[0].distance.text.substring(0,len)*0.621;
                            var d=miles.toString();
                            // alert(d.slice(0,8)+" "+result.routes[0].legs[0].distance.text.substring(0,len)+" "+len);
                            $("#directionInfo").html("TravelMode: "+usertravelMode+"<br />Distance = <span style='white-space:nowrap; text-overflow:ellipsis; overflow:hidden;width:35px;'>"+d.slice(0,6)+"</span> miles <br />Time = "+JSON.stringify(result.routes[0].legs[0].duration.text));
                            }catch(e){if(watchID){
                            window.clearInterval(watchID);}}
                            directionsDisplay.setDirections(result);
                            var leg = result.routes[ 0 ].legs[ 0 ];
                            if(makeDirectionMarker){
                            createMarker(leg.start_address ,leg.start_location, icons.start);
                            createMarker($( "#locationList option:selected" ).text(),leg.end_location, icons.end);
                            
                            }
                            stopActivitySpinner();
                            }
                            else
                            {
                            stopActivitySpinner();
                            navigator.notification.alert("This feature is not available in this area.",doNothing,travelModeOption,'Ok');
                            if(watchID){
                            window.clearInterval(watchID);}
                            }
                            });
    stopActivitySpinner();
    delete directionsDisplay;
}


function makeMarker( position, icon, title ) {
    dirMarker = new google.maps.Marker({
                                       animation: null,
                                       position: position,
                                       map: map,
                                       icon: icon,
                                       title: title
                                       });
    markersArray.push(dirMarker);
    // google.maps.event.addListener(marker, 'click', toggleBounce);
}
function toggleBounce() {
    
    if (marker.getAnimation() != null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(null);
    }
}


function showMapMarkers() {
    
    $("#directionDetail").hide();
    if (markersArray) {
        for (i in markersArray) {
            markersArray[i].setMap(null);
        }
    }
    directionsDisplay.setMap(null);
    if (mapMarkers) {
        for (i in mapMarkers) {
            mapMarkers[i].setMap(map);
        }
    }
    if (infowindow){infowindow.close();}
    //$(".mapRefresh").hide();
}


/**
 * preLoginAddContacts()
 * <function is used to Add Contact>
 * @param {string} pageID var is holding Page ID
 * @return {none}
 */
function preLoginAddContacts(pageID)
{
    var dirContact = navigator.contacts.create();
    
    if(myCampusObject.settings.androidphone)
    {
        dirContact.displayName = $("#displayName_directory_listing").text();
        var phoneNo= $("#phone").text();
        if(phoneNo)
        {
            var phoneNumbers = [1];
            phoneNumbers[0] = new ContactField('work', phoneNo, true); // preferred number
            dirContact.phoneNumbers = phoneNumbers;
        }
        
        var personEmail = $("#email").text();
        
        if(personEmail)
        {
            var emails = [1];
            emails[0] = new ContactField('emails', personEmail, false);
            dirContact.emails = emails;
        }
        
        ///for ios displayName is not supported insted of displayName we can use name object
        var name = new ContactName();
        name.givenName = $("#fname").text();
        name.familyName = $("#lname").text();
        dirContact.name = name;
        
        dirContact.save(preLoginSaveContacts,preLoginSaveError);
    }
    if(myCampusObject.settings.iphone)
    {
        
        var phoneNumbers = [2];
        // phoneNumbers[0] = new ContactField('mobile', $("#phone").text(), true); // preferred number
        phoneNumbers[0] = new ContactField('work', $("#phone").text(), true); // Work/Dept. number
        dirContact.phoneNumbers = phoneNumbers;
        
        var emails = [1];
        emails[0] = new ContactField('emails', $("#email").text(), false);
        dirContact.emails = emails;
        
        ///for ios displayName is not supported insted of displayName we can use name object
        var name = new ContactName();
        name.givenName = $("#fname").text();
        name.familyName = $("#lname").text();
        
        dirContact.name = name;
        
        dirContact.save(preLoginSaveContacts,preLoginSaveError);
    }
}

/**
 * preLoginSaveContacts()
 * <function is used to Save Contact>
 * @return {none}
 */
function preLoginSaveContacts(contact)
{
    navigator.notification.alert("Contact Saved Successfully",doNothing,"Success");
}

/**
 * preLoginSaveError()
 * <function is used to Save Error>
 * @param {int} button var is holding true/false
 * @param {string} strMsg Error Message
 * @return {none}
 */
function preLoginSaveError(contactError)
{
    navigator.notification.alert("Contact already saved",doNothing,"Alert");
}


/**
 * preloginHomePage()
 * <Loads index page>
 * @return {none}
 */

function preloginHomePage()
{
    
    window.location.replace("index.html#mainDashboardPage");
}


/**
 * <sendMessageRequestST>
 * <stores the value of parameters(subject, content, individual user) and calls send message fn>
 * @param {string} service Ticket for sending the message
 * return { none }
 */
function sendMessageRequestST()
{
    
    /* if($("#IndividualUsers").val().length == 0)
     {
     // navigator.notification.alert("Please enter indivisual user ",doNothing,"Alert");
     //return false;
     }
     */
    
    if($("#subject").val().length == 0)
    {
        navigator.notification.alert("Please enter a subject.",doNothing,"Subject Required");
        return false;
    }
    
    if($("#messageContent").val().length == 0)
    {
        navigator.notification.alert("Please enter some content.",doNothing,"Content Required");
        return false;
    }
    
    
    var url_service = '/Alerts/AlertsService/AnnouncementsServiceForUser/message'
    request_MsgST(url_service,sendMessageRequestSTComplete,false);
    
}


/**
 * <sendMessageRequestSTComplete>
 * <stores the value of parameters(subject, content, individual user) and calls send message fn>
 * @param {string} service Ticket for sending the message
 * return { none }
 */


function sendMessageRequestSTComplete(result)
{
    try{
        var groupid,userid,tagid=null;
        
        
        // if($("#users").val() != "") userid[0]= $("#users").val();
        // if($("#communities").val() != "") groupid[0]= $("#communities").val();
        // alert(document.getElementById("communities").length);
        //alert($("#users").length);
        
        for (var i=1; i<document.getElementById("communities").length; i++)
        {
            if (document.getElementById("communities").options[i].selected==true){
                groupid = document.getElementById("communities").options[i].value;
            }
        }
        
        
        
        
        for (var i=1; i<document.getElementById("users").length; i++)
        {
            if (document.getElementById("users").options[i].selected==true){
                userid = document.getElementById("users").options[i].value;
            }
        }
        if(groupid == null && userid == null)
        {
            navigator.notification.alert("Select a group or communities.",doNothing,"Send Message");
            return false;
        }
        
        for (var i=1; i<document.getElementById("tags").length; i++)
        {
            if (document.getElementById("tags").options[i].selected==true){
                tagid = document.getElementById("tags").options[i].value;
            }
        }
        if(tagid == null || tagid == "undefined" || tagid == "")
        {
            navigator.notification.alert("Please choose a tag to send your message.",doNothing,"Send Message");
            return false;
        }
        
        /*if($("#IndividualUsers").val().length == 0)
         {
         var recipientUser="";
         }
         else
         {
         var recipientUser=$("#IndividualUsers").val();
         }*/
        
        var recipientUser="";
        var content=$("#messageContent").val();
        var subject=$("#subject").val();
        
        
        if(userid == null || userid == "undefined")
        {
            userid =false;
        }
        if(groupid == null || groupid == "undefined")
        {
            groupid =false;
        }
        
        sendMessage(groupid,userid,recipientUser,subject,content,tagid,result);
    }catch(e){}
    
    
}
/**
 * <request_ST>
 * <request service ticket>
 * @param {string} service url of the requested service
 * @param {string} name of the function to call after successfull service ticket
 * @param {boolean} checks wheather to show error or not
 * return { none }
 */

function request_ST(_url_service,_return_Where,_silent)
{
    try
    {
        
        if (window.localStorage.getItem("LS_RememberMe") == "true")
        {
            tempECryptUname =  window.localStorage.getItem("LS_usernameEncrypted");
            tempECryptPwd =  window.localStorage.getItem("LS_passwordEncrypted");
        }
        else
        {
            tempECryptUname =  usernameEncrypted;
            tempECryptPwd =  passwordEncrypted;
        }
        
        _silent =( _silent || false);
        
        generateST(tempECryptUname,tempECryptPwd,_url_service,_return_Where,_silent);
    }catch(e){}
}


function request_MsgST(_url_service,_return_Where,_silent)
{
    try
    {
        
        if (window.localStorage.getItem("LS_RememberMe") == "true")
        {
            tempECryptUname =  window.localStorage.getItem("LS_usernameEncrypted");
            tempECryptPwd =  window.localStorage.getItem("LS_passwordEncrypted");
        }
        else
        {
            tempECryptUname =  usernameEncrypted;
            tempECryptPwd =  passwordEncrypted;
        }
        
        _silent =( _silent || false);
        
        generateMsgST(tempECryptUname,tempECryptPwd,_url_service,_return_Where,_silent);
    }catch(e){}
}
/**
 * saveAlertsJSON()
 * <saves the msg json in local storage >
 * @param {json} _jsonArr contains list of all msgs in json format
 */
function saveAlertsJSON(_jsonArr) {
    var dOld;
    var dNew;
    
    if (_jsonArr.length > 0) {
        dNew = _jsonArr[_jsonArr.length - 1].date;
    }
    
    if (lastAlertTS == null) {
        dOld = window.localStorage.getItem("LS_lastAlertTS");
    }
    else {
        dOld = lastAlertTS;
    }
    
    alertsJSON = _jsonArr; // Save the value for the Display function
    window.localStorage.setItem("LS_lastAlertTS", dNew);
    lastAlertTS = dNew;
    
     //alert(window.sessionStorage.getItem("_checkMsgCount") + " " + msgClick);
    if (window.sessionStorage.getItem("_checkMsgCount") == "true") {
        
        if (alertsJSON.page.length == 0) {
            $(".msgcount").hide();
            msgcount = alertsJSON.page.length;
            window.localStorage.setItem("LS_total_Msg", msgcount);
        }
        else {
        try{$(".msgcount").show();
            if (msgClick == false) {
                var processedMsg=0;
                var sentMsgs=0;
                var viewedMsgs=0;
                var acknowledgeMsgs=0;
                var expiredFailed=0;
                for(var k=0;k<alertsJSON.page.length;k++){
                    switch(alertsJSON.page[k].status){
                        case 0:{
                            processedMsg=processedMsg+1;
                            break;
                        }
                        case 1:{
                            sentMsgs=sentMsgs+1;
                            break;
                        }
                        case 2:{
                            viewedMsgs=viewedMsgs+1;
                            break;
                        }
                        case 3:{
                            acknowledgeMsgs=acknowledgeMsgs+1;
                            break;
                        }
                        default:{
                            expiredFailed=expiredFailed+1;
                        }
                    }
                }
                
                newMsgs = (alertsJSON.page.length-(viewedMsgs+acknowledgeMsgs+expiredFailed));
                //newMsgs = alertsJSON.page.length - window.localStorage.getItem("LS_total_Msg");
                //alert(alertsJSON.page.length + " " + window.localStorage.getItem("LS_total_Msg") + " " + newMsgs + " " + oldMsgs);
                if (newMsgs <= 0) {
                    $(".msgcount").hide();
                    $(".msgcount").html(0);
                } else {
                    //alert(newMsgs+" "+oldMsgs);
                    if (newMsgs > oldMsgs) {
                        oldMsgs = newMsgs;
                        $(".msgcount").html(newMsgs);
                        notifyUser();
                    }
                }
            }
            else {
                $(".msgcount").hide();
                //$(".msgcount").html(newMsgs);
            }
            
        }catch(e){alert(e)}
        }
    }
    else {
        
        displayAllAlerts(false);
        /* newMsgs = alertsJSON.page.length - window.localStorage.getItem("LS_total_Msg");
         
         if (newMsgs == 0) {
         } else {
         if (newMsgs > oldMsgs) {
         window.localStorage.setItem("LS_total_Msg", alertsJSON.page.length);
         oldMsgs = newMsgs;
         notifyUser();
         }
         }*/
    }
}

/**
 * notifyUser()
 * This function notifies the user based on his chosen preferences in the settings.
 * In Android phones this is used to generate the status bar notifications as well.
 */
function notifyUser()
{
    
    try{
        if (window.localStorage.getItem("LS_soundAlert")!= "false")
        {
            navigator.notification.beep(1);
        }
        if (window.localStorage.getItem("LS_vibrateAlert")!= "false")
        {
            navigator.notification.vibrate(1000);
        }
        if(myCampusObject.settings.androidphone)
        {
            // Do Android Specific status bar notification
        }
    }
    catch(e)
    {
        
    }
}

/* Confirm whether user wants to acknowledge the alert or not*/


/**
 * removeAckFromGVariable()
 * <acknowledge the message>
 
 */

function removeAckFromGVariable()
{
    var len = alertsJSON.length;
    var flag=0;
    var pos;
    for(i=0;i<len;i++)
    {
        if(alertsJSON[i].id==id_req)
        {
            flag=1;
            pos=i;
            break;
        }
    }
    if(flag==1)
    {
        alertsJSON.splice(pos, 1);
        displayAllAlerts(true);
    }
}

/**
 * logOut()
 * <sign out the user depending upon the response>
 * @param {boolean} button contains 1 or 0 value depending upon user response
 
 */
function checkLogout(button)
{
    if (button == 1)
    {
        
        tgtTicket = null;
        passwordEncrypted = null;
        usernameEncrypted = null;
        window.sessionStorage.removeItem("LS_tgtTicket");
        window.localStorage.removeItem("LS_serviceTicket");
        window.localStorage.removeItem("LS_usernameEncrypted");
        window.localStorage.removeItem("LS_passwordEncrypted");
        window.localStorage.removeItem("LS_firstTimeUser");
        window.sessionStorage.removeItem("LS_TryAutoLogin");
        
        //window.localStorage.removeItem("LS_total_Msg");
        window.localStorage.removeItem("favInfo");
        
        clearInterval(msgInterval);
        
        if(!myCampusObject.settings.isCollegeBuild && window.localStorage.getItem("LS_RememberMe") == "false" )
        {
            serverName = null;
            serviceName = null;
            $("#LoginServerUrl").val("");
            $("#LoginServiceUrl").val("");
            window.localStorage.removeItem("LS_serviceDetails");
            window.localStorage.removeItem("LS_serverDetails");
            
        }
        
        //Display the Login Form again after setting the value of other fields apart from password
        $("#loginUsername").val("");
        $("#loginPassword").val("");
        
        $("#preLoginHeaderLoginbutton").show();
        $("#preLoginHeaderHomebutton").hide();
        $.mobile.changePage("#mainDashboardPage",{ transition: "none"});
        //window.location.replace("../../index.html");
        $(".hideFav").hide();
        $.each(ThreeDLoggedListObject, function(key,val){$("#"+val.id).die();});
        loginiconScroll.destroy();
    }
    
    
    
}


/**
 * refreshAlerts()
 * <>
 * @_silent {bool} Controls verbosity of error alerts
 * @return {boolean}
 */
function refreshAlerts(_silent)
{
    
    url_service="/Alerts/AlertsService/AlertsInfoService/inbox";
    return_Where=getComplete;
    request_MsgST(url_service,return_Where,false);
    
}


function getComplete(_serviceTicket)
{
    getAlertsJSON(_serviceTicket,false);
}

/**
 * autoRefresh()
 * <fn-description>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message
 * @return {boolean}
 */
function autoRefresh()
{
    
    var tempTime = parseInt(autoRefreshTimer);
    if(timer){ clearTimeout(timer);}
    
    if(firstTimeSupress == true)
    {
        if (tempTime != 0){ timer = setTimeout(autoRefresh, tempTime);}
        firstTimeSupress = false;
    }
    else
    {
        if (tempTime != 0)
        {
            var activePage = $('.ui-page-active').attr('id');
            if(activePage == "home" || activePage == "login_page")
            {
                // clear timer as user is logged out/not logged in yet
                //clearTimeout(timer);
                //Do Nothing, timer has already been cleared as the first step of the function
                //console.log("Pre-login supress");
            }
            else
            {
                //clearTimeout(timer);
                timer = setTimeout(autoRefresh, tempTime);
                refreshAlerts(true);
                //console.log("refreshing");
            }
        }
    }
}

function tabSelector(page_id)
{
    if(page_id == "loggedInDashboardPage")
    {
        loginiconScroll.destroy();
        loginiconScroll = new iScroll('loggedwrapper', {
                                      snap: true,
                                      momentum: false,
                                      hScrollbar: false,
                                      onScrollEnd: function () {
                                      document.querySelector('#loggedindicator > li.active').className = '';
                                      document.querySelector('#loggedindicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
                                      }
                                      });
        
        var html_nav = "<ul id='loggedindicator' >";
        html_nav +="<li class='active'>1</li>";
        for(var k = 2;k<=lognavCount;k++)
        {
            html_nav +="<li>"+k+"</li>";
        }
        html_nav +="</ul>";
        var loggednavtextWidth = (8 * lognavCount);
        var loggednavtextMarginLeft = ($(document).width()-110)/2;
        // $("#loggednavtext").css({'width':loggednavtextWidth});
        $("#loggednavtext").css({'margin-left':loggednavtextMarginLeft});
        
        $("#loggednavtext").html(html_nav);
        
        
        $.mobile.changePage("index.html#"+page_id);
    }
    else{
        $.mobile.changePage("index.html#"+page_id);
    }
}


/**
 * addToFavoriteConfirm()
 * <adds service to the favorites page>
 * @return {none}
 */

try{
    
    function addToFavoriteConfirm()
    {
        
        taphold = true;
        var flag = 0;
        var tempObject;
        var pageId=$('.ui-page-active').attr('id')
        e=$(this).attr("id");
        switch (pageId)
        {
            case "loggedInDashboardPage":{
                tempObject=ThreeDLoggedListObject;
                
                break;
            }
                
            case "favorites":{
                flag=1;
                tempObject=JSON.parse(localStorage['favInfo']);
                break;}
        }
        
        if(localStorage['favInfo']!=null){
            var storedData=JSON.parse(localStorage['favInfo']);
            /*checking existing fav starts*/
            for(i=0;i<storedData.items.length;i++){
                if(storedData.items[i].id==e){
                    flag = 1;
                    window.sessionStorage.setItem("pos",i);
                    window.sessionStorage.setItem("_id",e);
                    break;
                }
            }
        }
        
        AddingToFav(e,tempObject,flag)
    }
    
    
    function AddingToFav(id,tempObject,flag)
    {
        if(flag == 0)
        {
            $.each(ThreeDLoggedListObject, function(key,val)
                   {
                   if(id == val.id)
                   {
                   //alert(val.id+val.imageUrl+val.description+val.roles);
                   window.sessionStorage.setItem("img_Src",val.imageUrl);
                   window.sessionStorage.setItem("img_Title",val.description);
                   window.sessionStorage.setItem("_id",val.id);
                   window.sessionStorage.setItem("serverUrl",val.servername);
                   //break;
                   }
                   });
            navigator.notification.confirm('Are you sure you want to add this to favorites?',addTofav,'Add this to Favorites',['Yes','No']);
        }
        else{
            navigator.notification.confirm('Are you sure you want to remove it from favorites?',removeFromfav,'Remove this from Favorites',['Yes','No']);
        }
    }
    /**
     * addTofav()
     * <adds service to the favorites page>
     
     * @return {none}
     */
    
    
    function addTofav(button){
        
        if(button==1){
            if(localStorage['favInfo']==null)
            { var storedData = {items: []}; }
            else
            { var storedData=JSON.parse(localStorage['favInfo']); }
            
            
            var img_Src=window.sessionStorage.getItem("img_Src");
            var img_Title=window.sessionStorage.getItem("img_Title");
            var _id=window.sessionStorage.getItem("_id");
            var serverUrl=window.sessionStorage.getItem("serverUrl");
            
            //alert(img_Src+" "+img_Title+" "+_id+" "+serverUrl);
            storedData.items.push({imageUrl: img_Src, description: img_Title,id:_id,roles:serverUrl});
            localStorage.setItem("favInfo",JSON.stringify(storedData));
            try{
                
                $("#"+_id+"Heart").attr("src","images/star-activered.png");
                taphold = false;
            }catch(e){}
            
            
        }
        if(button==2){taphold = false;}
        
    }
    
    /**
     * removeFromfav()
     * <remove service to the favorites page>
     
     * @return {none}
     */
    function removeFromfav(button){
        if(button==1){
            var storedData=JSON.parse(localStorage['favInfo']);
            var pos=window.sessionStorage.getItem("pos");
            
            storedData.items.splice(pos, 1);
            localStorage.setItem("favInfo",JSON.stringify(storedData));
            //getFavorite();
            var _id=window.sessionStorage.getItem("_id");
            
            $("#"+_id+"Heart").attr("src","images/star2.png");
            
            checkFavorite();
            taphold = false;
        }
        if(button==2){ taphold = false;}
        
    }
    
    
    
    
    /**
     * <addToFavPage>
     * <depending upon the id checks whether to add or remove>
     * @return {none}
     */
    
    
    
    
    function addToFavPage(id)
    {
        
        var flag =0;
        if(localStorage['favInfo']!=null){
            var storedData=JSON.parse(localStorage['favInfo']);
            /*checking existing fav starts*/
            for(i=0;i<storedData.items.length;i++){
                if(storedData.items[i].id==id){
                    flag = 1;
                    window.sessionStorage.setItem("pos",i);
                    window.sessionStorage.setItem("_id",id);
                    break;
                }
            }
        }
        //window.sessionStorage.setItem("_id",id);
        
        AddingToFav(id,ThreeDLoggedListObject,flag)
        
    }
    
}catch(e){}

$("#settings").live("pagebeforeshow",displaySettings);
$("#settings").live("pagebeforehide",saveSettings);
/**
 * <displaySettings>
 * <sets the display settings>
 * @return {none}
 */

function displaySettings() // Called on settings icon click
{
    
    try{
        
        if(window.localStorage.getItem("LS_vibrateAlert") == "false")
        {
            
            $("#settingVibrate-OFF").attr("checked",true).checkboxradio("refresh");
        }
        else
        {
            $("#settingVibrate-ON").attr("checked",true).checkboxradio("refresh");
        }
        
        if(window.localStorage.getItem("LS_soundAlert") == "false")
        {
            
            $("#settingSound-OFF").attr("checked",true).checkboxradio("refresh");
        }
        else
        {
            
            $("#settingSound-ON").attr("checked",true).checkboxradio("refresh");
        }
        
        
        if(window.localStorage.getItem("LS_autoRefreshTimer") != null)
        {
            $("#settingAutoRefresh").val(window.localStorage.getItem("LS_autoRefreshTimer"));
        }
        else
        {
            $("#settingAutoRefresh").val(autoRefreshTimer);
        }
        if(window.localStorage.getItem("LS_IconType") != null)
        {
            $("#IconSet").val(window.localStorage.getItem("LS_IconType"));
            
        }
        
        
        $("#settingAutoRefresh").selectmenu('refresh');
        $("#IconSet").selectmenu('refresh');
    } catch(e)
    {
        //alert(e)
    }
}

/**
 * <saveSettings>
 * <save the settings>
 * @return {boolean}
 */
function saveSettings() // Called on settings page page hide event
{
    
    try{
        // console.log("Inside SaveSettings");
        
        if($('#settingVibrate-ON:checked').val() == "on")
        {
            window.localStorage.setItem("LS_vibrateAlert","true");
        }
        else
        {
            window.localStorage.setItem("LS_vibrateAlert","false");
        }
    }
    catch(e){
        //alert("vibrate"+e)
    }
    
    try{
        if($('#settingSound-ON:checked').val() == "on")
        {
            window.localStorage.setItem("LS_soundAlert","true");
        }
        else
        {
            window.localStorage.setItem("LS_soundAlert","false");
        }
    }
    catch(e){
        //alert("setitem sound alert"+e);
    }
    
    try{
        if(window.localStorage.getItem("LS_RememberMe") == "true")
        {
            window.localStorage.setItem("LS_autoRefreshTimer",$("#settingAutoRefresh").val());
            
            clearInterval(msgInterval);
            msgInterval=setInterval(function(){TimeoutRefreshAlerts();},parseInt($("#settingAutoRefresh").val()));
            
            var PreviousIconType;
            PreviousIconType = window.localStorage.getItem("LS_IconType");
            
            if(PreviousIconType != $("#IconSet").val())
            {
                loginiconScroll.destroy();
                icon_generate($("#IconSet").val());
                window.localStorage.setItem("LS_IconType",$("#IconSet").val());
                //logiconScroll.refresh();
            }
            
        }
        else
        {
            window.localStorage.removeItem("LS_autoRefreshTimer");
            window.localStorage.removeItem("LS_IconType");
            
        }
    }
    catch(e){
        // alert("Set or clear auto refresh timer"+e)
    }
    
    
    try{
        autoRefreshTimer = $("#settingAutoRefresh").val();
        
    }
    catch(e){
        //alert("setting auto refresh"+e)
    }
    
}


/* Generate Service Ticket for calling the Web Service. If TGT has expired or is not there, then generate TGT as well*/
/**
 * <message_Success>
 * <notify user with success if messageis send successfully>
 * @param {string} TGT ticket for getting service ticket.
 * @return { boolean }
 */
function message_Success()
{
    navigator.notification.alert("Your message has been sent successfully.",doNothing,"Message Sent",'Ok');
    $("#messageContent").val("");
    $("#subject").val("");
    $("#IndividualUsers").val("");
    $("#users").val("");
    $("#tags").val("");
    $("#communities").val("");
    $("#communities").selectmenu("refresh");
    $("#users").selectmenu("refresh");
    $("#tags").selectmenu("refresh");
    
}

/**
 * <quick_Launch>
 * <opens child browser>
 * @param {string} TGT ticket for getting service ticket.
 * @return { boolean }
 */
function myInfo_Launch(_serviceTicket)
{
    try{
        var tempURL2  = serviceName+QL_myInfo_service + "&ticket="+_serviceTicket;
        navigator.notificationEx.loadingStop();
        //console.log(tempURL2);
        client_browser.showWebPage(tempURL2);
        
    }
    catch(e)
    {
        navigator.notificationEx.loadingStop();
        
    }
}

function generalShowHide(a, b, c, d)
{
    $("#"+a).show();
    $("#"+b).hide();
    $("#"+c).hide();
    $("#"+d).hide();
}



/**
 * splitDate()
 * <Splits the date>
 * @param {string} strMsg Error Message
 * @return {boolean}
 */

function splitDate(str)
{
    var timeZone;
    var intHour;
    var splHm;
    var retStr;
    var orgStr = str.split(' ');
    var splDt = orgStr[0];
    var splDtFull = splDt.split('-');
    var splTm = orgStr[1];
    var splTmFull = splTm.split(':');
    
    intHour = splTmFull[0];
    
    //console.log ("intHour is"+intHour);
    
    if (intHour < 13)
    {
        //console.log("setting AM"+intHour);
        timeZone = 'AM';
        if (intHour == 00)
        {
            intHour = '00';
            timeZone = 'AM';
        }
        if (intHour < 10)
        {
            intHour = intHour+"";
            intHour = " " + intHour;
        }
        if (intHour == 12)
        {
            //intHour = '00';
            timeZone = 'PM';
        }
    }
    else
    {
        intHour = intHour - 12;
        timeZone = 'PM';
        if (intHour < 10)
        {
            intHour = intHour+"";
            intHour = " "+intHour;
        }
    }
    
    splHm = intHour+":"+splTmFull[1]+" "+timeZone;
    
    // Change for month making it 7 instead of 07
    
    var newMonth = splDtFull[1];
    if (newMonth < 10)
    {
        newMonth = newMonth.substr(1);
    }
    
    var retStr = newMonth+"/"+splDtFull[2]+" "+splHm;
    
    return retStr;
}

/* Reduce the String length */
function restrictSource(str)
{
    var strRes = str.substr(0,25);
    return strRes;
}


function BannerReqTermDown()
{
    url_service="/enmu/services/student/bannertermdropdown";
    return_Where=BannerTermDown;
    request_ST(url_service,return_Where,false);
    
}

function midtermgrade_shows(id){
    displaymidtermgrade(id)
}


function BannerFGTermDown()
{
    url_service="/enmu/services/student/bannertermdropdown";
    return_Where=BannerTermDownFinalGrades;
    request_ST(url_service,return_Where,false);
}

var finalGradeTerm;
function Fgrade_shows(id)
{
    finalGradeTerm=id;
    url_service="/enmu/services/student/bannerfinalgrade/termCode="+id;
    return_Where=displayFGgrade;
    request_ST(url_service,return_Where,false);
    //displayFGgrade(id)
}

var LUCterm;
function LUC_shows(id)
{
    $(".LUCSubParams").show();
    LUCterm=id;
    
    url_service="/enmu/services/student/bannergetOtherClassSearchParams/termCode="+id;
    return_Where=LUCcall;
    request_ST(url_service,return_Where,false);
}

var LUCSubCode;
function LUC_ChangeSubject(id)
{
    $(".hideLUC").show();
    LUCSubCode=id;
    url_service="/enmu/services/student/bannerlucinstructorsforsubj/termCode="+LUCterm+"&subj="+id;
    return_Where=LUCInstructors;
    request_ST(url_service,return_Where,false);
}

var academicTransTerm;
function acadeic_shows(id)
{
    
    academicTransTerm=id;
    url_service="/enmu/services/student/bannertranscripts/levl="+id;
    return_Where=AcademicCall;
    request_ST(url_service,return_Where,false);
    
}

function validateBeforeSearch()
{
    var trmcde = $("#LUC").val();
    var subj = $("#LUCsubject").val();
    var crseNum = $("#coursenum").val()
    var crseTit = $("#title").val()
    var crtfrm = $("#creditfrm").val()
    var crtto = $("#creditto").val()
    var pot = $("#partoftrm").val()
    var ins = $("#slctinstruc").val()
    
    
    server_url= "/enmu/services/student/bannerclasslookup/termCode="+trmcde+"&subjectCode="+subj+"&courseNumber="+crseNum+"&partTermCode="+pot+"&facultyPidm="+ins+"&courseTitle="+crseTit+"&creditFrom="+crtfrm+"&creditTo="+crtto;
    url_service=server_url;
    return_Where=checksearch;
    request_ST(url_service,return_Where,false);
}


function acadeicdrop_selec(id)
{
    ATfinshow(id);
}
var WeekTerm;
function WeekTermDownClick(id)
{
    WeekTerm=id;
    url_service="/enmu/services/student/bannerweekataglance/termCode="+id;
    return_Where=weekAtGlance;
    request_ST(url_service,return_Where,false);
    
    //weekAtGlance(id);
}

var accTermDown;
function AccountTermDownClick(id)
{
    accTermDown=id;
    url_service="/enmu/services/student/banneraccountsummary/termCode="+id;
    return_Where=bannerAccountSummary;
    startActivitySpinner();
    request_ST(url_service,return_Where,false);
}
function notification_Control(id)

{
    
    switch(id)
    {
            
        case "true" : 
        {
            url_service="/Alerts/AlertsService/AlertsInfoService/preference?key=ParseDeliveryChannel&value=true";
            return_Where=notification_ControlOn;            
            request_ST(url_service,return_Where,false);
            break;
            
        }
            
            
        case "false" : 
        {
            url_service="/Alerts/AlertsService/AlertsInfoService/preference?key=ParseDeliveryChannel&value=false";
            return_Where=notification_ControlOff;            
            request_ST(url_service,return_Where,false);
            break;
        }
            
    }
    
}

function check_push_status()
{
    url_service="/Alerts/AlertsService/AlertsInfoService/preference/ParseDeliveryChannel";
    return_Where=check_push_notification;            
    request_ST(url_service,return_Where,false);
}

function GoToNextMonth()
{
    NextMonthCounter=NextMonthCounter + 1;
    PrevMonthCounter=PrevMonthCounter-1;
    tdyDate.setDate(daysToNextMonth);
    fetchEvents(tdyDate);
    if(NextMonthCounter == 12)
    {$("#NextDay").css({'visibility':'hidden'});}
    $("#PreviousDay").css({'visibility':'visible'});
}
function GoToPrevMonth()
{
    NextMonthCounter=NextMonthCounter - 1;
    PrevMonthCounter=PrevMonthCounter + 1;
    tdyDate.setDate(0);
    fetchEvents(tdyDate);
    if(PrevMonthCounter == 1)
    {$("#PreviousDay").css({'visibility':'hidden'});}
    $("#NextDay").css({'visibility':'visible'});
}

var regTermDown;
function registrationTermDropDownSelect(id){
    regTermDown=id;
    url_service="/enmu/services/student/bannerregistrationstatus/termCode="+id;
    return_Where=registrationCalDisplay;
    request_ST(url_service,return_Where,false);
}
