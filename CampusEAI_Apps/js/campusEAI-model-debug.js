/*
 Copyright(c) 2012 CampusEAI Consortium.
 http://www.campuseai.org/
 */

/**
 * @fileoverview This file contains all the function calls for getting and storing the data from
 * web services, local storage and local database after the user before the user has logged in.
 */

/* Documentation Example */
try{
function collegeDirectory(_servername)
{   
 directoryentries=new Array();
    startActivitySpinner();
  
  var xhr=$.ajax({
                 url: _servername,
                 //type: "GET",
                 dataType:"xml",
                 async:true,
                 data:{},
                 success: function(data)
                 {
                 var xml=$(data);
                 var directorys=xml.find("USM_DIRECTORY");
                 $.each(directorys,function(i,v){
                         direntry = {
                        univinetMail:$(v).find("emailADDR").text(),
                        title:$(v).find("title").text(),
                        lastname:$(v).find("lastNAME").text(),
                        firstname:$(v).find("firstNAME").text(),
                        middleName:$(v).find("middleNAME").text(),
                        displayName:$(v).find("displayNAME").text(),
                        dept:$(v).find("department").text(),
                        phone:$(v).find("officePHONE").text(),
                        address:$(v).find("building").text(),
                        title:$(v).find("title").text(),
                        photo:$(v).find("Photo").text()
                              };
                              //alert(direntry);
                              directoryentries.push(direntry);
                              });
                       console.log(JSON.stringify(directoryentries));
                       pageChange("directory_listing");
                  },
                 error: function(e)
                 {
                 stopActivitySpinner();
                 alertServerError(e.status);
                 },
                 complete:function()
                 {
                 stopActivitySpinner();
                 }
                 });
  
  
}
}catch(e){//alert(e)
}

function CoursesList(_servername)
{   
 CoursesEntries=new Array();
    startActivitySpinner();
  
  var xhr=$.ajax({
                 url: _servername,
                 //type: "GET",
                 dataType:"xml",
                 async:true,
                 data:{},
                 success: function(data)
                 {
                 var xml=$(data);
                 var courses=xml.find("item");
                 $.each(courses,function(i,v){
                         courseEntry = {
                        campus:$(v).find("campus").text(),
                        SC:$(v).find("school_code").text(),
                        DN:$(v).find("department_name").text(),
                        SubC:$(v).find("subject_code").text(),
                        TN:$(v).find("term_name").text(),
                        TC:$(v).find("term_code").text(),
                        CT:$(v).find("course_title").text(),
                        CN:$(v).find("catalog_number").text(),
                        ClsN:$(v).find("class_number").text(),
                        CS:$(v).find("class_section").text(),
                        Units:$(v).find("units").text(),
                        SD:$(v).find("start_date").text(),
                        ED:$(v).find("end_date").text(),
                        ST:$(v).find("start_time").text(),
                        ET:$(v).find("end_time").text(),
                        day:$(v).find("day").text(),
                        IFN:$(v).find("instructor_firstname").text(),
                        ILN:$(v).find("instructor_lastname").text(),
                        BN:$(v).find("building_name").text(),
                        BC:$(v).find("building_code").text(),
                        RN:$(v).find("room_number").text(),
                        ECap:$(v).find("enroll_cap").text(),
                        ETot:$(v).find("enroll_tot").text(),
                              };
                              //alert(direntry);
                              CoursesEntries.push(courseEntry);
                              });
                       console.log(JSON.stringify(CoursesEntries));
                       pageChange("course_listing");
                  },
                 error: function(e)
                 {
                 stopActivitySpinner();
                 alertServerError(e.status);
                 },
                 complete:function()
                 {
                 stopActivitySpinner();
                 }
                 });
  
  
}
/**
 * GetNewsList()
 * <function Displaying Calender List>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message
 * @return {JSON}

*/
function GetNewsList(_servername)
{
    startActivitySpinner();
  
    var feed = new google.feeds.Feed(_servername+"?M="+Math.random());
    feed.setNumEntries(-1);
    feed.setResultFormat(google.feeds.Feed.JSON);
  
    feed.load(function(result) {
              if (!result.error) {
              var entries = []; 
              for (var i = 0; i < result.feed.entries.length; i++) {
              var entry = result.feed.entries[i];
              // Do whatever you want with each entry
              entries.push(entry);
              console.log(JSON.stringify(entries));
              }
              newsDetailList(entries);
              } else {
              // If nos results in the feed
              }   
              });
    
    
}

/**
 * getCalender()
 * <function Displaying Calender List>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message	
 * @return {JSON}
 */
function getCalender(_servername)
{   
    startActivitySpinner();
    $("#NextDay").css({'visibility':'visible'});
    
    $("#PreviousDay").css({'visibility':'visible'});
    NextMonthCounter=0;
    PrevMonthCounter=0;
    
    var Evententries=new Array();
    var weekday=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    var feed = new google.feeds.Feed(_servername);
    feed.setNumEntries(-1);
    feed.setResultFormat(google.feeds.Feed.JSON);
    
    feed.load(function(result) {
              if (!result.error) {       
             // console.log(result.feed.entries.length);
              var Evententries = []; 
              for (var i = 0; i < result.feed.entries.length; i++) {
              var entry = result.feed.entries[i];
              // Do whatever you want with each entry
              Evententries.push(entry);
              }
              console.log(JSON.stringify(Evententries));
              CalEvententries=Evententries;
              var tdyDate = new Date();
              var currDay=tdyDate.getDate();
              if(currDay<10){currDay="0"+currDay};
              var year=tdyDate.getFullYear();
              var month=tdyDate.getMonth();
              CurrentDayID=weekday[tdyDate.getDay()]+""+currDay+""+CurrentMonth[month].substr(0,3)+""+year;
              //console.log(dayId);
              
              fetchEvents(tdyDate);
              //calenderList(Evententries);
              stopActivitySpinner();  
              } else {
              // If nos results in the feed
              }   
              });
/*try{
  var xhr=$.ajax({
                 url: _servername,
                 //type: "GET",
                 dataType:"xml",
                 async:true,
                 data:{},
                 success: function(data)
                 {
                 console.log(data);
                 var xml=$(data);
                 var eventsFeed=xml.find("entry");
                 $.each(eventsFeed,function(i,v){
                         evententry = {
                        title:$(v).find("title").text(),
                        link:$(v).find("link").text(),
                        content:$(v).find("description").text(),
                        publishedDate:$(v).find("published").text(),
                              };
                              //alert(direntry);
                              Evententries.push(evententry);
                              });
                       console.log(JSON.stringify(Evententries));
              CalEvententries=Evententries;
              var tdyDate = new Date();
              var currDay=tdyDate.getDate();
              if(currDay<10){currDay="0"+currDay};
              var year=tdyDate.getFullYear();
              var month=tdyDate.getMonth();
              CurrentDayID=weekday[tdyDate.getDay()]+""+currDay+""+CurrentMonth[month].substr(0,3)+""+year;
              //console.log(dayId);
              
              fetchEvents(tdyDate);
              //calenderList(Evententries);
              stopActivitySpinner();  
              },
              error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   },
                   complete:function()
                   {
                   stopActivitySpinner();
                   }
              });
}catch(E){}*/
}

function GetAtheleteList(_servername)
{   
    startActivitySpinner();
       
        var feed = new google.feeds.Feed(_servername);
        feed.setNumEntries(-1);
        feed.setResultFormat(google.feeds.Feed.JSON);
        
        
        feed.load(function(result) {
        //console.log(JSON.stringify(result));
                  if (!result.error) {       
                  var entries = []; 
                  for (var i = 0; i < result.feed.entries.length; i++) {
                  var entry = result.feed.entries[i];
                  // Do whatever you want with each entry
                  entries.push(entry);
                  }
                  AtheleteDetailList(entries);
                  } 
                  else
                  {
                  
                  setTimeout(stopActivitySpinner(),2000);
                  }  
                  });
        
   }
/**
 * getAlertsJSON()
 * <fetches all messages>
 * @param {string} service Ticket for fetching all messages
 * @param {boolean} checks wheather to show alerts or not
 * @return {json array} recieved msgs.
 */
function TimeoutRefreshAlerts() {
    //alert("_checkMsgCount=" + window.sessionStorage.getItem("_checkMsgCount") + " " + window.localStorage.getItem("LS_autoRefreshTimer")+" "+msgClick);
    
    //if (window.sessionStorage.getItem("_checkMsgCount") == "true") {
        url_service = "/Alerts/AlertsService/AlertsInfoService/inbox";
        return_Where = getAlertsJSON;
        request_MsgST(url_service, return_Where, false);
    //}
    //else {
        //window.sessionStorage.setItem("_checkMsgCount", true);
    //}
}

function getAlertsJSON(_serviceTicket, _silent) {
    
    var url_json = serverNameMsg + '/Alerts/AlertsService/AlertsInfoService/inbox?ticket=' + _serviceTicket + '&pageNum=1&pageSize=10000';
    var xhr = $.ajax(
                     {
                     url: url_json,
                     dataType: 'json',
                     beforeSend: function () {
                     setTimeout(function () {
                                xhr.abort();
                                },
                                globalAjaxTimer);
                     },
                     //async:true,
                     success: function (data) {
                     console.log(JSON.stringify(data));
                     stopActivitySpinner();
                     if (msgClick == true && data.page.length == 0) {
                     navigator.notification.alert("You have no messages at this time.", doNothing, 'My Messages', 'Ok');
                     $(".msgcount").hide();
                     //msgcount = alertsJSON.length;
                     //window.localStorage.setItem("LS_total_Msg", msgcount);
                     }
                     else {
                     var _jsonArr = data;
                     //if (tgtTicket != null)
                     saveAlertsJSON(_jsonArr); // Break execution to stop autorefresh post login if a trace was running
                     /* else
                      clearTimeout(timer);
                      // clear Timer if any exists
                      if (autoRefreshTimer != "manual") {
                      if (timer != null) // controls the autorefresh functionality independently
                      {
                      clearTimeout(timer);
                      autoRefresh();
                      }
                      }
                      else {
                      if (timer != null) {
                      clearTimeout(timer); // kill timer if there was any trace left of it
                      }
                      }*/
                     
                     /* if (window.localStorage.getItem("LS_firstTimeUser") == null) // Clear the form values as they are no longer needed
                      {
                      window.localStorage.setItem("LS_firstTimeUser", "no");
                      $("#loginUsername").val("");
                      $("#loginPassword").val("");
                      }*/
                     }
                     },
                     error: function (response) {
                     stopActivitySpinner();
                     alertServerError(e.status);
                     if ($.mobile.activePage.attr("id") == "mainDashboardPage") {
                     $.mobile.changePage("#loggedInPage");
                     if (window.localStorage.getItem("LS_RememberMe") == "true") {
                     var key = device.uuid;
                     var tempDCryptUname = _usernameEnc;
                     $("#loginUsername").val(tempDCryptUname);
                     $("#loginPassword").val("");
                     }
                     }
                     },
                     compelete: function () {
                     }
                     }
                     );
    
    
}



/**
 * <getCommunities>
 * <fetches all communities groups and tags>
 * @param {string} service Ticket for fetching communities and usergroup
 * @return { none }
 */

function getCommunities(_serviceTicket, _silent)
{
    var server_url= serverNameMsg+"/Alerts/AlertsService/SendersLookup/groups"+"?ticket="+_serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                   showCommunties(data);
				   },
                   error: function(e)
                   {
                   console.log(JSON.stringify(e))
                   stopActivitySpinner();
                   navigator.notification.alert("You are not authorized to send messages at this time.",doNothing,'Cannot Send Message','Ok');
                   
                   //alertServerError(e.status);
                   }
                   });
}


/**
 * <sendToAny_User>
 * <gets permission wheather the user can send msg to individual user>
 * @param {string} service Ticket for getting permission
 * @return { boolean }
 */


function sendToAny_User(_serviceTicket, _silent)
{
    
   	server_url= serverNameMsg+"/Alerts/AlertsService/SendersLookup/sendToAnyUser"+"?ticket="+_serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   if(data == false)
                   { $("#IndividualUsers").hide();
                   $("#IndividualUsersDiv").html("You are not authorized to send messages to individuals.")
                 
                   }
                 else{
                 $("#IndividualUsersDiv").html("<input type='text' autocorrect='off' autocapitalize='off' name='IndividualUsers' id='IndividualUsers' placeholder='Individual Users' maxlength='50' style='margin-top:8px;' data-theme='d'/>");
                 $('#IndividualUsersDiv').trigger('create');
                 }
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
}

function tagsList(_serviceTicket)
{
server_url= serverNameMsg+"/Alerts/AlertsService/SendersLookup/tags"+"?ticket="+_serviceTicket;
//console.log(server_url);
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },

                   success: function(data)
                   {   
                   console.log("Tags List = "+JSON.stringify(data));
                   showTags(data);
                   $.mobile.changePage("#sendMessagePage");
                   $("#communities").selectmenu("refresh");
                   $("#users").selectmenu("refresh");
                   stopActivitySpinner();
                   var p=$("#messageContent").offset();
                   $("#messageContent").css({height:$(document).height()-p.top-80});
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });

}

/**
 * <sendMessage>
 * <send message to groups, usergroup and individual user>
 * @param {int} Id of the community selected
 * @param {int} Id of the user group selected
 * @param {string} username of the individual user
 * @param {string} subject of the msg
 * @param {string} content of the msg
 * @param {string} service Ticket for sending the msg
 * @return { ok if status is true else error if status is false}
 */


function sendMessage(_groupid,_userid,_recipientUser,_subject,_content,_tagID,serviceTicket)
{
    
    var url_json = serverNameMsg+'/Alerts/AlertsService/AnnouncementsServiceForUser/message?ticket='+serviceTicket;
    console.log(url_json);
    
    try{  
    if(_recipientUser == "" && _groupid == false)
    {
        sendTo={recipientUserGroupIds:_userid,contents:_content,subject:_subject,requireAcknowledgement:"false",priority:0};
    }
   else if(_recipientUser == "" && _userid == false)
    {
        sendTo={recipientCommunityIds:_groupid,contents:_content,subject:_subject,requireAcknowledgement:"false",priority:0};
    }
    else if(_userid == false && _groupid == false)
    {
    sendTo={recipientUserScreenNames:_recipientUser,contents:_content,subject:_subject,requireAcknowledgement:"false",priority:0};
    }
    else
    {
      sendTo={recipientUserScreenNames:_recipientUser,recipientCommunityIds:_groupid,recipientUserGroupIds:_userid,contents:_content,subject:_subject,requireAcknowledgement:"false",priority:0};
    }
        console.log(JSON.stringify(sendTo));
        
    startActivitySpinner();
    $.ajax(
           {
           url:url_json,
           dataType:'JSON',
           async:true,
           type:"POST",
           data:sendTo,
           beforeSend:function (){
           // alert(globalAjaxTimer);
           setTimeout(function()
                      {
                      xhr.abort();
                      },globalAjaxTimer);
           },

           success:function(data)
           {
           if(data.status == false)
           {
           navigator.notification.alert(data.response,doNothing,"Error");
           }
           else{
           message_Success();                               
           }
           stopActivitySpinner();
           },
           error:function(response)
           {	
           //alert("error"+JSON.stringify(response));
           alertServerError(e.status);
           stopActivitySpinner();
           },
           compelete:function()
           {
           
           }				   
           });

    }catch(e){alert(e)}
}
/**
 * <acknowledgeConfirm()>
 * <acknowledge the message>
 * @param {int} has the button value which tells wheather the user want to delete the msg or not 
 * @return {boolean}
 */


function acknowledgeConfirm(button)
{
    if(button == 1)
    {
        //startActivitySpinner();
        var url_acknow = serverNameMsg+'/Alerts/AlertsService/AlertsInfoService/acknowledge/'+id_req;
       // console.log("Acknowledge URL"+url_acknow);
        
		var xhr = $.ajax({
                         url:url_acknow,
                         type:"POST",
                         async:true,
                         beforeSend:function()
                         { 
                         setTimeout(function()
                                    {
                                    xhr.abort();
                                    },
                                    globalAjaxTimer);
                         },
                         data:{alertId:id_req},
                         success:function(data)
                         {
                         if (data == 'ok')
                         {
                         removeAckFromGVariable();
                         }
                         else
                         {
                         }	
                         },
                         error:function(response)
                         {
                         navigator.notification.alert("Oops! Acknowledge failed. Please try again later",doNothing,'Error','Ok');
                         //console.log("error"+response.responseText);
                         stopActivitySpinner();
                         }
                         
                         });
        
    }
    else
    {
	    return false;
    }
}

/**
 * <FetchIconUrl()>
 * <request the web-service used to show logged in icons>
 * @param {string} _serviceTicket 
 * @return {boolean}
 */

function FetchIconUrl(_serviceTicket,_silent)
{
    var url_json = serviceName+'/QuickLaunch/api/list/applications'+'?ticket='+_serviceTicket;
    //console.log(url_json);
    var xhr=$.ajax({
                   url: url_json,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                  // console.log(JSON.stringify(data));
                   LoggedObject=data;
                   WebServiceIconGenerate(data);
				   },
                   error: function(e)
                   {  
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });
    
    
    
}

function roleBaseSecurity()
{
   var make_basic_auth = function(user, password) {
    
        var tok = user + ':' + password;
        
        var hash = Base64.encode(tok);
        
        return "Basic " + hash;
        
    };
    
    
    var auth = make_basic_auth('tomcat', 'T37U#d%bWrfbb#8TsEQK-rXNjyjLyTbA');        
    
    
    var key = device.uuid;
    var _userNameEnc = window.localStorage.getItem("LS_usernameEncrypted");
    var tempDCryptUname = _userNameEnc;
    
    
    var url=serverName+"/geis-mycampus/reporting/run/qry?q=select name from role_ where roleId=-1 or roleId in(select roleId from users_roles where userId in(select userId from user_ where screenname='"+tempDCryptUname+"'))or roleId in(select roleId from usergrouprole where userId in(select userId from user_ where screenname='"+tempDCryptUname+"'))or roleId in(select roleId from groups_roles where groupId in(select groupId from users_groups where userId in(select userId from user_ where screenname='"+tempDCryptUname+"')))or roleId in(select roleId from groups_roles where groupId in(select groupId from groups_usergroups where userGroupId in(select userGroupId from users_usergroups where userId in(select userId from user_ where screenname='"+tempDCryptUname+"'))))or roleId in(select roleId from orggrouprole where groupId in(select groupId from groups_usergroups where userGroupId in(select userGroupId from users_usergroups where userId in(select userId from user_ where screenname='"+tempDCryptUname+"'))))or roleId in(select roleId from groups_roles where groupId in(select groupId from group_ where classNameId in(select classNameId from classname_ where value='com.liferay.portal.model.UserGroup')AND classPk in(select userGroupId from users_usergroups where userId in(select userId from user_ where screenname='"+tempDCryptUname+"'))))";
    
   // console.log(decodeURIComponent(url));
    rolesArray=new Array();
    
    $.ajax({
           url : url,
           async : false,
           method : 'GET',
           beforeSend : function(req) {
           req.setRequestHeader('Authorization', auth);},
           success:function(data){
           
           for(var i=0;i<data.result.length;i++)
           {
           
           rolesArray[i]=data.result[i].name.toLowerCase();
           
           }
          
           rolesArray[i]="public";
           //rolesArray[i+1]="administrator";
           console.log(JSON.stringify(rolesArray));
           //WebServiceIconGenerate();
           var tempECryptUname = window.localStorage.getItem("LS_usernameEncrypted");
           var tempECryptPwd = window.localStorage.getItem("LS_passwordEncrypted");
           generateMsgTGT(tempECryptUname,tempECryptPwd,null,null,false);
           },
           error:function(e)
           {
           console.log(JSON.stringify(e));
           }
           
           });
 
}

function BannerHold(_serviceTicket)
{
//url_service="https://mycampus.lipscomb.edu/enmu/services/student/bannerhold";
    startActivitySpinner();
    var server_url=serviceName+"/enmu/services/student/bannerhold"+"?ticket="+_serviceTicket;    
    //console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                   console.log(JSON.stringify(data));     
                   displayHolds(data);
				   },
                   error: function(e)
                   {
                   console.log(JSON.stringify(e));
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });


}

function bannerMidtermGrade(_serviceTicket)
{
startActivitySpinner();
var server_url=serviceName+"/enmu/services/student/bannermidtermgrade"+"?ticket="+_serviceTicket;    
    //console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                  // alert(JSON.stringify(data)); 
                   bannerMTG(data); 
                      
                   
				   },
                   error: function(e)
                   {  
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });



}


function displaymidtermgrade(id)
{
    startActivitySpinner();
    server_url= serviceName+"/enmu/services/student/bannermidtermgrade/termCode="+id;
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   dispmidtergra(data);
                   //stopActivitySpinner();
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
 

}

function BannerTermDown(_serviceTicket)
{
startActivitySpinner();
var server_url=serviceName+"/enmu/services/student/bannertermdropdown"+"?ticket="+_serviceTicket;    
    //console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                   //alert(JSON.stringify(data));     
                   befrMTGdrop(data);
				   },
                   error: function(e)
                   {  
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });

}


function bannerFinalGrade(_serviceTicket)
{
startActivitySpinner();
 var server_url=serviceName+"/enmu/services/student/bannerfinalgrade"+"?ticket="+_serviceTicket;    
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                   //alert(JSON.stringify(data)); 
                   beforeFGprint(data);
                    
                   
				   },
                   error: function(e)
                   {  
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });

}

function BannerTermDownFinalGrades(_serviceTicket)
{
startActivitySpinner();
var server_url=serviceName+"/enmu/services/student/bannerfinalgradetermdropdown"+"?ticket="+_serviceTicket;    
    //console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
				   {   
                   //alert(JSON.stringify(data));     
                   bfreFGDrop(data);
				   },
                   error: function(e)
                   {  
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   });
}

function displayFGgrade(_serviceTicket)
{
  startActivitySpinner();
    server_url= serviceName+"/enmu/services/student/bannerfinalgrade/termCode="+finalGradeTerm+"?ticket="+_serviceTicket;
    console.log(server_url)
      var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   dispFG(data);
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
    

}

function LUClas(serviceTicket)
{
startActivitySpinner();
$(".LUCSubParams").hide();
$(".hideLUC").hide();
server_url= serviceName+"/enmu/services/student/bannertermsForClassLookup"+"?ticket="+serviceTicket;
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   dispLUC(data);
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
    
    
}

function LUCcall(_serviceTicket)
{
    startActivitySpinner();
    server_url= serviceName+"/enmu/services/student/bannergetOtherClassSearchParams/termCode="+LUCterm+"?ticket="+_serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   LUCsucessdata(data);
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   }); 
       
}


function LUCInstructors(_serviceTicket)
{
    startActivitySpinner();
    server_url= serviceName+"/enmu/services/student/bannerlucinstructorsforsubj/termCode="+LUCterm+"&subj="+LUCSubCode+"?ticket="+_serviceTicket;
    console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   LUCInstructorData(data);
                   },
                   error: function(e)
                   {
                   stopActivitySpinner();
                   alertServerError(e.status);
                   }
                   
                   });
    
}




function checksearch(_serviceTicket){
    startActivitySpinner();
    var trmcde = $("#LUC").val();
    var subj = $("#LUCsubject").val();
    var crseNum = $("#coursenum").val()
    var crseTit = $("#title").val()
    var crtfrm = $("#creditfrm").val()
    var crtto = $("#creditto").val()
    var pot = $("#partoftrm").val()
    var ins = $("#slctinstruc").val()
    
    var numbers=/^[0-9]+$/;
    if(!numbers.test(crseNum)){
        navigator.notification.alert("Please enter a valid CRN.",null,'Look Up Classes','Ok');
        stopActivitySpinner();
        return false;
    }
   server_url= serviceName+"/enmu/services/student/bannerclasslookup/termCode="+trmcde+"&subjectCode="+subj+"&courseNumber="+crseNum+"&partTermCode="+pot+"&facultyPidm="+ins+"&courseTitle="+crseTit+"&creditFrom="+crtfrm+"&creditTo="+crtto+"?ticket="+_serviceTicket;
  console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   displayLUCGra(data);
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   }); 
   
    
    /*$("#LUC").val("");
    $("#LUC").selectmenu("refresh");
    $("#LUCsubject").val("");
    $("#LUCsubject").selectmenu("refresh");
    $("#coursenum").val("");
    $("#title").val("");
    $("#creditfrm").val("");
    $("#creditto").val("");
    $("#partoftrm").val("");
    $("#partoftrm").selectmenu("refresh");
    $("#slctinstruc").val("");
    $("#slctinstruc").selectmenu("refresh");*/

}


function registrationClasDropDown(serviceTicket)
{
startActivitySpinner();
server_url= serviceName+"/enmu/services/student/bannertermsForClassLookup"+"?ticket="+serviceTicket;
    console.log(server_url)
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   dispRegistration(data);
                   stopActivitySpinner();
                   },
                   error: function(e)
                   {
                   console.log(JSON.stringify(e))
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   },
                   compelete:function(){
                   
                   stopActivitySpinner();
                   }
                   
                   });
    
    
}


function registrationCalDisplay(_serviceTicket)
{
//alert(">>>>> Inside registrationCalDisplay ")
//alert(">>>>> Current Id : " +id)
startActivitySpinner();
//console.log(">>>ID in Reigstration::"+id);
    regTermDown = regTermDown.replace(/\s/g, '');
    server_url= serviceName+"/enmu/services/student/bannerregistrationstatus/termCode="+regTermDown+"?ticket="+_serviceTicket;
    //server_url= serviceName+"/enmu/services/student/bannerregistrationstatus/termCode=200650";
    
   console.log("Server URL:"+server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   console.log(JSON.stringify(data));
                   registrationSucessData(data);
                   
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   },
                   compelete:function(){
                   
                   stopActivitySpinner();
                   }
                   }); 
       
}



function AccountSummaryTermDown(serviceTicket)
{
startActivitySpinner();
server_url= serviceName+"/enmu/services/student/bannertermswithaccountsummary"+"?ticket="+serviceTicket;
//console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   dispAccountSummaryTerm(data);
                   //dispAccountSummary(data);
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   },
                   compelete:function(){
                   
                   stopActivitySpinner();
                   }
                   
                   });
    
    
}




function bannerAccountSummary(_serviceTicket)
{
server_url= serviceName+"/enmu/services/student/banneraccountsummary/termCode="+accTermDown+"?ticket="+_serviceTicket;
//console.log(server_url);
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   dispAccountSummary(data);
                   
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   },
                   compelete:function(){
                   
                   stopActivitySpinner();
                   }
                   
                   });
    
    
}

function getFlicker()
{

startActivitySpinner();
    
        var feed = new google.feeds.Feed("http://api.flickr.com/services/feeds/photos_public.gne?id=50767319@N00&lang=en-us&format=rss_200");
       
        feed.setNumEntries(-1);
        feed.setResultFormat(google.feeds.Feed.JSON);
        
        
        feed.load(function(result) {
                  if (!result.error) {       
                  var entries = []; 
                  for (var i = 0; i < result.feed.entries.length; i++) {
                  var entry = result.feed.entries[i];
                  // Do whatever you want with each entry
                  entries.push(entry);
                  }
                  showFlickr(entries);
                  alert(JSON.stringify(entries));
                  } 
                  else
                  {
                  //alert(error);
                  setTimeout(stopActivitySpinner(),2000);
                  }  
                  });

}

function weekAtGlanceTermDown(serviceTicket)
{
	server_url= serviceName+"/enmu/services/student/bannertermsStudentHasClasses"+"?ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   console.log(JSON.stringify(data));
                   if(data.termsStudentHasClasses.length == 0)
                   {
                   navigator.notification.alert("You don’t have classes scheduled.",doNothing,'Alert','Ok');
                   stopActivitySpinner();
                   }else{
                   displayWeekAtGlnaceTermDown(data);
                   }
                 //  displayWeekAtGlance(data);
                   },
                   error: function(e)
                   {
                   console.log(JSON.stringify(e));
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
    
}

function weekAtGlance(serviceTicket)
{
    server_url= serviceName+"/enmu/services/student/bannerweekataglance/termCode="+WeekTerm+"?ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                // displayWeekAtGlnaceTermDown(data);
                   displayWeekAtGlance(data);
                   stopActivitySpinner();
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
}

function AcademicTrans(serviceTicket)
{
    
    
    server_url= serviceName+"/enmu/services/student/bannertranscriptdropdown"+"?ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   //console.log(JSON.stringify(data));
                   befreAcademicTranscripts(data);
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
               });
            }

function AcademicCall(_serviceTicket)
{
    server_url= serviceName+"/enmu/services/student/bannertranscripts/levl="+academicTransTerm+"?ticket="+_serviceTicket;
  //server_url="/Users/letsgomo08/Desktop/MYCampusColleges/GCC1/www/Framework/transscript_view.js";
   console.log(server_url);
    startActivitySpinner();
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   dataType:"JSON",
                   async:true,
                   data:{},
                   beforeSend:function (){
                   // alert(globalAjaxTimer);
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   {
                   acadeTrans=data;
                   displayATdata();
                   console.log(JSON.stringify(data));
                   },
                   error: function(e)
                   {   
                   stopActivitySpinner();
                   alertServerError(e.status);                   
                   }
                   
                   });
    
    
}


/**
 * getSportsList()
 * <function Displaying Sports List>
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message 
 * @return {JSON}
 */
function getSportsList(sportAthleticsServerName) {
  localStorage.sportAthleticsServerName = sportAthleticsServerName;
  startActivitySpinner();
  if(sportAthleticsServerName == "http://www.msumustangs.com") {
    sportsList();
  } else if (sportAthleticsServerName == "http://www.beaconsathletics.com") {
    beaconsSportList();    
  }
  
}


function check_push_notification(serviceTicket)
{
    
    server_url= serverName+"/Alerts/AlertsService/AlertsInfoService/preference/ParseDeliveryChannel"+"?ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   //dataType:'json',
                   async:true,
                   data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   
                    //alert("done!"+ geturl.getAllResponseHeaders());
                   //console.log(data);
                   if(data!='true') data='false';
                   CheckControl(data);
                   },
                   error: function(e)
                   { 
                   if(e.status==200){
                   CheckControl("false");}
                   else
                   alertServerError(e.status);                   
                   }
                   
                   });


}


function CheckControl(data)
{

    if(data=='true')
    {
    $("input[name=notification]").attr('checked',true)
    }
    
    else
    {
        $("input[name=notification]").attr('checked',false)
    }
}


function notification_ControlOn(serviceTicket)
{    
    
    server_url= serverName+"/Alerts/AlertsService/AlertsInfoService/preference?key=ParseDeliveryChannel&value=true"+"&ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   //dataType:'json',
                   async:true,
                   data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   console.log("Success On Function");
                   console.log(data);
                   console.log(JSON.stringify(data));
                   },
                   error: function(e)
                   {   
                   alertServerError(e.status);                   
                   }
                   
                   });
}

function notification_ControlOff(serviceTicket)
{
    
    
    server_url= serverName+"/Alerts/AlertsService/AlertsInfoService/preference?key=ParseDeliveryChannel&value=false"+"&ticket="+serviceTicket;
	var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   //dataType:'json',
                   async:true,
                   data:{},
                   beforeSend:function (){
                   setTimeout(function()
                              {
                              xhr.abort();
                              },globalAjaxTimer);
                   },
                   
                   success: function(data)
                   { 
                   console.log("Success off Function");
                   console.log(data);
                   console.log(JSON.stringify(data));
                   },
                   error: function(e)
                   {   
                   alertServerError(e.status);                   
                   }
                   
                   });
}


function getDemoTwitterPageFeeds(_servername)
{
    startActivitySpinner();
    $.ajax({
           url:_servername,
           dataType:'JSON',
           success:function(res)
           {
           
           if(res && res.length)
           {
           console.log(JSON.stringify(res));
           displayDemoTwitterPageFeeds(res);
           }
           else
           {
           setTimeout(stopActivitySpinner(),0);
           navigator.notification.alert("Could not load the twitter feeds. Please try again",null,"Oops!","Ok");
           }
           },
           error:function(err)
           {
           setTimeout(stopActivitySpinner(),0);
           navigator.notification.alert("Could not load the twitter feeds. Please try again",null,"Oops!","Ok");
           }
           });
    
}




