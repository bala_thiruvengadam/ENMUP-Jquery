/*
 Copyright(c) 2012 CampusEAI Consortium.
 http://www.campuseai.org/
 */

/**
 * @fileoverview This file contains all the function calls for display/views to user
 * before the user has logged in.
 */

function preLoginDasdboardIcon()
{
    $(".headerH1Class").html(collegeName);
    
    var navCount=1;
    var i=0;
    var html_view="";
    
    html_view +="<ul id='thelist'>";
    html_view +="<li class='scrollerUlLi'><ul>";
    $.each(UIPreLoginListObject, function(key,val)
           {
           if(val.enabled == true)
           {
           if(i < 9)
           {
           html_view +="<li><div id='"+val.id+"' class='iconsInnerDiv' onclick='preLoggedinPageChange(\""+val.servername+"\", this.id)'><img src='Settings/images/"+val.url+"' /></div>";
           html_view += "<div class='dashboard-txtDiv'>"+val.title+"</div></li>";
           i=i+1;
           
           }
           else
           {
           i = 0;
           navCount=navCount+1;
           html_view +="</ul></li>";
           html_view +="<li class='scrollerUlLi'><ul>";
           html_view +="<li><div  id='"+val.id+"' class='iconsInnerDiv' onclick='preLoggedinPageChange(\""+val.servername+"\", this.id)'><img src='Settings/images/"+val.url+"' /></div>";
           html_view += "<div class='dashboard-txtDiv'>"+val.title+"</div></li>";
           i=i+1;
           }
           }
           });
    html_view +="</ul></li></ul>";
    $("#scroller").html(html_view);
    $("#wrapper").css({'width':wrapperWidth});
    $("#wrapper").css({'height':wrapperHeight});
    
    $(".scrollerUlLi").css({'width':wrapperWidth});
    $(".scrollerUlLi").css({'height':wrapperHeight});
    
    var scrollerWidth=navCount * wrapperWidth;
    var scrollerHeight=wrapperHeight;
    $("#scroller").css({'width':scrollerWidth});
    $("#scroller").css({'height':scrollerHeight});
    $("#scroller ul li ul li").css({'height':$(document).height()*0.20});
    if(iconScroll)
    {
        iconScroll.refresh();
    }
    var html_nav ="<li class='active'>1</li>";
    for(k = 2;k<=navCount;k++)
    {
        html_nav +="<li>"+k+"</li>";
    }
    $("#indicator").html(html_nav);
    
    switch(navCount)
    {
        case 1:{
            $("#navtext").hide();
            break;}
        case 2:{
            var loggednavtextMarginLeft = ($(document).width()-105)/2;
            $("#navtext").show();
            break;
        }
        case 3:{
            var loggednavtextMarginLeft = ($(document).width()-115)/2;
            $("#navtext").show();
            break;
        }
        case 4:{
            var loggednavtextMarginLeft = ($(document).width()-125)/2;
            $("#navtext").show();
            break;
        }
        case 5:{
            var loggednavtextMarginLeft = ($(document).width()-135)/2;
            $("#navtext").show();
            break;
        }
        case 6:{
            var loggednavtextMarginLeft = ($(document).width()-145)/2;
            $("#navtext").show();
            break;
        }
            
    }
    
    
    $("#navtext").css({'margin-left':loggednavtextMarginLeft});
    
    setTimeout($.mobile.changePage("index.html#mainDashboardPage"),1000);
    $(".hideFav").hide();
}





function newsDetailList(entries)

{
    var html_view ="";
    var html_view = "<ul id='news_Articles_List'  data-role='listview' data-theme='d'>";
    //var length = newsjson.length;
    
    for(i=0;i<entries.length;i++)
    {
        
        var title=encodeURIComponent(entries[i].title);
        var date=encodeURIComponent(entries[i].publishedDate);
        var content=encodeURIComponent(entries[i].content);
        var link=encodeURIComponent(entries[i].link);
        
        var inputDate = entries[i].publishedDate.split(" ");
        
        var date;
        switch(inputDate[0]){
            case 'Mon,':{
                date="Monday,"
                break;
            }
            case 'Tue,':{
                date="Tuesday,"
                break;
            }
            case 'Wed,':{
                date="Wednesday,"
                break;
            }
            case 'Thu,':{
                date="Thursday,"
                break;
            }
            case 'Fri,':{
                date="Friday,"
                break;
            }
            case 'Sat,':{
                date="Saturday,"
                break;
            }
                
            case 'Sun,':{
                date="Sunday,"
                break;
            }
        }
        date =date+" "+inputDate[2]+" "+inputDate[1]+", "+getFormattedTime(inputDate[4].substr(0,5).replace(":",""));
        var date1=encodeURIComponent(date);
        html_view += "<li data-theme='d' data-icon='false'><a href='javascript:void(0)' id='"+i+"' onclick=RSSNewsDesc(\""+title+"\",\""+date1+"\",\""+content+"\",\""+link+"\",this.id) style='white-space:normal'><div class='news-articles-heading-left' >"+entries[i].title+"</div><span class='news-articles-subheading-left'>";
        
        html_view += "Posted on: "+date+"</span></a></li>";
        
        
    }
    
    html_view += "</ul>";
    
    
    
    $("#articles_News_Display").html(html_view);
    
    setTimeout("listRefreshHelper('news_Articles_List')",100);
    $.mobile.changePage("#news_Articles",{transition:"none"});
    
    setTimeout(stopActivitySpinner(),2000);
    
}

function RSSNewsDesc(title,date,content,link,id)
{
    try{
        title=decodeURIComponent(title);
        date=decodeURIComponent(date);
        content=decodeURIComponent(content);
        var link=decodeURIComponent(link);
        var inputDate = date;
        
        
        var newsdetailshead = title+"<div style='border-bottom:1px solid silver;text-align:left;letter-spacing:1px;color:#006633;font-size:13px;font-weight:normal;'>"+inputDate;
        
        
        newsdetailshead += "</div>";
        
        
        
        $("#articlesDetailsHeading").html(newsdetailshead);
        
        var newsdetails = "";
        
        
        newsdetails +="<div class='articles-body-details' style='text-align:justify;padding-bottom:10px;text-shadow:none;font-size:16px;line-height:21px'>"+content+"</div>";
        
        newsdetails +="<div class='ui-bar ui-bar-f' style='margin:0 0 0 10%;width:70%;text-align:center' onclick='openWebpage(\""+link+"\")'>Visit Website</div>";
        
        
        $("#Indivisualarticles_News_Display").html(newsdetails);
        
        $.mobile.changePage("#news_ArticlesDetailsPage");
        
        
        
        
    }catch(e){}
    
}
/**
 * calenderList()
 * <function Displaying Calender>
 * @param {string} Results Variable holding JSON
 * @param {string} strMsg Error Message
 * @return {boolean}
 */
function calenderList(Eventsentries)

{
    
    var html_view ="";
    var html_view = "<ul id='ul_Calendar_List'  data-role='listview' data-theme='d'>";
    
    
    //var length = newsjson.length;
    
    for(i=0;i<Eventsentries.length;i++)
    {
        var title=encodeURIComponent(Eventsentries[i].title);
        var date=encodeURIComponent(Eventsentries[i].publishedDate);
        var content=encodeURIComponent(Eventsentries[i].content);
        var link=encodeURIComponent(Eventsentries[i].link);
        html_view += "<li data-theme='d' data-icon='false'><a href='javascript:void(0)' id='"+i+"' onclick=RSSEventsDesc(\""+title+"\",\""+date+"\",\""+content+"\",\""+link+"\",this.id) style='white-space:normal'><div class='news-articles-heading-left' >"+Eventsentries[i].title+"</div><span class='news-articles-subheading-left'>";
        
        
        var inputDate = new Date(Eventsentries[i].publishedDate);
        
        
        html_view += "Event starts at: "+inputDate+"</span></a></li>";
        
    }
    
    html_view += "</ul>";
    
    
    
    $("#calender_List_Display").html(html_view);
    setTimeout("listRefreshHelper('ul_Calendar_List')",100);
    
    $.mobile.changePage("#calendar_List",{transition:"none"});
    setTimeout(stopActivitySpinner(),2000);
    
}

function RSSEventsDesc(title,date,content,link,id)
{
    try{
        title=decodeURIComponent(title);
        date=decodeURIComponent(date);
        content=decodeURIComponent(content);
        var link=decodeURIComponent(link);
        
        var inputDate = date;
        
        var Eventdetailshead = title+"<div style='border-bottom:1px solid silver;text-align:left;letter-spacing:1px;color:#006633;font-size:13px;font-weight:normal;'>"+inputDate+"";
        
        Eventdetailshead += "</div>";
        
        
        
        $("#EventDetailsHeading").html(Eventdetailshead);
        
        var eventdetails = "";
        
        //newsdetails +="<div class='articles-body-image'><img src=images/"+newsjson[0].IMAGE+".jpg width='98%'/></div>";
        
        eventdetails +="<div class='articles-body-details'>"+content+"</div>";
        
        eventdetails +="<div class='ui-bar ui-bar-f' style='margin:15px 0 0 10%;width:70%;text-align:center' onclick='openWebpage(\""+link+"\")'>See Full Event</div>";
        
        
        
        $("#event_details_container").html(eventdetails);
        
        $.mobile.changePage("#calenderEventDetails");
        
        
    }catch(e){}
    
}





function AtheleteDetailList(entries)

{
    
    var html_view ="";
    var html_view = "<ul id='Athelete_Articles_List'  data-role='listview' data-theme='d'>";
    //var length = newsjson.length;
    
    for(i=0;i<entries.length;i++)
    {
        
        var title=encodeURIComponent(entries[i].title);
        var date=encodeURIComponent(entries[i].publishedDate);
        var content=encodeURIComponent(entries[i].content);
        var link=encodeURIComponent(entries[i].link);
        html_view += "<li data-theme='d' data-icon='false'><a href='javascript:void(0)' id='"+i+"' onclick=RSSSAtheleteNewsDesc(\""+title+"\",\""+date+"\",\""+content+"\",\""+link+"\",this.id) style='white-space:normal'><div class='news-articles-heading-left' >"+entries[i].title+"</div><span class='news-articles-subheading-left'>";
        var inputDate = entries[i].publishedDate;
        
        
        html_view += "Events Start at: "+inputDate+"</span></a></li>";
        
        
    }
    
    html_view += "</ul>";
    
    
    
    $("#Athelete_News_Display").html(html_view);
    setTimeout("listRefreshHelper('Athelete_Articles_List')" );
    $.mobile.changePage("#AtheleteNews",{transition:"none"});
    setTimeout(stopActivitySpinner(),2000);
    
}

function RSSSAtheleteNewsDesc(title,date,content,link,id)
{
    try{
        title=decodeURIComponent(title);
        date=decodeURIComponent(date);
        content=decodeURIComponent(content);
        var link=decodeURIComponent(link);
        var inputDate = date;
        
        
        var newsdetailshead = title+"<div style='border-bottom:1px solid silver;text-align:left;letter-spacing:1px;color:#999999;font-size:13px;font-weight:normal;'>"+inputDate+"";
        
        
        newsdetailshead += "</div>";
        
        
        
        $("#AtheleteDetailsHeading").html(newsdetailshead);
        
        var newsdetails = "";
        
        
        newsdetails +="<div class='articles-body-details' style='text-align:left;padding-bottom:10px;text-shadow:none;font-size:16px;line-height:21px'>"+content+"</div>";
        
        newsdetails +="<div class='ui-bar ui-bar-f' style='margin:0 0 0 10%;width:70%;text-align:center' onclick='openWebpage(\""+link+"\")'>View Original Article</div>";
        
        
        $("#AtheleteIndividual_News_Display").html(newsdetails);
        
        $.mobile.changePage("#AtheleteNewsDetailsPage");
        
        
        
        
    }catch(e){}
    
}


/**
 * displayEmergencyPage()
 * <function Displaying Emergency Page>
 * @param {string} strMsg Error Message
 * @return {none}
 */
function displayEmergencyPage()
{
    var EmregencyNo = "";
    EmregencyNo += "<li class='ui-btn ui-bar-f' data-role='list-divider' style='font-size:16px;'>Phone Numbers</li>";
    $.each(UIEmergencyObject, function(key, val)
           {
           if(val.enabled == true)
           {
           teleNo = val.number;
           //teleNo = teleNo.replace(')','-');
           //teleNo = teleNo.replace('(','');
           EmregencyNo += "<li data-theme='d'><a href='tel:"+teleNo+"' ><label class='modifiedH3text' >"+val.name+"</label><p class='modifiedH4text'>"+val.number+"</p></a></li>";
           }
           });
    $("#ul_emergency_Numbers").html(EmregencyNo);
    setTimeout("listRefreshHelper('ul_emergency_Numbers')",10);
    $.mobile.changePage("#emergency_Numbers");
    
}


function displayImpEmergencyPage()
{
    var ImpEmregencyNo = "";
    ImpEmregencyNo += "<li class='ui-btn ui-bar-f' data-role='list-divider' style='font-size:16px;'>Important Numbers</li>";
    $.each(UIImportantObject, function(key, val)
           {
           if(val.enabled == true)
           {
           teleNo = val.number;
           //teleNo = teleNo.replace(')','-');
           //teleNo = teleNo.replace('(','');
           ImpEmregencyNo += "<li data-theme='d'><a href='tel:"+teleNo+"' ><label class='modifiedH3text' >"+val.name+"</label><p class='modifiedH4text'>"+val.number+"</p></a></li>";
           }
           });
    $("#ul_Imp_Numbers").html(ImpEmregencyNo);
    setTimeout("listRefreshHelper('ul_Imp_Numbers')",10);
    $.mobile.changePage("#imp_Numbers");
}

/**
 * displayAllAlerts()
 * Display the lst of all the alerts from the saved Alerts JSON to the user
 * @param {string} strValid Valid Input Characters
 * @param {string} strMsg Error Message
 * @return {boolean}
 */

function displayAllAlerts(_overRidePageChange) {
    
    
    var html_view = "";
    
    if (alertsJSON.page.length == 0) {
        //$("#ul_first").hide();
        $("#myMessagesDisplay").html("");
        
        $("#AlertsNoContent").show();
        $(".msgcount").hide();
    }
    else {
        var processedMsg=0;
        var sentMsgs=0;
        var viewedMsgs=0;
        var acknowledgeMsgs=0;
        var expiredFailed=0;
        //html_view += "<ul id='ul_first' data-role='listview' data-theme='d'>";
        for (var i = 0; i < alertsJSON.page.length; i++) {
            var dateDisplay = alertsJSON.page[i].date;
            var sourceDisplay = alertsJSON.page[i].source;
            
            switch(alertsJSON.page[i].status){
                case 0:{
                    processedMsg=processedMsg+1;
                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#E6E6E6),to(#E6E6E6));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[i].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[i].id+"\",\"NotViewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[i].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[i].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                    break;
                }
                case 1:{
                    sentMsgs=sentMsgs+1;
                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#E6E6E6),to(#E6E6E6));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[i].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[i].id+"\",\"NotViewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[i].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[i].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                    break;
                }
                case 2:{
                    viewedMsgs=viewedMsgs+1;
                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[i].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[i].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[i].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[i].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                    break;
                }
                case 3:{
                    acknowledgeMsgs=acknowledgeMsgs+1;
                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[i].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[i].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[i].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[i].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                    break;
                }
                default:{
                    expiredFailed=expiredFailed+1;
                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[i].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[i].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[i].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[i].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                }
            }
            
        }
        //html_view += "</ul>";
        
        newMsgs = (alertsJSON.page.length-(viewedMsgs+acknowledgeMsgs+expiredFailed));
        //alert(newMsgs);
        if(newMsgs <= 0){$(".msgcount").hide();
            $(".msgcount").html(0);}
        else{

            if (newMsgs <= 0) {
                $(".msgcount").hide();
                $(".msgcount").html(0);
            } else {
                if (newMsgs > oldMsgs) {
                    oldMsgs = newMsgs;
                    $(".msgcount").html(newMsgs);
                    notifyUser();
                }
            }
            
        }
        $("#AlertsNoContent").hide();
        //$("#ul_first").show();
        $("#myMessagesDisplay").html(html_view);
        setTimeout("listRefreshHelper('ul_first')", 10);
        
    }
    window.sessionStorage.setItem("_checkMsgCount",false);
    setTimeout("listRefreshHelper('ul_first')", 10);
    $.mobile.changePage("#myMessagesPage", { transition: "none" });
    
    
    setTimeout("listRefreshHelper('total_Msg')", 10);
    stopActivitySpinner();
    
}


/**
 * <WebServiceIconGenerate>
 * <displays all the icons in the after logging in>
 * @param {json} contains json for showing icons after logging in
 * @return {none}
 */
function WebServiceIconGenerate()
{
    try{
        $("#loggedscroller").html("");
        var count=0;
        lognavCount=1;
        var found;
        var html_view="";
        html_view +="<ul id='thelist'>";
        html_view +="<li class='scrollerUlLi'><ul>";
        $.each(ThreeDLoggedListObject, function(key,val)
               {
               var rolesString=val.rolesAllowed.toLowerCase().split(',');//.split(",");
               found="false";
               for(k=0;k<rolesArray.length;k++)
               {
               if(rolesString.indexOf(rolesArray[k]) != -1)
               {
               found="true";
               break;
               }
               }
               
               if(found == "true")
               {
               
               //alert(val.id);
               if(count < 9)
               {
               
               html_view += "<li><div class='iconsInnerDiv addToFavHandler' id="+val.id+" onclick='LoggedinHomePageChange(\""+val.servername+"\",this.id)'><img src='"+val.imageUrl+"' /></div>";
               html_view += "<div class='dashboard-txtDiv'>"+val.description+"</div></li>";
               //$("#"+val.id).die();
               // $("#"+val.id).live("click",function(){LoggedinHomePageChange(val.servername,val.id)});
               count=count+1;
               }
               else
               {
               count = 0;
               lognavCount=lognavCount+1;
               html_view +="</ul></li>";
               html_view +="<li class='scrollerUlLi'><ul>";
               html_view += "<li><div class='iconsInnerDiv addToFavHandler' id="+val.id+" onclick='LoggedinHomePageChange(\""+val.servername+"\",this.id)'><img src='"+val.imageUrl+"' /></div>";
               html_view += "<div class='dashboard-txtDiv'>"+val.description+"</div></li>";
               //$("#"+val.id).die();
               //$("#"+val.id).live("click",function(){LoggedinHomePageChange(val.servername,val.id)});
               count=count+1;
               }
               
               }
               });
        html_view +="</ul></li></ul>";
        
        $("#loggedscroller").html(html_view);
        
        $("#loggedwrapper").css({'width':wrapperWidth});
        $("#loggedwrapper").css({'height':wrapperHeight});
        
        $(".scrollerUlLi").css({'width':wrapperWidth});
        $(".scrollerUlLi").css({'height':wrapperHeight});
        
        var scrollerWidth=lognavCount * wrapperWidth;
        var scrollerHeight=wrapperHeight;
        $("#loggedscroller").css({'width':scrollerWidth});
        $("#loggedscroller").css({'height':scrollerHeight});
        $("#loggedscroller ul li ul li").css({'height':$(document).height()*0.24});
        var html_nav = "<ul id='loggedindicator'>";
        html_nav +="<li class='active'>1</li>";
        for(var k = 2;k<=lognavCount;k++)
        {
            html_nav +="<li>"+k+"</li>";
        }
        html_nav +="</ul>";
        var loggednavtextWidth = (8 * lognavCount);
        var loggednavtextMarginLeft = ($(document).width()-110)/2;
        // $("#loggednavtext").css({'width':loggednavtextWidth});
        $("#loggednavtext").css({'margin-left':loggednavtextMarginLeft});
        
        switch(lognavCount)
        {
            case 1:{
                $("#loggednavtext").hide();
                break;}
            case 2:{
                var loggednavtextMarginLeft = ($(document).width()-105)/2;
                $("#loggednavtext").show();
                break;
            }
            case 3:{
                var loggednavtextMarginLeft = ($(document).width()-115)/2;
                $("#loggednavtext").show();
                break;
            }
            case 4:{
                var loggednavtextMarginLeft = ($(document).width()-125)/2;
                $("#loggednavtext").show();
                break;
            }
            case 5:{
                var loggednavtextMarginLeft = ($(document).width()-135)/2;
                $("#loggednavtext").show();
                break;
            }
            case 6:{
                var loggednavtextMarginLeft = ($(document).width()-145)/2;
                $("#loggednavtext").show();
                break;
            }
                
        }
        
        
        $("#loggednavtext").css({'margin-left':loggednavtextMarginLeft});
        
        
        $("#loggednavtext").html(html_nav);
        
        try{if(loginiconScroll){loginiconScroll.destroy();}}catch(e){}
        try{if(typeof loginiconScroll == undefined){loginiconScroll.destroy();}}catch(e){}
        
        loginiconScroll = new iScroll('loggedwrapper', {
                                      snap: true,
                                      momentum: false,
                                      hScrollbar: false,
                                      onScrollEnd: function () {
                                      document.querySelector('#loggedindicator > li.active').className = '';
                                      document.querySelector('#loggedindicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
                                      }
                                      });
        
        
        //call to count number of messages
        
       // $(".addToFavHandler").bind('taphold',addToFavoriteConfirm);
        
        
        
        if(localStorage['favInfo']!=null){
            var storedData=JSON.parse(localStorage['favInfo']);
            /*checking existing fav starts*/
            for(i=0;i<storedData.items.length;i++){
                
                $("#"+storedData.items[i].id+"Heart").attr("src","images/star-activered.png");
                
            }
        }
        
        $(".hideFav").show();
        if($(".ui-page-active").attr("id") != "loggedInDashboardPage" )
        {
            $.mobile.changePage("#loggedInDashboardPage");
        }
        
        //  mergingJSON();
        setTimeout(function(){stopActivitySpinner();},4000);
        
        
    }catch(e){alert(e)}
}

function mergingJSON()
{
    
    $.each(ThreeDLoggedListObject, function(key,val)
           {
           LoggedObject.applicationDefinitions.push({description: val.description, id:val.id,imageUrl: val.imageUrl,roles:val.roles});
           });
    //console.log(JSON.stringify(LoggedObject));
    
}



/**
 * <displayIndividualJSON>
 * <displays individual message>
 * @param {int} Id of the particular message
 * @return {none}
 */

function displayIndividualJSON(_id_req,check)
{

    id_req = _id_req;
    var len = alertsJSON.page.length;
    
    for(var i=0;i<len;i++)
    {
        if(alertsJSON.page[i].id == id_req)
        {
            //alert(JSON.stringify(alertsJSON));
            var hid_subject = alertsJSON.page[i].subject;
            var hid_source = alertsJSON.page[i].source;
            var hid_recipient = alertsJSON.page[i].recipient;
            //Date Display Change
            var hid_date = alertsJSON.page[i].date;
            var hid_contents = alertsJSON.page[i].contents;
            
            $("#msg_subject").html(hid_subject);
            $("#source").html(hid_source);
            $("#date").html(hid_date);
            
            $("#msg_Content").html(hid_contents);
            console.log($("#msg_Content").html());
            // Change the Display Page
            $.mobile.changePage("#messages_Details_Page");
            
            if(check == "NotViewed"){
                url_service="/Alerts/AlertsService/AlertsInfoService/viewed/"+id_req;
                return_Where=msgViewed;
                request_MsgST(url_service,return_Where,false);
            }
            
            
            
            
            break;
        }
    }
    
}

function msgViewed(ST)
{
    
    $.ajax(
           {
           url:"https://my.enmu.edu/Alerts/AlertsService/AlertsInfoService/viewed/"+id_req+"?ticket="+ST,
           dataType:'JSON',
           type:"POST",
           data:{alertId:id_req},
           beforeSend:function (){
           // alert(globalAjaxTimer);
           setTimeout(function()
                      {
                      xhr.abort();
                      },globalAjaxTimer);
           },
           
           success:function(data)
           {
           console.log(JSON.stringify(data));
           },
           error:function(response)
           {
           //alert("error"+JSON.stringify(response));
           alertServerError(e.status);
           stopActivitySpinner();
           },
           compelete:function()
           {
           
           }
           });
    oldMsgs=0;
    setTimeout(function() {
               url_service = "/Alerts/AlertsService/AlertsInfoService/inbox";
               return_Where = showAlertMsgs;
               request_MsgST(url_service, return_Where, false);
               },3000);
}


function showAlertMsgs(_serviceTicket, _silent) {
        
        var url_json = serverNameMsg + '/Alerts/AlertsService/AlertsInfoService/inbox?ticket=' + _serviceTicket + '&pageNum=1&pageSize=10000';
        var xhr = $.ajax(
                         {
                         url: url_json,
                         dataType: 'json',
                         beforeSend: function () {
                         setTimeout(function () {
                                    xhr.abort();
                                    },
                                    globalAjaxTimer);
                         },
                         //async:true,
                         success: function (data) {
                         console.log(JSON.stringify(data));
                         stopActivitySpinner();
                         if (msgClick == true && data.page.length == 0) {
                         navigator.notification.alert("You have no messages at this time.", doNothing, 'My Messages', 'Ok');
                         $(".msgcount").hide();
                         //msgcount = alertsJSON.length;
                         //window.localStorage.setItem("LS_total_Msg", msgcount);
                         }
                         else {
                         var _jsonArr = data;
                         
                         countMsgs(_jsonArr); // Break execution to stop autorefresh post login if a trace was running
                         console.log(_jsonArr.page.length);
                         }
                         },
                         error: function (response) {
                         stopActivitySpinner();
                         alertServerError(e.status);
                         if ($.mobile.activePage.attr("id") == "mainDashboardPage") {
                         $.mobile.changePage("#loggedInPage");
                         if (window.localStorage.getItem("LS_RememberMe") == "true") {
                         var key = device.uuid;
                         var tempDCryptUname = _usernameEnc;
                         $("#loginUsername").val(tempDCryptUname);
                         $("#loginPassword").val("");
                         }
                         }
                         },
                         compelete: function () {
                         }
                         }
                         );

}

function countMsgs(_jsonArr) {
    var html_view="";
        var dOld;
        var dNew;
        
        if (_jsonArr.length > 0) {
            dNew = _jsonArr[_jsonArr.length - 1].date;
        }
        
        if (lastAlertTS == null) {
            dOld = window.localStorage.getItem("LS_lastAlertTS");
        }
        else {
            dOld = lastAlertTS;
        }
        
        alertsJSON = _jsonArr; // Save the value for the Display function
        window.localStorage.setItem("LS_lastAlertTS", dNew);
        lastAlertTS = dNew;
    
        if (window.sessionStorage.getItem("_checkMsgCount") == "true") {
            
            if (alertsJSON.page.length == 0) {
                $("#myMessagesDisplay").html("");
                $(".msgcount").hide();
                msgcount = alertsJSON.page.length;
                window.localStorage.setItem("LS_total_Msg", msgcount);
            }
            else {
                try{$(".msgcount").show();
                    if (msgClick == false) {
                        var processedMsg=0;
                        var sentMsgs=0;
                        var viewedMsgs=0;
                        var acknowledgeMsgs=0;
                        var expiredFailed=0;
                        for(var k=0;k<alertsJSON.page.length;k++){
                            var dateDisplay = alertsJSON.page[k].date;
                            var sourceDisplay = alertsJSON.page[k].source;
                            switch(alertsJSON.page[k].status){
                                case 0:{
                                    processedMsg=processedMsg+1;
                                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#E6E6E6),to(#E6E6E6));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[k].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[k].id+"\",\"NotViewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[k].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[k].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                                    break;
                                }
                                case 1:{
                                    sentMsgs=sentMsgs+1;
                                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#E6E6E6),to(#E6E6E6));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[k].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[k].id+"\",\"NotViewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[k].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[k].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                                    break;
                                }
                                case 2:{
                                    viewedMsgs=viewedMsgs+1;
                                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[k].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[k].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[k].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[k].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                                    break;
                                }
                                case 3:{
                                    acknowledgeMsgs=acknowledgeMsgs+1;
                                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[k].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[k].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[k].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[k].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                                    break;
                                }
                                default:{
                                    expiredFailed=expiredFailed+1;
                                    html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#fff),to(#fff));color:#fff;padding:10px 5px;border-bottom:1px solid #000;color:#000;' id='list1_" + alertsJSON.page[k].id + "' onclick='displayIndividualJSON(\""+alertsJSON.page[k].id+"\",\"Viewed\")'><div style='float:left;width:85%;'><b style='padding:0;margin:0'>" + alertsJSON.page[k].subject + "</b><p style='padding:0;margin:0'> " + alertsJSON.page[k].source + " "+dateDisplay+"</p></div><div style='float:right;text-align:right;margin-top:9px;'><div class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div></div><div style='clear:both;'></div></div>";
                                }
                            }
                        }
                        
                        newMsgs = (alertsJSON.page.length-(viewedMsgs+acknowledgeMsgs+expiredFailed));
                        
                        if (newMsgs <= 0) {
                            $(".msgcount").hide();
                            $(".msgcount").html(0);
                        } else {
                            //alert(newMsgs+" "+oldMsgs);
                            if (newMsgs > oldMsgs) {
                                oldMsgs = newMsgs;
                                $(".msgcount").html(newMsgs);
                                //notifyUser();
                            }
                        }
                    }
                    else {
                        $(".msgcount").hide();
                    }
                    
                }catch(e){alert(e)}
                $("#AlertsNoContent").hide();
                //$("#ul_first").show();
                $("#myMessagesDisplay").html(html_view);
            }
        }
        else {
            
            displayAllAlerts(false);
           
        }
}
/**
 * <showCommunities>
 * <display all communities>
 * @param {json array} json array of communities and user groups
 * @return {none}
 */

function showCommunties(results)
{
    console.log(JSON.stringify(results));
    var communitesGroups ="<option value=''>Select a Community</option>";
    var usersGroups ="<option value=''>Select a Group</option>";
    //communitiesGroups="<option>Select communities</option>";
    for(var i=0;i<results.length;i++)
    {
        
        if(results[i].groupType == "Community")
        {
            communitesGroups+="<option value="+results[i].originalGroupId+">"+results[i].originalGroupName+"</option>";
        }
        else
        {
            usersGroups+="<option value="+results[i].originalGroupId+">"+results[i].originalGroupName+"</option>";
        }
    }
    console.log(usersGroups);
    $("#communities").html(communitesGroups);
    $("#users").html(usersGroups);
    
    generalShowHide('usersDiv','communitiesDiv','IndividualUsersDiv','tagsDiv');
    
    url_service= "/Alerts/AlertsService/SendersLookup/sendToAnyUser";
    return_Where=sendToAny_User;
    generateMsgST(tempECryptUname,tempECryptPwd,url_service,return_Where,false);
    
    getTagList();
    
    
}

function getTagList()
{
    url_service= "/Alerts/AlertsService/SendersLookup/tags";
    return_Where=tagsList;
    generateMsgST(tempECryptUname,tempECryptPwd,url_service,return_Where,false);
}

function showTags(results)
{
    var tagList="";
    var tagList ="<option value=''>Select a tag</option>";
    for(var i=0;i<results.length;i++)
    {
        tagList+="<option value="+results[i].tagId+">"+results[i].tagName+"</option>";
        
    }
    $("#tags").html(tagList);
}


/**
 * <getFavorite>
 * <change page to favorites>
 * @return {none}
 */


function getFavorite()
{
    $.mobile.changePage("#favorites");
    
    checkFavorite();
}


/**
 * <checkFavorite>
 * <display list of all favorite items>
 * @return {none}
 */

function checkFavorite()
{
    try{
        if(localStorage['favInfo']!=null){
            if(FaviconScroll){FaviconScroll.destroy();}
            //console.log(JSON.stringify(localStorage['favInfo']));
            var favnavCount=1;
            var k=0;
            var html_view="";
            
            html_view +="<ul id='thelist'>";
            html_view +="<li class='scrollerUlLi'><ul>";
            var storedData=JSON.parse(localStorage['favInfo']);
            for(i=0;i<storedData.items.length;i++)
            {
                
                if(k < 9)
                {
                    html_view += "<li><div class=removeFavHandler id="+storedData.items[i].id+" onclick='LoggedinHomePageChange(\""+storedData.items[i].roles+"\",this.id)'><img src='"+storedData.items[i].imageUrl+"' /></div>";
                    html_view += "<div class='dashboard-txtDiv'>"+storedData.items[i].description+"</div></li>";
                    k=k+1;
                    
                }
                else
                {
                    k = 0;
                    favnavCount=favnavCount+1;
                    html_view +="</ul></li>";
                    html_view +="<li class='scrollerUlLi'><ul>";
                    html_view += "<li><div class=removeFavHandler id="+storedData.items[i].id+" onclick='LoggedinHomePageChange(\""+storedData.items[i].roles+"\",this.id)'><img src='"+storedData.items[i].imageUrl+"' /></div>";
                    html_view += "<div class='dashboard-txtDiv'>"+storedData.items[i].description+"</div></li>";
                    k=k+1;
                }
                
            }
            html_view +="</ul></li></ul>";
            
            
            
            $("#FavLoggedScroller").html(html_view);
            $("#FavLoggedWrapper").css({'width':wrapperWidth});
            $("#FavLoggedWrapper").css({'height':wrapperHeight});
            
            $(".scrollerUlLi").css({'width':wrapperWidth});
            $(".scrollerUlLi").css({'height':wrapperHeight});
            
            var scrollerWidth=favnavCount * wrapperWidth;
            var scrollerHeight=wrapperHeight;
            $("#FavLoggedScroller").css({'width':scrollerWidth});
            $("#FavLoggedScroller").css({'height':scrollerHeight});
            $("#FavLoggedScroller ul li ul li").css({'height':$(document).height()*0.20});
            
            var html_nav ="<li class='active'>1</li>";

            for(var a = 2;a<=favnavCount;a++)
            {
                html_nav +="<li>"+a+"</li>";
            }
            $("#Favindicator").html(html_nav);
            
            switch(favnavCount)
            {
                case 1:{
                    $("#FavLoggednavtext").hide();
                    break;}
                case 2:{
                    var loggednavtextMarginLeft = ($(document).width()-105)/2;
                    $("#FavLoggednavtext").show();
                    break;
                }
                case 3:{
                    var loggednavtextMarginLeft = ($(document).width()-115)/2;
                    $("#FavLoggednavtext").show();
                    break;
                }
                case 4:{
                    var loggednavtextMarginLeft = ($(document).width()-125)/2;
                    $("#FavLoggednavtext").show();
                    break;
                }
                case 5:{
                    var loggednavtextMarginLeft = ($(document).width()-135)/2;
                    $("#FavLoggednavtext").show();
                    break;
                }
                case 6:{
                    var loggednavtextMarginLeft = ($(document).width()-145)/2;
                    $("#FavLoggednavtext").show();
                    break;
                }
                    
            }
            
            
            $("#FavLoggednavtext").css({'margin-left':loggednavtextMarginLeft});
            
            
            //$(".removeFavHandler").bind('taphold',addToFavoriteConfirm);
            
            if(favnavCount == 1){ FaviconScroll = new iScroll('FavLoggedWrapper',{
                                                              snap: true,
                                                              momentum: false,
                                                              hScrollbar: false,
                                                              vScrollbar: false });
                
            }else{
                FaviconScroll = new iScroll('FavLoggedWrapper', {
                                            snap: true,
                                            momentum: false,
                                            hScrollbar: false,
                                            onScrollEnd: function () {
                                            document.querySelector('#Favindicator > li.active').className = '';
                                            document.querySelector('#Favindicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
                                            }
                                            });
            }
            
            
            
        }
        
    }catch(e){}
    
    
}


function displayHolds(data)
{
    
    try{//console.log(JSON.stringify(data));
        var hasHold;
        if(data.hold.length == 0)
        {
            navigator.notification.alert("Good news! You currently have no holds on your account.",doNothing,'Holds','Ok');
        }
        else{
            
            var html_view="";
            html_view ="<ul  id=holdUL data-role='listview' data-theme='c' data-inset='true'>";
            for(i=0;i<data.hold.length;i++)
            {
                
                html_view +="<li data-role='list-divider' class='ui-bar-f'>Hold Information</li>";
                
                html_view +="<li>Hold Initiated By:<span class='custom-right-span custom-color-right-span'>"+data.hold[i].USERNAME+"</span></li>";
                
                html_view +="<li>Code:<span class='custom-right-span custom-color-right-span'>"+data.hold[i].HOLD_CODE+"</span></li>";
                
                
                html_view +="<li>Start Date:<span class='custom-right-span custom-color-right-span'>"+data.hold[i].HOLD_START_DATE+"</span></li>";
                
                html_view +="<li>End Date:<span class='custom-right-span custom-color-right-span'>"+data.hold[i].HOLD_END_DATE+"</span></li>";
                
                html_view +="<li>Reason Code:<span class='custom-right-span custom-color-right-span'>"+data.hold[i].REASON+"</span></li>";
                
                html_view +="<li>Amount Owed:<span class='custom-right-span custom-color-right-span'>$"+data.hold[i].AMOUNT_OWED+"</span></li>";
                
            }
            
            html_view +="</ul>";
            setTimeout("listRefreshHelper('holdUL')",100);
            $("#HoldsInfo").html(html_view);
            $.mobile.changePage("index.html#HoldsPage");
            
            
        }
        
    }catch(e){}
    stopActivitySpinner();
}



function bannerMTG(data)
{
    
    // console.log(JSON.stringify(data));
    try{ if(data.PROGRAM_NAME == null)
    {
        navigator.notification.alert("You have no Midterm Grades record",doNothing,'Alert','Ok');
    }else{
        var html_view = "";
        try{
            
            html_view ="<ul  id='bfremtg' data-role='listview' data-theme='c' data-inset='true'>";
            
            html_view +="<li data-role='list-divider' class='ui-bar-f'>"+data.studentInfo[0].STUDY_PATH+"<span style='float:right;margin:0 -2px 0 0;white-space:nowrap;width:120px;overflow:hidden;text-overflow:ellipsis;'>"+data.studentInfo[0].PROGRAM_NAME+"</span></li>";
            
            html_view +="<li>"+data.studentInfo[0].ADMIT_TERM+"-"+data.studentInfo[0].CATLG_TREM+"<span class='custom-right-span custom-color-right-span'>"+data.studentInfo[0].COLLEGE+"</span></li>";
            
            html_view +="</ul>";
        }
        
        catch(e)
        {
            
            
        }
        $("#bfremtdisp").html(html_view);
        
        BannerReqTermDown();
    }
    }catch(e){}
    stopActivitySpinner();
}


function befrMTGdrop(data){
    
    try{ var html_view = "";
        
        
        html_view +="<option value=''>Select the Term</option>";
        
        for(var i=0;i<data.dropdown.length;i++)
            
        {
            
            html_view +="<option value='"+data.dropdown[i].TERM_CODE+"'>"+data.dropdown[i].TERM_DESC+"</option>";
            
        }
        
        $("#MidtrmGrd").html(html_view);
        
        //setTimeout("listRefreshHelper('MidtrmGrd')",10);
        setTimeout("listRefreshHelper('bfremtg')",100);
        
        $.mobile.changePage("index.html#MidtermGradePage");
    }catch(e){}
    stopActivitySpinner();
}

function dispmidtergra(data){
    
    
    //console.log(JSON.stringify(data));
    try{ $("#MidtrmGrdshow").html("");
        var hasMidtermGrade;
        for(i=0;i<data.midterm.length;i++)
        {
            if(data.midterm[i].MIDTERM_GRADE){hasMidtermGrade="true"; break;}else{hasMidtermGrade="false"}
        }
        if(hasMidtermGrade == "true"){
            var html_view="";
            html_view ="<ul  id=disp data-role='listview' data-theme='c' data-inset='true'>";
            
            for(i=0;i<data.midterm.length;i++)
            {
                if(data.midterm[i].MIDTERM_GRADE){
                    
                    html_view +="<li data-role='list-divider' class='ui-bar-f'><span style='float:left;margin:0 -2px 0 0;white-space:nowrap;width:160px;overflow:hidden;text-overflow:ellipsis;'>"+data.midterm[i].COURSE_TITLE+"</span><span style='margin:0 5px 0 0'>"+data.midterm[i].SUBJECT_CODE+"/"+data.midterm[i].COURSE_NUMBER+"</span></li>";
                    
                    html_view +="<li>Term:<span class='custom-right-span custom-color-right-span'>"+data.midterm[i].TERM+"</span></li>";
                    
                    html_view +="<li>College/Campus:<span class='custom-right-span custom-color-right-span'>"+data.studentInfo[0].COLLEGE+"</span></li>";
                    
                    html_view +="<li>CRN:<span class='custom-right-span custom-color-right-span'>"+data.midterm[i].CRN+"</span></li>";
                    
                    html_view +="<li>MIDTERM GRADE:<span class='custom-right-span custom-color-right-span'>"+data.midterm[i].MIDTERM_GRADE+"</span></li>";
                    
                    
                    
                }
            }
            html_view +="</ul>";
            
            $("#MidtrmGrdshow").html(html_view);
            setTimeout("listRefreshHelper('disp')", 100);
            $("#MidtrmGrd").val("");
            $("#MidtrmGrd").selectmenu("refresh");
            
        }
        else{
            navigator.notification.alert("You have no Midterm Grades record for "+$("#MidtrmGrd option:selected").text()+" term",doNothing,'Alert','Ok');
        }
    }catch(e){}
    stopActivitySpinner();
}


function beforeFGprint(data)
{
    // console.log(JSON.stringify(data));
    
    try{ if(data.finalgrade.length == 0)
    {
        navigator.notification.alert("Your grades are not available yet. Please try again after the grades have been posted.",doNothing,'Final Grades','Ok');
    }
    else
    {
        
        
        
        var html_view = "";
        
        try{
            
            html_view ="<ul  id=befreFG data-role='listview' data-theme='c' data-inset='true'>";
            
            html_view +="<li data-role='list-divider' class='ui-bar-f'>"+data.studentInfo[0].STUDY_PATH+"<span style='float:right;margin:0 -2px 0 0;white-space:nowrap;width:120px;overflow:hidden;text-overflow:ellipsis;'>"+data.studentInfo[0].PROGRAM_NAME+"</span></li>";
            
            html_view +="<li>"+data.studentInfo[0].ADMIT_TERM+"-"+data.studentInfo[0].CATLG_TREM+"<span class='custom-right-span custom-color-right-span'>"+data.studentInfo[0].COLLEGE+"</span></li>";
            
            
            html_view +="</ul>";
            
            
        }
        
        catch(e)
        {
            alertServerError(e);
            
        }
        $("#bfreftdisp").html(html_view);
        
        BannerFGTermDown();
    }
    }catch(e){}
    stopActivitySpinner();
}

function bfreFGDrop(data)
{
    try{
        
        
        var html_view = "";
        
        
        html_view+="<option value=''>Select the Term</option>";
        for(var i=0;i<data.dropdown.length;i++)
        {
            html_view +="<option value='"+data.dropdown[i].TERM_CODE+"'>"+data.dropdown[i].TERM_DESC+"</option>";
        }
        
        
        $("#FGrd").html(html_view);
        
        
        $.mobile.changePage("index.html#FinalGradePage");
    }catch(e){}
    $("#FGrd").val("");
    $("#FGrd").selectmenu("refresh");
    $("#FinalGRDshow").html("");
    stopActivitySpinner();
}


function dispFG(data){
    console.log(JSON.stringify(data));
    try{ $("#FinalGRDshow").html("");
        var hasFinalGrade;
        for(i=0;i<data.finalgrade.length;i++)
        {if(data.finalgrade[i].FINAL_GRADE){hasFinalGrade="true"; break;}else{hasFinalGrade="false"}}
        if(hasFinalGrade == "true"){
            var html_view="";
            
            for(i=0;i<data.finalgrade.length;i++)
            {
                if(data.finalgrade[i].FINAL_GRADE){
                    html_view +="<ul  id='dis"+i+"' data-role='listview' data-theme='c' data-inset='true'>";
                    html_view +="<li data-role='list-divider' class='ui-bar-f'><span style='float:left;margin:0 -2px 0 0;white-space:nowrap;width:160px;overflow:hidden;text-overflow:ellipsis;'>"+data.finalgrade[i].COURSE_TITLE+"</span><span style='margin:0 5px 0 0'>"+data.finalgrade[i].SUBJECT_CODE+"/"+data.finalgrade[i].COURSE_NUMBER+"</span></li>";
                    
                    html_view +="<li>Term:<span class='custom-right-span custom-color-right-span'>"+data.finalgrade[i].TERM+"</span></li>";
                    
                    //html_view +="<li>College/Campus:<span class='custom-right-span custom-color-right-span' style='white-space:nowrap; text-overflow:ellipsis; overflow:hidden;width:140px;margin:3px 0 0 0'>"+data.studentInfo[0].COLLEGE+"</span></li>";
                    
                    html_view +="<li>CRN:<span class='custom-right-span custom-color-right-span'>"+data.finalgrade[i].CRN+"</span></li>";
                    
                    html_view +="<li>Final Grade:<span class='custom-right-span custom-color-right-span'>"+data.finalgrade[i].FINAL_GRADE+"</span></li>";
                    html_view +="</ul>";
                }
            }
            
            $("#FinalGRDshow").html(html_view);
            
            try{ for(k=0; k<=i; k++)
                
            {
                var classULName="dis"+k;
                setTimeout("listRefreshHelper('"+classULName+"')",10);
                
            }
            }catch(e){//alert(e)
            }
            setTimeout("listRefreshHelper('dis')", 100);
            
        }
        
        else
        {
            navigator.notification.alert("Your grades are not available yet. Please try again after the grades have been posted.",doNothing,'Final Grades','Ok');
        }
    }catch(e){}
    stopActivitySpinner();
}

function dispLUC(data)
{
    try{ var html_view = "";
        
        
        html_view +="<option value=''>Search by Term</option>";
        for(var i=0;i<data.termdropdown.length;i++)
        {
            html_view +="<option value='"+data.termdropdown[i].TERM_CODE+"'>"+data.termdropdown[i].TERM_DESC+"</option>";
        }
        
        
        
        $("#LUC").html(html_view);
        $("#LUC").val("");
        $("#LUC").selectmenu();
        $("#LUC").selectmenu("refresh",true);
        
        //$("#LUCMain").hide();
        $.mobile.changePage("index.html#LUCpage");
        
        
        $("#LUCsubject").html("<option value=''>Select Subject</option>");
        $("#LUCsubject").val("");
        $("#LUCsubject").selectmenu();
        $("#LUCsubject").selectmenu("refresh",true);
        
        $("#slctinstruc").html("<option value=''>Select Instructor</option>");
        $("#slctinstruc").val("");
        $("#slctinstruc").selectmenu();
        $("#slctinstruc").selectmenu("refresh",true);
        
        
    }catch(e){alert(e)}
    stopActivitySpinner();
}

function LUCsucessdata(data)
{
    
    try{ var html_view = "";
        html_view +="<option value=''>Select Subject</option>";
        for(var i=0;i<data.subjectDropDown.length;i++)
        {
            html_view +="<option value='"+data.subjectDropDown[i].SUBJECT_CODE+"'>"+data.subjectDropDown[i].SUBJECT_DESC+"</option>";
        }
        
        
        $("#LUCsubject").html(html_view);
        $("#LUCsubject").val("");
        $("#LUCsubject").selectmenu();
        $("#LUCsubject").selectmenu("refresh");
        
        
        
        $("#slctinstruc").val("");
        $("#slctinstruc").selectmenu();
        $("#slctinstruc").selectmenu("refresh");
        
        
        
        var html_vieww="";
        html_vieww+="<option value=''>Select Part of Term</option>";
        for(var i=0;i<data.partOfTermDropDown.length;i++)
        {
            html_vieww +="<option value='"+data.partOfTermDropDown[i].PART_TERM_CODE+"'>"+data.partOfTermDropDown[i].PART_TERM_DESC+"</option>";
        }
        $("#partoftrm").html(html_vieww);
        
        
        
        $("#LUCMain").show();
    }catch(e){}
    stopActivitySpinner();
}

function LUCInstructorData(data)
{
    try{
        stopActivitySpinner();
        var html_view1="";
        html_view1 +="<option value=''>Select Instructor</option>";
        for(var i=0;i<data.instructorForSubj.length;i++)
        {
            html_view1 +="<option value='"+data.instructorForSubj[i].SPRIDEN_PIDM+"'>"+data.instructorForSubj[i].SPRIDEN_FIRST_NAME+" "+data.instructorForSubj[i].SPRIDEN_LAST_NAME+"</option>";
        }
        
        $("#slctinstruc").html(html_view1);
        $("#slctinstruc").val("");
        $("#slctinstruc").selectmenu();
        $("#slctinstruc").selectmenu("refresh");
    }catch(e){alert(e)}
}

function displayLUCGra(data)
{
    console.log(JSON.stringify(data));
    
    try{
        if(data.CLASSLOOKUP.length == 0){
            navigator.notification.alert("Your search came back with no results. Try a new search.",doNothing,'No Record Found','Ok')
        }
        else{
            var html_view="";
            for(i=0;i<data.CLASSLOOKUP.length;i++)
            {
                //alert(data.CLASSLOOKUP[i][0].COURSE_ID);
                html_view =html_view+"<ul  id='dispLUC"+i+"' data-role='listview' data-theme='c' data-inset='true'>"
                html_view +="<li data-role='list-divider' class='ui-bar-f'>"+data.CLASSLOOKUP[i][0].TITLE+": "+data.CLASSLOOKUP[i][0].COURSE_ID+"</div></li>";
                
                html_view +="<li>CRN:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].CRN+"</span></li>";
                
                html_view +="<li>Credits:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].CREDIT_HOURS+"</span></li>";
                
                html_view +="<li>Instructor:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].INSTRUCTOR+"</span></li>";
                
                if(data.CLASSLOOKUP[i][0].TIME != "TBA")
                {
                    var LUC = data.CLASSLOOKUP[i][0].TIME.split(" ");
                    var LUCTime=LUC[1].split("-");
                    var T1=getFormattedTime(LUCTime[0]);
                    var T2=getFormattedTime(LUCTime[1]);
                    html_view +="<li>Day/Time:<span class='custom-right-span-luc'>"+LUC[0]+" "+T1+"-"+T2+"</span><div style='clear:both'></div></li>";
                }
                else
                {
                    html_view +="<li>Day/Time:<span class='custom-right-span-luc'>TBA</span></li>";
                }
                
                html_view +="<li>Location:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].LOCATION+"</span></li>";
                if(data.CLASSLOOKUP[i][0].FEE == 0){
                    
                }else{
                    html_view +="<li>Fee:<span class='custom-right-span-luc'>$"+data.CLASSLOOKUP[i][0].FEE+"</span></li>";
                }
                
                
                html_view +="<li>Duration:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].DURATION+"</span></li>";
                html_view +="<li>Capacity:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].CAPACITY+"</span></li>";
                html_view +="<li>Remaining:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].REMAINING+"</span></li>";
                //html_view +="<li>Delete Occupied:<span class='custom-right-span-luc'>"+data.CLASSLOOKUP[i][0].ACTUAL+"</span></li>";
                
                html_view +="</ul>";
            }
            
            //$("#LUCdrop").hide();
            //$("#LUCMain").hide();
            for(i=0;i<data.CLASSLOOKUP.length;i++)
            {
                setTimeout("listRefreshHelper('dispLUC"+i+"')", 100);
            }
            
            $("#lookupclassdisp").html(html_view);
            $.mobile.changePage("index.html#LUCDisplaypage");
        }
    }catch(e){}
    stopActivitySpinner();
}

function dispRegistration(data)
{
    //console.log(">>>Display Registration::"+JSON.stringify(data));
    try{ $("#registrationInfo").html("");
        var html_view = "";
        
        
        html_view +="<option value=''>Search by Term</option>";
        
        for(var i=0;i<data.termdropdown.length;i++)
        {
            html_view +="<option value='"+data.termdropdown[i].TERM_CODE+"'>"+data.termdropdown[i].TERM_DESC+"</option>";
        }
        
        
        
        $("#registrationInfo").html(html_view);
        $.mobile.changePage("index.html#registrationPage");
        try{$("#registrationInfo").val("");
            $("#registrationInfo").selectmenu("refresh");
        }catch(e){}
        
    }catch(e){}
    stopActivitySpinner();
}

function registrationSucessData(data)
{
    //alert("reached registrationSucessData")
    
    
    // alert("before data");
    console.log(JSON.stringify(data));
    $("#displayRegFinalInfo").html("");
    try{
        var html_view="";
        html_view ="<ul  id=regUL data-role='listview' data-theme='c' data-inset='true'>";
        
        html_view +="<li data-role='list-divider' class='ui-bar-f'>Registration</li>";
        if(data.STUDENT_STATUS == "Permit"){
            html_view += "<li>Student Standing<div class='check'></div></li>";
        }else{
            html_view += "<li>Student Standing<div class='cross'></div></li>";
        }
        
        if(data.ACADEMIC_STANDING == "Permit"){
            html_view += "<li>Academic Standing<div class='check'></div></li>";
        }else{
            html_view += "<li>Academic Standing<div class='cross'></div></li>";;
        }
        if(data.HOLDS == "Permit"){
            //html_view += "<li>Holds<div class='check'></div></li>";
        }else{
            html_view += "<li>Holds<div class='cross'></div></li>";
        }
        html_view +="</ul>";
        $("#displayRegFinal").html(html_view);
        setTimeout("listRefreshHelper('regUL')",100);
        html_view="";
        var creditInformation = data.CREDIT_INFORMATION;
        //console.log("credit info"+JSON.stringify(creditInformation)+"lenght"+creditInformation.length);
        for(var i=0; i<creditInformation.length; i++){
            //try{alert(data.CREDIT_INFORMATION[1][0].CREDIT_TYPE)}catch(e){alert(e)}
            
            if(data.CREDIT_INFORMATION[i][0].CREDIT_TYPE != "Transfer"){
                html_view +="<ul id='regUL"+i+"' data-role='listview' data-theme='c' data-inset='true'>";
                
                if((data.CREDIT_INFORMATION[i][0].CREDIT_TYPE==null))
                {
                    html_view +="<li data-role='list-divider'  class='ui-bar-f'>"+((data.CREDIT_INFORMATION[i][0].CREDIT_TYPE == "Institutional")?'Student Status':data.CREDIT_INFORMATION[i][0].CREDIT_TYPE)+"</li>";
                }
                else
                {
                    html_view +="<li data-role='list-divider'  class='ui-bar-f'>"+((data.CREDIT_INFORMATION[i][0].CREDIT_TYPE == "Institutional")?'Student Status':data.CREDIT_INFORMATION[i][0].CREDIT_TYPE)+"</li>";
                }
                html_view +="<li>Classification<span class='custom-right-span custom-color-right-span'>"+data.classDesc+"</span></li>";
                html_view +="<li>Credit Hours<span class='custom-right-span custom-color-right-span'>"+data.CREDIT_INFORMATION[i][0].HOURS+"</span></li>";
                html_view +="</ul>";
            }
            var ulNAME="regUL"+i;
            $("#displayRegFinalInfo").append(html_view);
            html_view="";
            setTimeout("listRefreshHelper('"+ulNAME+"')",100);
        }
        
        
        
        
    }catch(e){}
    stopActivitySpinner();
}

function dispAccountSummaryTerm(data)
{
    var html_view="";
    html_view +="<option value=''>Select the Term</option>";
    for(var i=0;i<data.termswithaccountsummary.length;i++)
    {
        html_view +="<option value='"+data.termswithaccountsummary[i].TERM_CODE+"'>"+data.termswithaccountsummary[i].DESCRIPTION+"</option>";
    }
    $("#AccountTermDown").html(html_view);
    $.mobile.changePage("#AccountSummaryTermDown");
    $("#AccountTermDown").val("");
    $("#AccountTermDown").selectmenu("refresh");
    stopActivitySpinner();
}

function dispAccountSummary(data)
{
    
    console.log(JSON.stringify(data));
    
    try{ var html_view="";
        html_view ="<ul  id='accsummary' data-role='listview' data-theme='c' data-inset='true'>";
        // for(i=0;i<data.summary.length;i++)
        // {
        
        html_view +="<li data-role='list-divider' class='ui-bar-f'><span>Current Account Balance</span><span style='margin:0 0 0 30px;white-space:nowrap;width:90px;overflow:hidden;text-overflow:ellipsis;'></span></li>";
        
        
        /*if(data.creditsandpayments[0].CHARGES == null || data.creditsandpayments[0].CHARGES == "$0.00")
        {
            formatAccountBalance ="$0.00";
            html_view +="<li>Charges:<span class='custom-right-span custom-color-right-span' style='font-weight:bold;'>"+formatAccountBalance+"</span></li>";
            
            html_view +="<li>Credits and Payments:<span class='custom-right-span custom-color-right-span accSummTextColor' style='font-weight:bold;'>"+data.creditsandpayments[0].CREDITS_AND_PAYMENTS+"</span></li>";
            
        }
        else
        {
            var formatAccountBalance=formatCurrency(data.creditsandpayments[0].CHARGES);
            html_view +="<li>Charges:<span class='custom-right-span custom-color-right-span' style='font-weight:bold;color:#CC0000;'>"+formatAccountBalance+"</span></li>";
            
            html_view +="<li>Credits and Payments:<span class='custom-right-span custom-color-right-span accSummTextColor' style='font-weight:bold;'>"+data.creditsandpayments[0].CREDITS_AND_PAYMENTS+"</span></li>";
        }*/
        
        html_view +="<li>Balance:<span class='custom-right-span custom-color-right-span' style='font-weight:bold;'>"+data.accountbalance[0].ACCOUNT_BALANCE+"</span></li>";
        
        
        html_view +="</ul>";
        setTimeout("listRefreshHelper('accsummary')",100);
        
        html_view +="<ul  id='accsummaryPayCredit' data-role='listview' data-theme='c' data-inset='true'>";
        html_view +="<li data-role='list-divider' class='ui-bar-f'>Payments and Credits</li>";
        var found=false;
        for(i=0;i<data.summary.length;i++)
        {
            if(data.summary[i].PAYMENT != "$0.00"){
                found=true;
                html_view +="<li><span style='font-size:11px;'>"+data.summary[i].TBRACCD_EFFECTIVE_DATE+"<br/>"+data.summary[i].DESCRIPTION+"</span><span class='custom-right-span custom-color-right-span accSummTextColor'>"+data.summary[i].PAYMENT+"</span><div style='clear:both;'></div></li>";
            }else{
                found=true;
                html_view +="<li><span style='font-size:11px;'>"+data.summary[i].TBRACCD_EFFECTIVE_DATE+"<br/>"+data.summary[i].DESCRIPTION+"</span><span class='custom-right-span custom-color-right-span accSummChargeColor'>"+data.summary[i].CHARGE+"</span><div style='clear:both;'></div></li>";
            }
        }
        if(!found){
            html_view +="<li>You have no payments and credits information.</li>";
        }
        
        html_view +="</ul>";
        $("#accountInfo").html(html_view);
        setTimeout("listRefreshHelper('accsummaryPayCredit')",10);
        
        $.mobile.changePage("index.html#accountSummaryPage");
    }catch(e){}
    stopActivitySpinner();
}


function showFlickr(data)
{
    var len=data.length;
    var flickr_view="";
    //flickr_view +="<li data-role='list-divider'>NYLS Flickr Photos</li></ul>";
    for( var i=0;i<len;i++)
    {
        //var biggerPicUrl=encodeURIComponent(data[i].mediaGroups[0].contents[0].url);
        flickr_view +="<div style='margin:20px 0px 0px 20px;float:left' onclick=showBigFlickrPhoto('"+data[i].mediaGroups[0].contents[0].url+"')><img class='flickrPhoto' src='"+data[i].mediaGroups[0].contents[0].thumbnails[0].url+"'></div>";
        
    }
    //flickr_view +="</ul>";
    $("#flickrContent").html(flickr_view);
    $.mobile.changePage("#flickrPage");
    //setTimeout("listRefreshHelper('flickrPhoto')", 100);
    
    
    stopActivitySpinner();
}




function showBigFlickrPhoto(url)
{
    //console.log(url);
    //var url=encodeURIComponent(url);
    $("#flickrBigPhoto").html("<img style='width:280px;height:280px;-webkit-border-radius:10px' src='"+url+"'/>");
    $.mobile.changePage("#flickrBigPhotoPage");
    
}



function directorySearch(querytext)
{
    startActivitySpinner();
    var patt=new RegExp(querytext,"i");
    var firstlastname="",found=false;
    var html_view="";
    //html_view +="<ul data-role='listview' id='dirList'>"
    for(var i=0;i<directoryentries.length;i++)
    {
        var fullname=directoryentries[i].firstname+" "+directoryentries[i].lastname;
        if(directoryentries[i].firstname.match(patt) || directoryentries[i].lastname.match(patt) || fullname.match(patt)){
            
            found =true;
            var fname=encodeURIComponent(directoryentries[i].firstname);
            var lname=encodeURIComponent(directoryentries[i].lastname);
            var mname=encodeURIComponent(directoryentries[i].middleName);
            var umail=encodeURIComponent(directoryentries[i].univinetMail);
            var phone=encodeURIComponent(directoryentries[i].phone);
            var dept=encodeURIComponent(directoryentries[i].dept);
            var address=encodeURIComponent(directoryentries[i].address);
            var title=encodeURIComponent(directoryentries[i].title);
            var photo=encodeURIComponent(directoryentries[i].photo);
            
            if(directoryentries[i].lastname[0] == firstlastname)
            {
                html_view +="<li onclick=dispDirList(\""+fname+"\",\""+lname+"\",\""+mname+"\",\""+umail+"\",\""+phone+"\",\""+dept+"\",\""+address+"\",\""+title+"\",\""+photo+"\")><a>"+directoryentries[i].firstname+" "+directoryentries[i].lastname+" - "+directoryentries[i].dept+"</a></li>";
                
            }
            else
            {//alert(directoryentries[i].lastname[0]);
                html_view +="<li class='ui-btn ui-bar-f' data-role='list-divider' style='height:20px'>"+directoryentries[i].lastname[0]+"</li>"
                html_view +="<li onclick=dispDirList(\""+fname+"\",\""+lname+"\",\""+mname+"\",\""+umail+"\",\""+phone+"\",\""+dept+"\",\""+address+"\",\""+title+"\",\""+photo+"\")><a>"+directoryentries[i].firstname+" "+directoryentries[i].lastname+" - "+directoryentries[i].dept+"</a></li>";
                firstlastname=directoryentries[i].lastname[0];
            }
            
        }
        
    }
    
    // html_view +="</ul>";
    
    if(found == true){
        $("#display_DirSearchResults").html(html_view);
        setTimeout("listRefreshHelper('display_DirSearchResults')",10);
        $("#directorymiddlebox").hide();
        $("#directory").show();
        //alert($("#display_DirSearchResults").html());
    }
    else{
        $("#display_DirSearchResults").html("<div class='noResultmiddilebox'>This search did not return any results.</div>");
        
        $("#directorymiddlebox").hide();
        $("#directory").show();
    }
    stopActivitySpinner();
}

function dispDirList(fname,lname,mname,mailid,phone,dept,address,title,photo)
{
    //alert(fname+lname+mailid+phone);
    var fname=decodeURIComponent(fname);
    var lname=decodeURIComponent(lname);
    var mname=decodeURIComponent(mname);
    var mailid=decodeURIComponent(mailid);
    var phone=decodeURIComponent(phone);
    var dept=decodeURIComponent(dept);
    var address=decodeURIComponent(address);
    var title=decodeURIComponent(title);
    var photo=decodeURIComponent(photo);
    $("#displayName_directory_listing").text(fname+" "+mname+" "+lname);
    $("#fname").text(fname);
    $("#lname").text(lname);
    if(photo == ""){$("#dirPhoto").attr("src", "Settings/images/1.jpg");        }else{$("#dirPhoto").attr("src",photo);        }
    $("#dirInfoList").html("");
    try{var html_view="";
        //html_view +="<ul id='DirInfoList' data-role='listview' data-inset='true'>";
        html_view +="<li class='ui-btn ui-bar-f' data-role='list-divider'>Title: <span style='text-shadow:none'>"+title+"</span></li>";
        html_view +="<li data-theme='d' onclick='sendMail(null,null,\"" +mailid+ "\",null,null,null)'><a>Email: <span id='email'>"+mailid+"</span></a></li>";
        html_view +="<li data-theme='d'><a href='tel:"+phone+ "'>Phone: <span id='phone'>"+phone+"</span></a></li>";
        html_view +="<li data-theme='d' style='font-size:13px'>Department: <span id='phone'>"+dept+"</span></a></li>";
        html_view +="<li data-theme='d' style='font-size:13px'>Location: <span id='phone'>"+address+"</span></a></li>";
        //html_view +="</ul>";
        $("#dirInfoList").html(html_view);
        setTimeout("listRefreshHelper('dirInfoList')", 100);
        $.mobile.changePage("#directory_info_details");
    }catch(e){}
}


function CourseSearch(querytext)
{
    startActivitySpinner();
    var patt=new RegExp(querytext,"i");
    var firstlastname="",found=false;
    var html_view="";
    //html_view +="<ul data-role='listview' id='dirList'>"
    for(var i=0;i<CoursesEntries.length;i++)
    {
        
        if(CoursesEntries[i].CN.match(patt) || CoursesEntries[i].CT.match(patt) || CoursesEntries[i].DN.match(patt)){
            
            found =true;
            var campus=encodeURIComponent(CoursesEntries[i].campus);
            var SC=encodeURIComponent(CoursesEntries[i].SC);
            var DN=encodeURIComponent(CoursesEntries[i].DN);
            var TN=encodeURIComponent(CoursesEntries[i].TN);
            var TC=encodeURIComponent(CoursesEntries[i].TC);
            var CT=encodeURIComponent(CoursesEntries[i].CT);
            var CN=encodeURIComponent(CoursesEntries[i].CN);
            var ClsN=encodeURIComponent(CoursesEntries[i].ClsN);
            var CS=encodeURIComponent(CoursesEntries[i].CS);
            var Units=encodeURIComponent(CoursesEntries[i].Units);
            var ED=encodeURIComponent(CoursesEntries[i].ED);
            var SD=encodeURIComponent(CoursesEntries[i].SD);
            var ST=encodeURIComponent(CoursesEntries[i].ST);
            var ET=encodeURIComponent(CoursesEntries[i].ET);
            var day=encodeURIComponent(CoursesEntries[i].day);
            var IFN=encodeURIComponent(CoursesEntries[i].IFN);
            var ILN=encodeURIComponent(CoursesEntries[i].ILN);
            var BN=encodeURIComponent(CoursesEntries[i].BN);
            var BC=encodeURIComponent(CoursesEntries[i].BC);
            var RN=encodeURIComponent(CoursesEntries[i].RN);
            var ECap=encodeURIComponent(CoursesEntries[i].ECap);
            var ETot=encodeURIComponent(CoursesEntries[i].ETot);
            
            /* if(CoursesEntries[i].lastname[0] == firstlastname)
             {
             html_view +="<li onclick=dispDirList(\""+fname+"\",\""+lname+"\",\""+mname+"\",\""+umail+"\",\""+phone+"\",\""+dept+"\",\""+address+"\",\""+title+"\",\""+photo+"\")><a>"+directoryentries[i].firstname+" "+directoryentries[i].lastname+" - "+directoryentries[i].dept+"</a></li>";
             
             }
             else
             {//alert(directoryentries[i].lastname[0]);
             html_view +="<li class='ui-btn ui-bar-f' data-role='list-divider' style='height:20px'>"+directoryentries[i].lastname[0]+"</li>"
             html_view +="<li onclick=dispDirList(\""+fname+"\",\""+lname+"\",\""+mname+"\",\""+umail+"\",\""+phone+"\",\""+dept+"\",\""+address+"\",\""+title+"\",\""+photo+"\")><a>"+directoryentries[i].firstname+" "+directoryentries[i].lastname+" - "+directoryentries[i].dept+"</a></li>";
             firstlastname=directoryentries[i].lastname[0];
             }*/
            html_view +="<li onclick=dispCourseList(\""+campus+"\",\""+SC+"\",\""+DN+"\",\""+TN+"\",\""+TC+"\",\""+CT+"\",\""+CN+"\",\""+ClsN+"\",\""+CS+"\",\""+Units+"\",\""+ST+"\",\""+ED+"\",\""+SD+"\",\""+ET+"\",\""+day+"\",\""+IFN+"\",\""+ILN+"\",\""+BN+"\",\""+BC+"\",\""+RN+"\",\""+ECap+"\",\""+ETot+"\")><a>"+CoursesEntries[i].CT+" - "+CoursesEntries[i].CN+" - "+CoursesEntries[i].DN+"</a></li>";
            
        }
        
    }
    
    // html_view +="</ul>";
    
    if(found == true){
        $("#display_coursesSearchResults").html(html_view);
        setTimeout("listRefreshHelper('display_coursesSearchResults')",100);
        $("#coursesmiddlebox").hide();
        $("#courses").show();
        //alert($("#display_DirSearchResults").html());
    }
    else{
        $("#display_coursesSearchResults").html("<div class='noResultmiddilebox'>This search did not return any results.</div>");
        
        $("#coursesmiddlebox").hide();
        $("#courses").show();
    }
    stopActivitySpinner();
}

function dispCourseList(campus,SC,DN,TN,TC,CT,CN,ClsN,CS,Units,ST,ED,SD,ET,day,IFN,ILN,BN,BC,RN,ECap,ETot)
{
    try{
        var campus=decodeURIComponent(campus);
        var SC=decodeURIComponent(SC);
        var DN=decodeURIComponent(DN);
        var TN=decodeURIComponent(TN);
        var TC=decodeURIComponent(TC);
        var CT=decodeURIComponent(CT);
        var CN=decodeURIComponent(CN);
        var ClsN=decodeURIComponent(ClsN);
        var CS=decodeURIComponent(CS);
        var Units=decodeURIComponent(Units);
        var ST=decodeURIComponent(ST);
        var ED=decodeURIComponent(ED);
        var SD=decodeURIComponent(SD);
        var ET=decodeURIComponent(ET);
        var day=decodeURIComponent(day);
        var IFN=decodeURIComponent(IFN);
        var ILN=decodeURIComponent(ILN);
        var BN=decodeURIComponent(BN);
        var BC=decodeURIComponent(BC);
        var RN=decodeURIComponent(RN);
        var ECap=decodeURIComponent(ECap);
        var ETot=decodeURIComponent(ETot);
        var html_view="";
        html_view +="<li data-role='list-divider' class='ui-bar-f'>"+CT+" - "+CN+"</li>";
        html_view +="<li class='custom-label-li'>Subject Code:<span class='custom-right-span custom-color-right-span'>"+SC+"</span></li>";
        html_view +="<li class='custom-label-li'>Department Name:<span class='custom-right-span custom-color-right-span' style='text-align:right;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;width:56%'>"+DN+"</span></li>";
        html_view +="<li class='custom-label-li'>Term Name-Code:<span class='custom-right-span custom-color-right-span'>"+TN+" "+TC+"</span></li>";
        html_view +="<li class='custom-label-li'>Class Number-Section:<span class='custom-right-span custom-color-right-span'>"+ClsN+" "+CS+"</span></li>";
        html_view +="<li class='custom-label-li'>Units:<span class='custom-right-span custom-color-right-span'>"+Units+"</span></li>";
        html_view +="<li class='custom-label-li'>Start-End Date:<span class='custom-right-span custom-color-right-span'>"+SD+"/"+ED+"</span></li>";
        html_view +="<li class='custom-label-li'>Start-End Time:<span class='custom-right-span custom-color-right-span'>"+ST+" "+ET+"</span></li>";
        html_view +="<li class='custom-label-li'>Days:<span class='custom-right-span custom-color-right-span'>"+day+"</span></li>";
        //html_view +="<li class='custom-label-li'>Building Name-Code:<span class='custom-right-span custom-color-right-span' style='white-space:nowrap; text-overflow:ellipsis; overflow:hidden;width:42%'>"+BN+" "+BC+"</span></li>";
        html_view +="<li class='custom-label-li'>Building-Code:<span class='custom-right-span custom-color-right-span' style='text-align:right;white-space:nowrap;overflow:ellipsis;width:66%'>"+BN+" "+BC+"</span></li>";
        //html_view +="<li class='custom-label-li'>Building-Code:<span class='custom-right-span custom-color-right-span' style='white-space:nowrap; text-overflow:ellipsis;width:67%'>"+BN+" "+BC+"</span></li>";
        //html_view +="<li>Bldg-Code:<span class='custom-right-span custom-color-right-span' style='white-space:nowrap;width:67%'>"+BN+" "+BC+"</span></li>";
        html_view +="<li class='custom-label-li'>Room Number:<span class='custom-right-span custom-color-right-span'>"+RN+"</span></li>";
        html_view +="<li class='custom-label-li'>Total Seats:<span class='custom-right-span custom-color-right-span'>"+ETot+"</span></li>";
        html_view +="<li class='custom-label-li'>Occupied Seats:<span class='custom-right-span custom-color-right-span'>"+ECap+"</span></li>";
        
        $("#CoursesInfoList").html(html_view);
        setTimeout("listRefreshHelper('CoursesInfoList')", 100);
        $.mobile.changePage("#courses_info_details");
    }catch(e){alert(e)}
}



function displayWeekAtGlnaceTermDown(data)
{
    /*var html_view="";
     html_view+="<option value=''>Select the Term</option>";
     for(var i=0;i<data.termsStudentHasClasses.length;i++)
     {
     html_view +="<option value='"+data.termsStudentHasClasses[i].TERM_CODE+"'>"+data.termsStudentHasClasses[i].TERM_DESC+"</option>";
     }
     $("#WeekTermDown").html(html_view);
     $.mobile.changePage("#weekAtGlancePageTermDown");*/
    WeekTermDownClick(data.currentTerm[0].CURRENT_TERM);
}


var weekfound=false;
function displayWeekAtGlance(data)
{
    //console.log(JSON.stringify(data));
    // alert(data.weekataglance.length);
    
    
    var weekataglanceLength=data.weekataglance.length;
    var monday = new Array();
    var tuesday = new Array();
    var wednesday = new Array();
    var thursday = new Array();
    var friday = new Array();
    var saturday = new Array();
    var sunday = new Array();
    
    var temp = {"desc":"", "place":"", "time":""}
    //alert("Length::"+weekataglanceLength);
    
    for(var i=0; i<weekataglanceLength; i++){
        temp.desc = data.weekataglance[i].DESCRIPTION;
        temp.place = data.weekataglance[i].PLACE;
        temp.time = data.weekataglance[i].CLASS_TIME;
        
        
        if(data.weekataglance[i].MON_DAY!=null){
            monday.push(temp)
        }
        if(data.weekataglance[i].TUE_DAY!=null){
            tuesday.push(temp)
        }
        if(data.weekataglance[i].WED_DAY!=null){
            wednesday.push(temp)
        }
        if(data.weekataglance[i].THU_DAY!=null){
            thursday.push(temp)
        }
        if(data.weekataglance[i].FRI_DAY!=null){
            friday.push(temp)
        }
        if(data.weekataglance[i].SAT_DAY!=null){
            saturday.push(temp)
        }
        if(data.weekataglance[i].SUN_DAY!=null){
            sunday.push(temp)
        }
        temp = {"desc":"", "place":"", "time":""}
    }
    
    daysArray = [monday,tuesday,wednesday,thursday,friday,saturday];
    
    todayDate = new Date();
    
    todaysDay = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    
    if(todayDate.getDay() == 6 || todayDate.getDay() == 7){currentDay =daysArray[0]
        $("#currentday").html(todaysDay[0]);
    }
    else{
        currentDay = daysArray[todayDate.getDay()-1];
        $("#currentday").html(todaysDay[daysArray.indexOf(currentDay)])
        if(todaysDay[daysArray.indexOf(currentDay)] == "Saturday"){$("#nextButton").hide()}
        if(todaysDay[daysArray.indexOf(currentDay)] == "Monday"){$("#backButton").hide()}
    }
    // alert(JSON.stringify(currentDay));
    //currentDay = wednesday
    
    var html_view="";
    html_view+="<ul id='weekAtGlance' data-role='listview' data-theme='c' data-inset='true'>";
    
    for(var i=0;i<currentDay.length;i++){
        
        
        
        weekfound=true;
        // html_view+= "<li><span class='serial_num circle'>"+(i+1)+"</span>";
        
        html_view+= "<li><div>"+currentDay[i].desc+"</div>";
        html_view+= "<div><span>Location: "+currentDay[i].place+"</span>";
        var a = currentDay[i].time;
        a = a.split(" - ");
        a[0] = a[0].replace(":", "");
        a[1] = a[1].replace(":", "");
        var t1 = getFormattedTime(a[0]); var t2 = getFormattedTime(a[1]);
        
        html_view+= "<div><span>   Time: "+t1+" - "+t2+"</span></div></div>";
        
        html_view+= "</li>";
        
        
    }
    
    html_view +="</ul>";
    // console.log(html_view)
    if(weekfound == true)
    {
        $("#weekGlance").html(html_view);
    }
    else
    {
        $("#weekGlance").html("<div class='weekAlert'>There are no classes on your schedule for today.</div>");
    }
    //setTimeout("listRefreshHelper('holdUL')",100);
    
    
    html_view = "";
    setTimeout("listRefreshHelper('weekAtGlance')",100);
    $.mobile.changePage("index.html#weekAtGlancePage");
    stopActivitySpinner();
    
}

function previous(){
    weekfound=false;
    $("#nextButton").show()
    if(daysArray.indexOf(currentDay) == -1){
        daysArray.indexOf(currentDay) = 6;
    }
    //alert("previous")
    currentDay = daysArray[daysArray.indexOf(currentDay)-1];
    var html_view="";
    html_view+="<ul id='weekAtGlance' data-role='listview' data-theme='c' data-inset='true'>";
    
    for(var i=0;i<currentDay.length;i++){
        
        
        
        weekfound=true;
        html_view+= "<li><div>"+currentDay[i].desc+"</div>";
        html_view+= "<div><span>Location: "+currentDay[i].place+"</span>";
        var a = currentDay[i].time;
        a = a.split(" - ");
        a[0] = a[0].replace(":", "");
        a[1] = a[1].replace(":", "");
        var t1 = getFormattedTime(a[0]); var t2 = getFormattedTime(a[1]);
        
        html_view+= "<div><span>   Time: "+t1+" - "+t2+"</span></div></div>";
        
        html_view+= "</li>";
        
        
    }
    
    html_view +="</ul>";
    //console.log(html_view);
    
    //setTimeout("listRefreshHelper('holdUL')",100);
    if(todaysDay[daysArray.indexOf(currentDay)] == "Monday"){$("#backButton").hide()}
    $("#currentday").html(todaysDay[daysArray.indexOf(currentDay)]);
    if(weekfound == true){
        $("#weekGlance").html(html_view);
        
    }
    else{
        $("#weekGlance").html("<div class='weekAlert'>There are no classes on your schedule for today.</div>");
    }
    
    html_view = "";
    setTimeout("listRefreshHelper('weekAtGlance')",100);
    
    $.mobile.changePage("index.html#weekAtGlancePage")
    
    
}
function next(){
    //alert("next")
    weekfound=false;
    $("#backButton").show();
    currentDay = daysArray[daysArray.indexOf(currentDay)+1];
    if(daysArray.indexOf(currentDay) == 6){
        daysArray.indexOf(currentDay) = 0;
    }
    var html_view="";
    html_view+="<ul id='weekAtGlance' data-role='listview' data-theme='c' data-inset='true'>";
    
    for(var i=0;i<currentDay.length;i++){
        
        
        
        weekfound=true;
        html_view+= "<li><div>"+currentDay[i].desc+"</div>";
        html_view+= "<div><span>Location: "+currentDay[i].place+"</span>";
        var a = currentDay[i].time;
        a = a.split(" - ");
        a[0] = a[0].replace(":", "");
        a[1] = a[1].replace(":", "");
        var t1 = getFormattedTime(a[0]); var t2 = getFormattedTime(a[1]);
        
        html_view+= "<div><span>   Time: "+t1+" - "+t2+"</span></div></div>";
        
        html_view+= "</li>";
        
        
    }
    
    html_view +="</ul>";
    //console.log(html_view);
    
    if(todaysDay[daysArray.indexOf(currentDay)] == "Saturday"){$("#nextButton").hide()}
    $("#currentday").html(todaysDay[daysArray.indexOf(currentDay)]);
    if(weekfound == true){
        $("#weekGlance").html(html_view);
        
    }
    else{
        $("#weekGlance").html("<div class='weekAlert'>There are no classes on your schedule for today.</div>");
    }
    
    html_view = "";
    setTimeout("listRefreshHelper('weekAtGlance')",100);
    
    //$.mobile.changePage("index.html#weekAtGlancePage")
    
}


function befreAcademicTranscripts(data)
{
    var html_view = "";
    
    html_view +="<option value=''>Select Transcript Level</option>";
    for(var i=0;i<data.transcriptleveldropdown.length;i++)
        
    {
        
        html_view +="<option value='"+data.transcriptleveldropdown[i].LEVEL_CODE+"'>"+data.transcriptleveldropdown[i].LEVEL_DESC+"</option>";
        
    }
    
    $("#AT").html(html_view);
    $.mobile.changePage("index.html#AcademicTranscriptsPage");
    stopActivitySpinner();
    $("#AT").val("");
    $("#AT").selectmenu("refresh");
    
}

function displayATdata()
{
    
    
    var html_view="";
    
    html_view += "<option value=''>Select Summary</option>";
    html_view += "<option value='transfercredits'>Transfer Credits</option>";
    html_view += "<option value='institutioncredits'>Institution Credits</option>";
    html_view += "<option value='transcriptstotal'>Transcripts Total</option>";
    
    $("#selectAca").html(html_view);
    // $.mobile.changePage("index.html#AcademicTranscriptsDisplayPage",{ transition: "none"});
    $("#selectAca").val("");
    $("#AcadefinaShowS").html("");
    $("#selectAca").selectmenu("refresh");
    $("#bfreatdrop_selectAca").show();
    stopActivitySpinner();
}

function ATfinshow(id)
{
    var html_view="";
    try{ if(id=="transfercredits")
    {
        
        html_view ="<ul  id='transcreditshow' data-role='listview' data-theme='c' data-inset='true'>";
        //html_view +="<li data-role='list-divider' class='ui-bar-f'>"+acadeTrans.studentInfo[0].ADMIT_TERM+" :<span style='float:right;margin:0 -2px 0 0;white-space:nowrap;width:90px;overflow:hidden;text-overflow:ellipsis;'></span></li>";
        for(i=0;i<acadeTrans.transfer_data.length;i++)
        {
            if(typeof acadeTrans.transfer_data[i][0].TITLE == "undefined")
            {
                
            }else{
                
                html_view +="<li>"+acadeTrans.transfer_data[i][0].SUBJECT+" "+acadeTrans.transfer_data[i][0].COURSE+" "+acadeTrans.transfer_data[i][0].TITLE;
                
                html_view +="<div style='margin:10px 0 0 0;font-size:10px;font-weight:normal;'><span style='float:left;width:28%;'>Grade: "+acadeTrans.transfer_data[i][0].GRADE+"</span><span style='float:left;width:40%;'>Credit Hours: "+acadeTrans.transfer_data[i][0].CREDIT_HOURS+"</span><span style='float:left;width:32%;'>Quality Points: "+acadeTrans.transfer_data[i][0].QUALITY_POINTS+"</span><div style='clear:both'></div></div></li>";
            }
            
        }
        for(i=0;i<acadeTrans.transfer_data_total.length;i++)
        {
            atmHrs=typeof acadeTrans.transfer_data_total[i][0].ATTEMPTED_HOURS == "undefined"?0:acadeTrans.transfer_data_total[i][0].ATTEMPTED_HOURS;
            passHrs=typeof acadeTrans.transfer_data_total[i][0].PASSED_HOURS == "undefined"?0:acadeTrans.transfer_data_total[i][0].PASSED_HOURS;
            earnHrs=typeof acadeTrans.transfer_data_total[i][0].EARNED_HOURS == "undefined"?0:acadeTrans.transfer_data_total[i][0].EARNED_HOURS;
            gpaHrs=typeof acadeTrans.transfer_data_total[i][0].GPA_HOURS == "undefined"?0:acadeTrans.transfer_data_total[i][0].GPA_HOURS;
            quaPoints=typeof acadeTrans.transfer_data_total[i][0].QUALITY_POINTS == "undefined"?0:acadeTrans.transfer_data_total[i][0].QUALITY_POINTS;
            
            html_view +="<li><span style='float:left;width:40%;font-size:14px;color:#006633;font-weight:bold; text-shadow:none;'>Total Transfer: </span><span class='custom-right-span custom-color-right-span'>Attempted Hours: "+atmHrs+"<br />Passed Hours: "+passHrs+"<br />Earned Hours:"+earnHrs+"<br />Quality Points: "+quaPoints+"<br />GPA Hours: "+gpaHrs+"</span><span style='float:left;width:40%;font-size:14px;color:#006633;font-weight:bold; text-shadow:none; word-wrap:break-word;margin:10px 0 0 0px;'></span><div style='clear:both'></div></li>";
            
        }
        
        html_view +="</ul>";
        
    }
        
        
        if(id=="institutioncredits")
        {
            
            html_view ="<ul  id='transcreditshow' data-role='listview' data-theme='c' data-inset='true'>";
            // html_view +="<li data-role='list-divider' class='ui-bar-f'>"+acadeTrans.studentInfo[0].ADMIT_TERM+" :<span style='float:right;margin:0 -2px 0 0;white-space:nowrap;width:90px;overflow:hidden;text-overflow:ellipsis;'></span></li>";
            for(i=0;i<acadeTrans.institutioncredits.length;i++)
            {
                //html_view +="<li>"+acadeTrans.institutioncredits[i].SHRTCKN_SUBJ_CODE+" "+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_NUMB+" "+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_TITLE+"<div>Grade: "+acadeTrans.institutioncredits[i].SHRTCKG_GRDE_CODE_FINAL+" Credit Hours: "+acadeTrans.institutioncredits[i].SHRTCKG_CREDIT_HOURS+"</span>Quality Point: "+acadeTrans.institutioncredits[i].SHRTCKG_QUAL_PTS+"</span><div style='clear:both'></div></li>";
                
                html_view +="<li>"+acadeTrans.institutioncredits[i].SHRTCKN_SUBJ_CODE+" "+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_NUMB+" "+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_TITLE;
                
                html_view +="<div style='margin:10px 0 0 0;font-size:10px;font-weight:normal;'><span style='float:left;width:32%;'>Grade: "+acadeTrans.institutioncredits[i].SHRTCKG_GRDE_CODE_FINAL+"</span><span style='float:left;width:32%;'>Credit Hours: "+acadeTrans.institutioncredits[i].SHRTCKG_CREDIT_HOURS+"</span><span style='float:left;width:35%;text-align:right;'>Quality Points: "+acadeTrans.institutioncredits[i].SHRTCKG_QUAL_PTS+"</span><div style='clear:both'></div></div></li>";
                
                
                
            }
            
            var ah = 0;
            var ph = 0;
            var eh = 0;
            var gpah = 0;
            var qp= 0;
            var GPA=0;
            
            
            for(i=0;i<acadeTrans.institutioncredits.length;i++)
            {
                ah += acadeTrans.institutioncredits[i].ATTEMPTED_HOURS;
                ph += acadeTrans.institutioncredits[i].SHRTCKG_PASS_HOURS;
                eh += acadeTrans.institutioncredits[i].SHRTCKG_EARN_HOURS;
                gpah +=acadeTrans.institutioncredits[i].SHRTCKG_GPA_HOURS;
                qp  += acadeTrans.institutioncredits[i].SHRTCKG_QUAL_PTS;
                GPA= gpah/acadeTrans.institutioncredits.length;
                GPA= parseFloat(GPA).toFixed(4)
            }
            
            html_view +="<li><span style='float:left;width:40%;font-size:14px;color:#006633;font-weight:bold; text-shadow:none;'>Total Institution: </span><span class='custom-right-span custom-color-right-span'>Attempted Hours: "+ah+" <br />Passed Hours: "+ph+"<br />Earned Hours: "+eh+"<br />Quality Points: "+qp+"<br />GPA Hours: "+gpah+"</span><div style='clear:both'></div></li>";
            
            html_view +="</ul>";
            
            
        }
        
        if(id=="transcriptstotal")
        {
            
            
            //html_view +="<li data-role='list-divider' class='ui-bar-f'>"+acadeTrans.studentInfo[0].ADMIT_TERM+" :<span style='float:right;margin:0 -2px 0 0;white-space:nowrap;width:90px;overflow:hidden;text-overflow:ellipsis;'></span></li>";
            
            for(i=0;i<acadeTrans.transcripttotal.length;i++)
            {
                //html_view +="<li>"+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_TITLE+"<span class='custom-right-span custom-color-right-span'>Grade :"+acadeTrans.institutioncredits[i].SHRTCKG_GRDE_CODE_FINAL+"<br />Credit Hours: "+acadeTrans.institutioncredits[i].SHRTCKG_CREDIT_HOURS+"</span><div style='clear:both'></div><span class='custom-left-spannew'>subject: "+acadeTrans.institutioncredits[i].SHRTCKN_SUBJ_CODE+" / Course: "+acadeTrans.institutioncredits[i].SHRTCKN_CRSE_NUMB+" / Quality Point: "+acadeTrans.institutioncredits[i].SHRTCKG_QUAL_PTS+"</span><div style='clear:both'></div></li>";
                
                var a = acadeTrans.transcripttotal[i].TRANSCRIPT_TYPE
                
                if(a=='I')
                {
                    html_view ="<ul id='totalTansferUL1' data-role='listview' data-theme='c' data-inset='true'>";
                    
                    html_view +="<li data-role='list-divider' class='ui-bar-f'>Total Institution</li>";
                    
                    html_view +="<li>Attempted Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].ATTEMPT_HOURS+"</li>";
                    
                    html_view +="<li>Passed Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].PASSED_HOURS+"</li>";
                    
                    html_view +="<li>Earned Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].EARNED_HOURS+"</li>";
                    
                    html_view +="<li>GPA Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA_HOURS+"</li>";
                    
                    html_view +="<li>Quality Points:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].QUALITY_POINTS+"</li>";
                    
                    var a = acadeTrans.transcripttotal[i].GPA.toString().substr(0, 5);
                    html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+a+"</li>";
                    
                    //html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA+"</li>";
                    
                    html_view +="</ul>";
                }
                
                else if(a=='T')
                {
                    html_view +="<ul id='totalTansferUL2' data-role='listview' data-theme='c' data-inset='true'>";
                    
                    html_view +="<li data-role='list-divider' class='ui-bar-f'>Total Transfer</li>";
                    
                    html_view +="<li>Attempted Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].ATTEMPT_HOURS+"</li>";
                    
                    html_view +="<li>Passed Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].PASSED_HOURS+"</li>";
                    
                    html_view +="<li>Earned Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].EARNED_HOURS+"</li>";
                    
                    html_view +="<li>GPA Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA_HOURS+"</li>";
                    
                    html_view +="<li>Quality Points:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].QUALITY_POINTS+"</li>";
                    var a = acadeTrans.transcripttotal[i].GPA.toString().substr(0, 5);
                    html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+a+"</li>";
                    
                    //html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA+"</li>";
                    
                    html_view +="</ul>";
                }
                
                else if(a=='Z')
                {
                    html_view +="<ul id='totalTansferUL3' data-role='listview' data-theme='c' data-inset='true'>";
                    
                    html_view +="<li data-role='list-divider' class='ui-bar-f'>Cumulative</li>";
                    
                    html_view +="<li>Attempted Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].ATTEMPT_HOURS+"</li>";
                    
                    html_view +="<li>Passed Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].PASSED_HOURS+"</li>";
                    
                    html_view +="<li>Earned Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].EARNED_HOURS+"</li>";
                    
                    html_view +="<li>GPA Hours:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA_HOURS+"</li>";
                    
                    html_view +="<li>Quality Points:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].QUALITY_POINTS+"</li>";
                    var a = acadeTrans.transcripttotal[i].GPA.toString().substr(0, 5);
                    html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+a+"</li>";
                    //html_view +="<li>GPA:<span class='custom-right-span custom-color-right-span'>"+acadeTrans.transcripttotal[i].GPA+"</li>";
                    
                    html_view +="</ul>";
                }
                
                
            }
            
            html_view +="</ul>";
            
        }
    }catch(e){alert(e)}
    //console.log(html_view);
    setTimeout("listRefreshHelper('transcreditshow')",100);
    
    setTimeout("listRefreshHelper('totalTansferUL1')",100);
    setTimeout("listRefreshHelper('totalTansferUL2')",100);
    setTimeout("listRefreshHelper('totalTansferUL3')",100);
    
    // alert($("#AcadefinaShowS").html());
    $("#AcadefinaShowS").html(html_view);
    
    
    
}


/**
 * sportsList()
 * <function Displaying Sports List>
 * @param {string} Results Variable holding JSON
 * @param {string} strMsg Error Message
 * @return {boolean}
 */
function sportsList(sportsListArray) {
    
    var html_view = "<div class='sport_list_sub' onclick=getBasketballList() ><a href='javascript:void(0)' class='sport_list_sub_tag'>Basketball</a><div class='sub-list' id='basketball_list'><a href='javascript:void(0)' onclick=commonSportNewsList('basketball','mbball','3') class='bd-top-btm'>Men's Basketball</a><a href='javascript:void(0)' onclick=commonSportNewsList('basketball2','wbball','8')>Women's Basketball</a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getCrossCountryList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Cross Country</a><div class='sub-list' id='getcountry_list'><a href='javascript:void(0)' onclick=commonSportNewsList('crosscountry','wcross','9') class='bd-top-btm'>Men's Cross Country</a><a href='javascript:void(0)' onclick=commonSportNewsList('crosscountry2','wcross','9')>Women's Cross Country</a></div></div>";
    
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('football','football','1') class='sport_list_sub_tag'>Football</a></div>";
    
    // html_view += "<div class='sport_list_sub' onclick=getLacroseList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Lacrosse</a><div class='sub-list' id='lacrosse_list'><a href='javascript:void(0)' onclick=commonSportNewsList('lacrosse','mlacrosse','13') class='bd-top-btm'>Men's Lacrosse</a><a href='javascript:void(0)' onclick=commonSportNewsList('lacrosse','wlacrosse','14')>Women's Lacrosse</a></div></div>";
    //   html_view += "<div class='sport_list_sub' onclick=getRowingList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Rowing</a><div class='sub-list' id='rowing_list'><a href='javascript:void(0)' onclick=commonSportNewsList('basketball','mbasket','6') class='bd-top-btm'>Men's Rowing</a><a href='javascript:void(0)' onclick=commonSportNewsList('basketball2','wbasket','7')>Women's Rowing</a></div></div>";
    html_view += "<div class='sport_list_sub' onclick=getSoccerList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Soccer</a><div class='sub-list' id='soccer_list'><a href='javascript:void(0)' onclick=commonSportNewsList('soccer','msoc','5') class='bd-top-btm'>Men's Soccer</a><a href='javascript:void(0)' onclick=commonSportNewsList('soccer','wsoc','11')>Women's Soccer</a></div></div>";
    // html_view += "<div class='sport_list_sub' onclick=getTrackFieldList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Track and Field</a><div class='sub-list' id='track_field'><a href='javascript:void(0)' onclick=commonSportNewsList('rowing','crew','9') class='bd-top-btm'>Men's Track and Field</a><a href='javascript:void(0)' onclick=commonSportNewsList('rowing2','rowing','15')>Women's Track and Field</a></div></div>";
    //  html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('fieldhockey','fhockey','11') class='sport_list_sub_tag'>Field Hockey</a></div>";
    //  html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('icehockey','wice','26') class='sport_list_sub_tag'>Ice Hockey</a></div>";
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('softball','softball','7') class='sport_list_sub_tag'>Softball</a></div>";
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('tennis','wten','12') class='sport_list_sub_tag'>Tennis</a></div>";
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonSportNewsList('volleyball','wvball','13') class='sport_list_sub_tag'>Volleyball</a></div>";
    
    $("#dummyPageList").html(html_view);
    setTimeout(stopActivitySpinner(),2000);
    $.mobile.changePage("#dummyPage",{transition:"none"});
}


function getBasketballList() {
    $('#basketball_list').toggle('slow');
}

function getCrossCountryList() {
    $('#getcountry_list').toggle('slow');
}

function getLacroseList() {
    $('#lacrosse_list').toggle('slow');
}

function getRowingList() {
    $('#rowing_list').toggle('slow');
}

function getSoccerList() {
    $('#soccer_list').toggle('slow');
}

function getTrackFieldList() {
    $('#track_field').toggle('slow');
}

function getHockeyList() {
    $('#hockey_list').toggle('slow');
}

function getTennisList() {
    $('#tennis_list').toggle('slow');
}


function commonSportNewsList(sportName, path, id) {
    startActivitySpinner();
    setTimeout(function(){sportListDescNews(sportName, path, id)},100);
}


/**
 * sportListDesc()
 * <function show a list of news for selected sport>
 * @param {string} sportName Variable holding sport name
 * @param {string} path Variable holding path
 * * @param {string} id Variable holding ID of sport
 */

function sportListDescNews(sportName, path, id) {
    
    $("#scedule").removeClass("sceduleactive");
    $("#score").removeClass("scoreactive");
    $("#news").addClass("newsactive");
    
    localStorage.sportName = sportName;
    localStorage.path = path;
    localStorage.id = id;
    $('#sports_Details_List_Display').empty();
    var html_view = "<ul id='ul_Sport_NewsList' data-role='listview' class='sports-list-background-color'>";
    var feed = new google.feeds.Feed(sportServerName+"/rss.aspx?tab="+sportName+"&path="+path);
    feed.setNumEntries(10);
    feed.setResultFormat(google.feeds.Feed.JSON);
    
    feed.load(function(result) {
              if (!result.error) {
              // In case of no data
              if(result.feed.entries.length == '0') {
              html_view += "<br/><br/><br/><br/><ul id='ul_Sport_NewsList' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No news available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              } else {
              var sportsListArray = [];
              for (var i = 0; i < result.feed.entries.length; i++) {
              var entry = result.feed.entries[i];
              
              sportsListArray.push(entry);
              }
              
              for(i=0; i<sportsListArray.length; i++) {
              
              var title=encodeURIComponent(sportsListArray[i].title);
              var date=encodeURIComponent(sportsListArray[i].publishedDate);
              var link=encodeURIComponent(sportsListArray[i].link);
              var image_url = sportsListArray[i].content.split('"');
              var image = encodeURIComponent(image_url[1]);
              var content=encodeURIComponent(image_url[2].substr(9));
              
              html_view += "<li ><a href='javascript:void(0)' onclick=sportNewsDetails(\""+title+"\",\""+date+"\",\""+content+"\",\""+link+"\",\""+image+"\") style='text-decoration:none; white-space:normal'><div class='sports-thumbnail' style='background:url("+ image_url[1] +")'></div><div class='sports-title-heading' ><div class='sports-title-header'>"+sportsListArray[i].title+"</div><div class='post_time' >Posted Date: "+sportsListArray[i].publishedDate.substr(5,12)+"<span class='holder_string'>"+sportsListArray[i].contentSnippet.substring(0,40)+"...</span></div></div></a></li>";
              
              }
              
              html_view += "</ul>";
              $("#sports_Details_List_Display").html(html_view);
              }
              } else {
              html_view += "<br/><br/><br/><br/><ul id='ul_Sport_NewsList' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No news available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              }
              });
    $("#sports_Details_Title").html("News");
    setTimeout("listRefreshHelper('ul_Sport_NewsList')",100);
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
    window.scrollTo('0', '0');
}



function sportListDescOption(option) {
    
    localStorage.sportTitle = option;
    switch(option) {
	       case "News":{
               startActivitySpinner();
               setTimeout(function(){sportListDescNews(localStorage.sportName,localStorage.path,localStorage.id)},100);
               $("#news").addClass("newsactive");
               $("#scedule").removeClass("sceduleactive");
               $("#score").removeClass("scoreactive");
               
               break;
           }
	       case "Schedule":{
               startActivitySpinner();
               setTimeout(function(){sportListDescSchedule(localStorage.id)},100);
               $("#scedule").addClass("sceduleactive");
               $("#news").removeClass("newsactive");
               $("#score").removeClass("scoreactive");
               break;
           }
	       case "Scores":{
               startActivitySpinner();
               setTimeout(function(){sportListDescScore(localStorage.sportName)},100);
               $("#score").addClass("scoreactive");
               $("#news").removeClass("newsactive");
               $("#scedule").removeClass("sceduleactive");
               break;
           }
    }
}


/**
 * sportListDescSchedule()
 * <function show a list of schedule for selected sport>
 * @param {string} id Variable holding sport ID
 */

function sportListDescSchedule(id) {
    
    $('#sports_Details_List_Display').empty();
    var html_view = "<ul id='ul_Sport_Desc_List' data-role='listview' class='sports-list-background-color'>";
    var server_url = sportServerName+"/calendar.ashx/calendar.rss?sport_id="+id;
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   contentType: 'text/xml',
                   crossDomain: true,
                   
                   success: function(responseDoc) {
                   try {
                   var result;
                   var string;
                   if(typeof(responseDoc) == 'string') {
                   result = $.xml2json(responseDoc);
                   } else {
                   string = (new XMLSerializer()).serializeToString(responseDoc);
                   result = $.xml2json(string);
                   }
                   
                   for(i=0; i<result.channel.item.length; i++) {
                   
                   var title = encodeURIComponent(result.channel.item[i].title.split("  ")[1]);
                   var date = encodeURIComponent(dateFormat(result.channel.item[i].startdate, "dddd, mmmm dS, yyyy, h:MM:ss TT"));
                   var opponentlogo;
                   var teamlogo = encodeURIComponent(result.channel.item[i].teamlogo);
                   var location = encodeURIComponent(result.channel.item[i].location);
                   
                   if(result.channel.item[i].opponentlogo == "") {
                   oplogo = "Settings/images/opponentlogo.png";
                   opponentlogo = "Settings/images/opponentlogo.png";
                   } else {
                   oplogo = result.channel.item[i].opponentlogo;
                   opponentlogo = encodeURIComponent(result.channel.item[i].opponentlogo);
                   }
                   var scDate = dateFormat(result.channel.item[i].startdate, "dddd, mmmm d, yyyy");
                   var scTime = dateFormat(result.channel.item[i].startdate, "h:MM TT");
                   html_view += "<li><a href='javascript:void(0)' onclick=sportScheduleDetails(\""+title+"\",\""+date+"\",\""+opponentlogo+"\",\""+teamlogo+"\",\""+location+"\")><div class='sports-schedule-thumbnail_scedule' style='background:url("+ oplogo +")'></div><div class='sports-title-heading'><div class='sports-title-header_scedule'>"+result.channel.item[i].title.split("  ")[1]+"</div><div class='post_time_schedule'>"+dateFormat(result.channel.item[i].startdate, "dddd, mmmm d, yyyy")+"<br>"+dateFormat(result.channel.item[i].startdate, "h:MM TT")+"</div></div></a></li>";
                   }
                   html_view += "</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   } catch(e) {}
                   
                   },
                   error: function(e) {
                   setTimeout(function(){stopActivitySpinner()},2000);
                   // In case of no data
                   if($('#sports_Details_List_Display').html() == "") {
                   var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Desc_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No Schedule available</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   }
                   },
                   
                   complete:function() {
                   // In case of no data
                   if($('#sports_Details_List_Display').html() == "") {
                   html_view += "<br/><br/><br/><br/><ul id='ul_Sport_Desc_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No Schedule available</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   }
                   }
                   });
    
    $("#sports_Details_Title").html("Schedule");
    $("#schedules").addClass("ui-btn-active");
    // setTimeout("listRefreshHelper('ul_Sport_Desc_List')",100);
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
    window.scrollTo('0', '0');
}


/**
 * sportNewsDetails()
 * <function show news details>
 * @param {string} title Variable holding news title
 * @param {string} date Variable holding news publish date
 * @param {string} content Variable holding news details
 * @param {string} link Variable holding web link for news
 * @param {string} image Variable holding news image
 */

function sportNewsDetails(title, date, content, link, image) {
    
    try{
        var title = decodeURIComponent(title);
        var date = decodeURIComponent(date);
        var content = decodeURIComponent(content);
        var link = decodeURIComponent(link);
        var image = decodeURIComponent(image);
        
        $("#match_details_title").removeClass();
        $("#match_details_image").removeClass();
        $("#match_details_content").removeClass();
        
        $("#match_details_title").addClass('match_details_head');
        $("#match_details_image").addClass('match_details_thumbnail');
        $("#match_details_content").addClass('match_details_holder');
        
        var matchDetailTitle = title+"<br/><span class='date_span'>"+date+"";
        matchDetailTitle += "</span>";
        $("#match_details_title").html(matchDetailTitle);
        
        var matchImage = "<div class='sports-image' style='background:url("+ image +")'></div>";
        $("#match_details_image").html(matchImage);
        
        var matchDetailsContent = content;
        $("#match_details_content").html(matchDetailsContent);
        
        var button ="<div class='ui-bar ui-bar-f match_details_button' onclick='openWebpage(\""+link+"\")'>View Full Article</div>";
        $("#match_details_button").html(button);
        
        $.mobile.changePage("#match_details");
        $("#match_details_toolbar_title").empty();
        $("#match_details_toolbar_title").html("News");
        $("#match_detail_back_btn").html("News");
    }catch(e){}
}


/**
 * sportScheduleDetails()
 * <function show news details>
 * @param {string} title Variable holding match schedule title
 * @param {string} date Variable holding match schedule date
 * @param {string} opponentlogo Variable holding image of opponent team logo
 * @param {string} teamlogo Variable holding image of team logo
 * @param {string} location Variable holding location details
 */

function sportScheduleDetails(title, date, opponentlogo, teamlogo, location) {
    
    try {
        var title = decodeURIComponent(title);
        var date = decodeURIComponent(date);
        var opponentlogo = decodeURIComponent(opponentlogo);
        var teamlogo = decodeURIComponent(teamlogo);
        var location = decodeURIComponent(location);
        
        $('#match_details_toolbar_title').empty();
        $('#match_details_title').empty();
        $('#match_details_image').empty();
        $('#match_details_content').empty();
        $('#match_details_button').empty();
        
        $("#match_details_title").removeClass();
        $("#match_details_title").addClass('match_details_schedule_head');
        
        $("#match_details_image").removeClass();
        $("#match_details_image").addClass('match_details_schedule_thumbnail');
        
        $("#match_details_content").removeClass();
        $("#match_details_content").addClass('match_schedule_location');
        
        var matchDetailTitle = title+"<br/><span class='date_span'>"+date+"</span>";
        $("#match_details_title").html(matchDetailTitle);
        
        var matchImage = "<p><span style='float: left'><img src="+opponentlogo+" class='sports_schedule_team_image'></span><span style='float: right'><img src='Settings/images/msu.jpeg' class='sports_schedule_team_image'></span></p>";
        $("#match_details_image").html(matchImage);
        
        var matchLocation = "<p>Game Information<br/><br/>Location: "+location+"</p>";
        $("#match_details_content").html(matchLocation);
        
        $("#match_details_toolbar_title").html("Schedule");
        $("#match_detail_back_btn").html("Schedule");
        $.mobile.changePage("#match_details");
    } catch(e) {}
}


/**
 * sportListDescScore()
 * <function show a list of score for selected sport>
 * @param {string} id Variable holding sport ID
 */
function sportListDescScore(sport) {
    var sportName = sport;
    if(sport == "basketball")
    {
        sportName = "Men's Basketball"
    }
    if(sport == "basketball2")
    {
        sportName = "Women's Basketball"
    }
    if(sport == "soccer")
    {
        sportName = "Men's Soccer"
    }
    if(sport == "soccer2")
    {
        sportName = "Women's Soccer"
    }
    if(sport == "icehockey")
    {
        sportName = "Women's Ice Hockey"
    }
    
    $('#sports_Details_List_Display').empty();
    var html_view = "<ul id='ul_score_List' data-role='listview' class='sports-list-background-color'>";
    var url = sportServerName+"/services/scores_chris.aspx?format=json";
    $.ajax({
           url: url,
           method: 'GET',
           success:function(data) {
           for(var i=0; i<data.scores.length; i++) {
           var homeTeam = data.scores[i].hometeam?data.scores[i].hometeam:"Not Available";
           homeTeam = homeTeam.replace("'", "");
           var opponentTeam = data.scores[i].opponent?data.scores[i].opponent:"Not Available";
           opponentTeam = opponentTeam.replace("'", "");
           var date = data.scores[i].date?data.scores[i].date:"Not Available";
           date = date.replace("'", "");
           var location = data.scores[i].location.location?data.scores[i].location.location:"Not Available";
           location = location.replace("'", "");
           var teamScore = data.scores[i].team_score?data.scores[i].team_score:"X";
           teamScore  = teamScore .replace("'", "");
           var opponentScore = data.scores[i].opponent_score?data.scores[i].opponent_score:"X";
           opponentScore = opponentScore.replace("'", "");
           var hometeamLogo = data.scores[i].hometeam_logo?data.scores[i].hometeam_logo:"http://gonitzuoka.files.wordpress.com/2011/03/xx-1.jpg";
           hometeamLogo = hometeamLogo.replace("'", "");
           var opponentLogo = data.scores[i].opponent_logo?data.scores[i].opponent_logo:"http://gonitzuoka.files.wordpress.com/2011/03/xx-1.jpg";
           opponentTeam = opponentTeam.replace("'", "");
           var result = data.scores[i].result?data.scores[i].result:"Not Available";
           result = result.replace("'", "");
           
           if(data.scores[i].sport.toLowerCase().replace(/ /g,"") == sportName.toLowerCase().replace(/ /g,""))
           {
           html_view += "<li style='width:100%;'><div style='width:96%; padding:5px 2%;'><a href='javascript:void(0)' onclick='scoreDetails(\""+homeTeam+"\",\""+opponentTeam+"\",\""+date+"\",\""+location+"\",\""+teamScore+"\",\""+opponentScore+"\",\""+hometeamLogo+"\",\""+opponentLogo+"\",\""+result+"\" )' style='text-decoration:none; white-space:normal'><div class='sports-title-heading' >"+data.scores[i].hometeam+" Vs. "+data.scores[i].opponent+"<p class='post_time_schedule'>"+data.scores[i].date+"</p></div> <div style='float:right;width:28%;'><div class='score_display' style=' margin-right:3px;'>"+(data.scores[i].team_score?data.scores[i].team_score:"X")+"</div><div class='score_display'>"+(data.scores[i].opponent_score?data.scores[i].opponent_score:"X")+"</div><div style='float:left; font-weight:bold;font-size:13px;color:#fff;width:100%;text-align:center;margin-top:4px;'>"+data.scores[i].result+"</div></div></a></div></li>";
           }
           
           }
           
           html_view += "</ul>";
           $("#sports_Details_List_Display").html(html_view);
           
           
           },
           error: function(e) {
           setTimeout(function(){stopActivitySpinner()},2000);
           // In case of no data
           if($('#sports_Details_List_Display').html() == "") {
           var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No scores available</ul>";
           $("#sports_Details_List_Display").html(html_view);
           }
           },
           
           complete:function() {
           // In case of no data
           if($('#ul_score_List').html() == "") {
           html_view += "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No scores available</ul>";
           $("#sports_Details_List_Display").html(html_view);
           }
           }
           
           });
    
    $("#sports_Details_Title").html("Scores");
    $("#scores").addClass("ui-btn-active");
    setTimeout(listRefreshHelper('ul_score_List'),100);
    
    if(myCampusObject.settings.androidphone){
        $("#sports_Details_List_Display").css({'min-height':window.innerHeight});
    }
    
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
    window.scrollTo('0', '0');
}

/**
 * scoreDetails()
 * <function show a list of score details>
 * @param {string} id Variable holding sport ID
 */
function scoreDetails(homeTeam, opponent, date, location, teamScore, opponentScore, hometeamLogo, opponentLogo, result) {
    
    $('#match_details_toolbar_title').empty();
    $('#match_details_title').empty();
    $('#match_details_image').empty();
    $('#match_details_content').empty();
    $('#match_details_button').empty();
    
    $("#match_details_title").removeClass();
    $("#match_details_title").addClass('match_details_schedule_head');
    
    $("#match_details_image").removeClass();
    $("#match_details_image").addClass('score_details_thumbnail');
    
    $("#match_details_content").removeClass();
    $("#match_details_content").addClass('match_schedule_location');
    
    $("#match_details_button").removeClass();
    try{
        
        var resultFormat = result;
        if(resultFormat == "W")
        {
            resultFormat = "WON";
        }
        else if(resultFormat == "L")
        {
            resultFormat = "LOSS";
        }
        else
        {
            resultFormat = "Not Available";
        }
        
        var scoreDetailTitle = homeTeam+" Vs. "+opponent;
        //$("#match_details_toolbar_title").html(scoreDetailTitle);
        
        var scoreDetailSubTitle = scoreDetailTitle+"<br/>"+date;
        $("#match_details_title").html(scoreDetailSubTitle);
        
        
        var scoreImage = "<div class='score_holder_left'><img src='Settings/images/msu.jpeg' class='score-image float_left'/><div class='score_info float_left' >"+teamScore+"</div></div > <div class='score_holder_right'><img src="+opponentLogo+" class='score-image float_left'/> <div class='score_info float_right' >"+opponentScore+"</div> </div>";
        $("#match_details_image").html(scoreImage);
        
        
        var matchLocation = "<p>Game Information<br/><br/>Location: "+location+"<br/><br/>Result: "+resultFormat+"</p>";
        $("#match_details_content").html(matchLocation);
        
        $.mobile.changePage("#match_details");
        $("#match_details_toolbar_title").html("Summary");
        $("#match_detail_back_btn").html("Scores");
        
    }catch(e){}
}


//------------- Implementation of Beacons Athletics --------------//

/**
 * beaconsSportList()
 * <function Displaying Sports List>
 */
function beaconsSportList() {
    
    var html_view = "<div class='sport_list_sub' onclick=commonCall('m-basebl')><a href='javascript:void(0)'  class='sport_list_sub_tag'>baseball</a></div>";
    html_view += "<div class='sport_list_sub' onclick=getBasketballList() ><a href='javascript:void(0)' class='sport_list_sub_tag'>basketball</a><div class='sub-list' id='basketball_list'><a href='javascript:void(0)' onclick=commonCall('m-baskbl') class='bd-top-btm'>Men's Basketball</a><a href='javascript:void(0)' onclick=commonCall('w-baskbl')>Women's Basketball</a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getCrossCountryList()><a href='javascript:void(0)'  class='sport_list_sub_tag'>Cross Country</a><div class='sub-list' id='getcountry_list'><a href='javascript:void(0)' onclick=commonCall('m-xc') class='bd-top-btm'>Men's Cross Country</a><a href='javascript:void(0)' onclick=commonCall('w-xc')>Women's Cross Country</a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getHockeyList() ><a href='javascript:void(0)'  class='sport_list_sub_tag'>Hockey</a><div class='sub-list' id='hockey_list'><a href='javascript:void(0)' onclick=commonCall('m-hockey') class='bd-top-btm'>Men's Hockey</a><a href='javascript:void(0)' onclick=commonCall('w-hockey')> Women's Hockey </a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=commonCall('m-lacros')><a href='javascript:void(0)'  class='sport_list_sub_tag'>Lacrosse</a></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getSoccerList()><a href='javascript:void(0)' class='sport_list_sub_tag'>Soccer</a><div class='sub-list' id='soccer_list'><a href='javascript:void(0)' onclick=commonCall('m-soccer') class='bd-top-btm'>Men's Soccer</a><a href='javascript:void(0)' onclick=commonCall('w-soccer')>Women's Soccer</a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getTennisList() ><a href='javascript:void(0)' class='sport_list_sub_tag'>Tennis</a><div class='sub-list' id='tennis_list'><a href='javascript:void(0)' onclick=commonCall('m-tennis') class='bd-top-btm'>Men's Tennis</a><a href='javascript:void(0)' onclick=commonCall('w-tennis')>Women's Tennis</a></div></div>";
    
    html_view += "<div class='sport_list_sub' onclick=getTrackFieldList() ><a href='javascript:void(0)'  class='sport_list_sub_tag'>Track and Field</a><div class='sub-list' id='track_field'><a href='javascript:void(0)' onclick=commonCall('m-track') class='bd-top-btm'>Men's Track and Field</a><a href='javascript:void(0)' onclick=commonCall('w-track')>Women's Track and Field</a></div></div>";
    
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonCall('w-softbl') class='sport_list_sub_tag'>Softball</a></div>";
    
    html_view += "<div class='sport_list_sub'><a href='javascript:void(0)' onclick=commonCall('w-volley') class='sport_list_sub_tag'>Volleyball</a></div>";
    
    
    $("#dummyPageList").html(html_view);
    setTimeout(stopActivitySpinner(),2000);
    $.mobile.changePage("#dummyPage",{transition:"none"});
}


function commonCall(param) {
    var local = param;
    startActivitySpinner();
    setTimeout(function(){beaconsSportNewsListDesc(local)},100);
}


/**
 * beaconsSportListDesc()
 * <function show a list of news for selected sport>
 * @param {string} sportName Variable holding sport name
 * @param {string} path Variable holding path
 * @param {string} id Variable holding ID of sport
 */

function beaconsSportNewsListDesc(beaconsSportName) {
    
    $('#sports_Details_List_Display').empty();
    localStorage.beaconsSportName = beaconsSportName;
    var currentYear = new Date().getFullYear() +'';
    var nextYear = parseInt(currentYear.substring(2)) + 1;
    var html_view = "<ul id='beacons_Sport_News_List' data-role='listview' class='sports-list-background-color'>";
    var feed = new google.feeds.Feed(sportServerName+"/sports/"+localStorage.beaconsSportName+"/"+currentYear+"-"+nextYear+"/news?print=rss");
    
    feed.setNumEntries(100);
    feed.setResultFormat(google.feeds.Feed.JSON);
    
    feed.load(function(result) {
              if (!result.error) {
              // In case of no data
              if(result.feed.entries.length == '0') {
              html_view += "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No news available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              } else {
              var sportsListArray = [];
              for (var i = 0; i < result.feed.entries.length; i++) {
              var entry = result.feed.entries[i];
              sportsListArray.push(entry);
              }
              }
              
              for(i=0; i<sportsListArray.length; i++) {
              
              var title=encodeURIComponent(sportsListArray[i].title);
              var date=encodeURIComponent(dateFormat(sportsListArray[i].publishedDate, "dddd, mmmm dS, yyyy, h:MM:ss TT"));
              var link=encodeURIComponent(sportsListArray[i].link);
              var image = encodeURIComponent(sportsListArray[i].mediaGroups[0].contents[0].url);
              var content=encodeURIComponent(sportsListArray[i].content);
              
              html_view += "<li><a href='javascript:void(0)' onclick=beaconsNewsDetails(\""+title+"\",\""+date+"\",\""+content+"\",\""+link+"\",\""+image+"\") style='text-decoration:none; white-space:normal'><img src="+sportsListArray[i].mediaGroups[0].contents[0].url+" class='sports-thumbnail'><div class='sports-title-heading' >"+sportsListArray[i].title+"<p class='post_time' >Posted Date: "+dateFormat(sportsListArray[i].publishedDate, "dddd, mmmm dS, yyyy, h:MM:ss TT")+"<br /><br /><span class='holder_string'>"+sportsListArray[i].content.substring(0,35)+"...</span></p></div></a></li>";
              }
              
              html_view += "</ul>";
              $("#sports_Details_List_Display").html(html_view);
              
              } else {
              html_view += "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No news available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              }
              });
    $("#sports_Details_Title").html("News");
    setTimeout("listRefreshHelper('beacons_Sport_News_List')",10);
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
}


/**
 * beaconsNewsDetails()
 * <function show news details>
 * @param {string} title Variable holding news title
 * @param {string} date Variable holding news publish date
 * @param {string} content Variable holding news details
 * @param {string} link Variable holding web link for news
 * @param {string} image Variable holding news image
 */

function beaconsNewsDetails(title, date, content, link, image) {
    try{
        var title = decodeURIComponent(title);
        var date = decodeURIComponent(date);
        var content = decodeURIComponent(content);
        var link = decodeURIComponent(link);
        var image = decodeURIComponent(image);
        
        $("#match_details_title").removeClass();
        $("#match_details_image").removeClass();
        $("#match_details_content").removeClass();
        
        $("#match_details_title").addClass('match_details_head');
        $("#match_details_image").addClass('match_details_thumbnail');
        $("#match_details_content").addClass('match_details_holder');
        
        var matchDetailTitle = title+"<br/><span class='date_span'>"+date+"";
        matchDetailTitle += "</span>";
        $("#match_details_title").html(matchDetailTitle);
        
        var matchImage = "<img src="+image+" class='sports-image'>";
        $("#match_details_image").html(matchImage);
        
        var matchDetailsContent = content;
        $("#match_details_content").html(matchDetailsContent);
        
        var button ="<div class='ui-bar ui-bar-f match_details_button' onclick='openWebpage(\""+link+"\")'>View Full Article</div>";
        $("#match_details_button").html(button);
        
        $.mobile.changePage("#match_details");
        $("#match_details_toolbar_title").empty();
        $("#match_details_toolbar_title").html("News");
        $("#match_detail_back_btn").html("News");
    }catch(e){}
}


/**
 * checkAthleticsServerName()
 * <function check server name to open selected tab>
 * @param {string} option Variable holding selected tab name
 */

function checkAthleticsServerName(option) {
    if(sportServerName == "http://www.msumustangs.com") {
        sportListDescOption(option);
    } else if(sportServerName == "http://www.beaconsathletics.com") {
        beaconsSportListcOption(option);
    }
    
}

/**
 * sportListDescOption()
 * <function show list for selected option>
 * @param {string} option Variable holding selected tab name
 */

function beaconsSportListcOption(option) {
    localStorage.sportTitle = option;
    switch(option)
    {
        case "News":{
            startActivitySpinner();
            setTimeout(function(){beaconsSportNewsListDesc(localStorage.beaconsSportName)},100);
            break;
        }
        case "Schedule":{
            startActivitySpinner();
            setTimeout(function(){beaconsSportScheduleListDesc()},100);
            break;
        }
        case "Scores":{
            startActivitySpinner();
            setTimeout(function(){beaconsSportScoreList()},100);
            break;
        }
    }
}


/**
 * beaconsSportScheduleListDesc()
 * <function show schedule list for selected sport>
 */

function beaconsSportScheduleListDesc() {
    
    $('#sports_Details_List_Display').empty();
    var currentYear = new Date().getFullYear() +'';
    var nextYear = parseInt(currentYear.substring(2)) + 1;
    var feed = new google.feeds.Feed(sportServerName+"/sports/"+localStorage.beaconsSportName+"/"+currentYear+"-"+nextYear+"/schedule?print=rss");
    
    feed.setNumEntries(100);
    feed.setResultFormat(google.feeds.Feed.JSON);
    
    feed.load(function(result) {
              
              if (!result.error) {
              // In case of no data
              if(result.feed.entries.length == '0') {
              var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No Schedule available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              } else {
              var sportsListArray = [];
              for (var i = 0; i < result.feed.entries.length; i++) {
              var entry = result.feed.entries[i];
              sportsListArray.push(entry);
              }
              }
              
              var html_view = "<ul id='beacons_Sport_schedule_list' data-role='listview' class='sports-list-background-color'>";
              
              for(i=0; i<sportsListArray.length; i++) {
              
              var title=encodeURIComponent(sportsListArray[i].title);
              var date=encodeURIComponent(dateFormat(sportsListArray[i].publishedDate, "dddd, mmmm dS, yyyy, h:MM:ss TT"));
              var link=encodeURIComponent(sportsListArray[i].link);
              var content = encodeURIComponent(sportsListArray[i].content);
              
              html_view += "<li><a href='javascript:void(0)' onclick=beaconsScheduleDetails(\""+title+"\",\""+date+"\",\""+content+"\")><div class='sports-title-heading' style='width:100% !important'>"+sportsListArray[i].title+"<br /><p class='post_time'>"+dateFormat(sportsListArray[i].publishedDate, "dddd, mmmm dS, yyyy, h:MM:ss TT")+"</p></div></a></li>";
              
              }
              
              html_view += "</ul>";
              $("#sports_Details_List_Display").html(html_view);
              } else {
              var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No Schedule available</ul>";
              $("#sports_Details_List_Display").html(html_view);
              }
              });
    $("#sports_Details_Title").html("Schedule");
    $("#schedules").addClass("ui-btn-active");
    setTimeout("listRefreshHelper('beacons_Sport_schedule_list')",10);
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
}


/**
 * beaconsScheduleDetails()
 * <function show news details>
 * @param {string} title Variable holding match schedule title
 * @param {string} date Variable holding match schedule date
 * @param {string} description Variable holding description of match
 */

function beaconsScheduleDetails(title, date, description) {
    try {
        var title = decodeURIComponent(title);
        var date = decodeURIComponent(date);
        var description = decodeURIComponent(description);
        
        $('#match_details_toolbar_title').empty();
        $('#match_details_title').empty();
        $('#match_details_image').empty();
        $('#match_details_content').empty();
        $('#match_details_button').empty();
        
        $("#match_details_title").removeClass();
        $("#match_details_title").addClass('match_details_schedule_head');
        
        $("#match_details_image").removeClass();
        // $("#match_details_image").addClass('match_details_schedule_thumbnail');
        
        $("#match_details_content").removeClass();
        $("#match_details_content").addClass('match_schedule_location');
        
        var matchDetailTitle = title+"<br/><span class='date_span'>"+date+"</span>";
        $("#match_details_title").html(matchDetailTitle);
        
        //var matchImage = "<p><span style='float: left'><img src="+opponentlogo+" class='sports_schedule_team_image'></span><span style='float: right'><img src="+teamlogo+" class='sports_schedule_team_image'></span></p>";
        //$("#match_details_image").html(matchImage);
        
        var matchLocation = "<p>Game Information<br/><br/><span style='font-size:12px !important;'>Match Description: "+description+"</span></p>";
        $("#match_details_content").html(matchLocation);
        
        $.mobile.changePage("#match_details");
        $("#match_details_toolbar_title").html("Schedule");
        $("#match_detail_back_btn").html("Schedule");
    } catch(e) {}
}

//------------------score part by Rajesh ---------------//

/**
 ** <function show a list of score for selected sport(using prestosport)>
 * @param {string} id Variable holding sport ID
 *
 */
function beaconsSportScoreList() {
    
    $('#sports_Details_List_Display').empty();
    var html_view = "<ul id='beacons_Sport_Score_List' data-role='listview' class='sports-list-background-color'>";
    var currentYear = new Date().getFullYear() +'';
    var nextYear = parseInt(currentYear.substring(2)) + 1;
    var server_url = sportServerName+"/sports/"+localStorage.beaconsSportName+"/"+currentYear+"-"+nextYear+"/schedule?print=rss";
    
    var xhr=$.ajax({
                   url: server_url,
                   type: "GET",
                   contentType: 'text/xml',
                   crossDomain: true,
                   success: function(responseDoc) {
                   
                   var string = (new XMLSerializer()).serializeToString(responseDoc);
                   var result = $.xml2json(string);
                   
                   for(var i=0; i<result.channel.item.length; i++) {
                   var today = new Date();
                   var yesterday = new Date(result.channel.item[i].pubDate);
                   if(yesterday < today) {
                   
                   var homeTeam="";
                   var opponentTeam="";
                   var dateNew="";
                   var description="";
                   var teamScore="";
                   var opponentScore="";
                   
                   try {homeTeam = splitZeroReplaceNumber(result.channel.item[i].title);} catch(e){}
                   if(homeTeam == undefined) {homeTeam = "";}
                   try {opponentTeam = splitOneReplaceNumber(result.channel.item[i].title);} catch(e){}
                   if(opponentTeam == undefined) {opponentTeam = "";}
                   try {dateNew = encodeURIComponent(dateFormat(result.channel.item[i].date, "dddd, mmmm dS, yyyy, h:MM:ss TT"));} catch(e){}
                   if(dateNew == undefined) {dateNew = "";}
                   try {description = spitLocation(result.channel.item[i].description);} catch(e){}
                   try {teamScore = splitZeroReplaceAlp(result.channel.item[i].title);} catch(e){}
                   if(teamScore == undefined) {teamScore = "";}
                   try {opponentScore = splitOneReplaceAlp(result.channel.item[i].title);} catch(e){}
                   if(opponentScore == undefined) {opponentScore == "";}
                   try {
                   html_view += "<li style='width:100%;'><div style='width:96%; padding:5px 2%;'><a href='javascript:void(0)' onclick='prestoScoreDetails(\""+homeTeam+"\",\""+opponentTeam+"\",\""+dateNew+"\",\""+description+"\",\""+teamScore+"\",\""+opponentScore+"\",\""+null+"\",\""+null+"\",\""+null+"\" )' style='text-decoration:none; white-space:normal'><div class='sports-title-heading'>"+homeTeam+" Vs. "+opponentTeam+"<p class='' style='color:#808080 !important;'>"+dateFormat(result.channel.item[i].date, "dddd, mmmm dS, yyyy, h:MM:ss TT")+"</p></div> <div style='float:right;width:28%;'><div class='score_display' style=' margin-right:3px;'>"+teamScore+"</div><div class='score_display'>"+opponentScore+"</div><div style='float:left; font-weight:bold;font-size:13px;color:#fff;width:100%;text-align:center;'></div></div></a></div></li>";
                   } catch(e){}
                   }
                   }
                   html_view += "</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   
                   },
                   error: function(e) {
                   setTimeout(function(){stopActivitySpinner()},2000);
                   // In case of no data
                   if($('#sports_Details_List_Display').html() == "") {
                   var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No scores available</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   }
                   },
                   
                   complete:function() {
                   // In case of no data
                   if($('#beacons_Sport_Score_List').html() == "") {
                   var html_view = "<br/><br/><br/><br/><ul id='ul_Sport_Name_List' data-role='listview' class='sports-list-background-color' style='font-weight:normal;font-size:16px;text-align:center;color:#fff;'>No scores available</ul>";
                   $("#sports_Details_List_Display").html(html_view);
                   }
                   }
                   });
    
    $("#sports_Details_Title").html("Scores");
    $("#scores").addClass("ui-btn-active");
    setTimeout("listRefreshHelper('beacons_Sport_Score_List')",10);
    $.mobile.changePage("#sports_Details_List",{transition:"none"});
    setTimeout(function(){stopActivitySpinner()},2000);
}

//setTimeout("listRefreshHelper('ul_first')",10);
/**
 * prestoScoreDetails()
 * <function show a list of score details>
 * @param {string} id Variable holding sport ID
 */
function prestoScoreDetails(homeTeam, opponent, date, location, teamScore, opponentScore, hometeamLogo, opponentLogo, result) {
    
    var date = decodeURIComponent(date);
    $('#match_details_toolbar_title').empty();
    $('#match_details_title').empty();
    $('#match_details_image').empty();
    $('#match_details_content').empty();
    $('#match_details_button').empty();
    
    $("#match_details_title").removeClass();
    $("#match_details_title").addClass('match_details_schedule_head');
    
    $("#match_details_image").removeClass();
    $("#match_details_image").addClass('score_details_thumbnail');
    
    $("#match_details_content").removeClass();
    $("#match_details_content").addClass('match_schedule_location');
    try{
        
        var resultFormat = result;
        if(resultFormat == "W")
        {
            resultFormat = "WON";
        }
        else if(resultFormat == "L")
        {
            resultFormat = "LOSS";
        }
        else
        {
            resultFormat = "Not Available";
        }
        if(!hometeamLogo)
        {
            hometeamLogo = "Settings/images/opponentlogo.png";
        }
        if(!opponentLogo)
        {
            opponentLogo = "Settings/images/opponentlogo.png";
        }
        
        var scoreDetailTitle = homeTeam+" Vs. "+opponent;
        //$("#match_details_toolbar_title").html(scoreDetailTitle);
        
        var scoreDetailSubTitle = scoreDetailTitle+"<br/><span class='date_span'>"+date+"</span>";
        $("#match_details_title").html(scoreDetailSubTitle);
        
        
        //var scoreImage = "<div class='score_holder_left'><img src="+hometeamLogo+" class='score-image float_left'/><div class='score_info float_left' >"+teamScore+"</div></div > <div class='score_holder_right'><img src="+opponentLogo+" class='score-image float_left'/> <div class='score_info float_right' >"+opponentScore+"</div> </div>";
        //$("#match_details_image").html(scoreImage);
        
        var scoreImage = "<div class='score_holder_left'><div class='score_info float_left' style='margin-left:35px'>"+teamScore+"</div></div > <div class='score_holder_right'><div class='score_info float_right' style='margin-right:35px'>"+opponentScore+"</div> </div>";
        $("#match_details_image").html(scoreImage);
        
        
        var matchLocation = "<p onclick='prestoSportScore()'>Game Information<br/><br/>"+location+"<br/><br/>Result: "+resultFormat+"</p>";
        $("#match_details_content").html(matchLocation);
        
        $.mobile.changePage("#match_details");
        $("#match_details_toolbar_title").html("Summary");
        $("#match_detail_back_btn").html("Scores");
        
    }catch(e){}
}


//Baseball on Mar 10, 2012 at 12:00 PM: FDU-Florham vs. UMass Boston, Winter Haven, Fla. - Chain of Lakes, Final, 7-9
/** function to split perform etc **/

function splitZeroReplaceNumber(str)
{
    var a = str.split(',')[0];
    var b = a.replace(/[0-9]/g, '');
    return b;
}

function splitOneReplaceNumber(str)
{
    var a = str.split(',')[1];
    var b = a.replace(/[0-9]/g, '');
    var c= b.split('Final')[0];
    return c;
}

function splitZeroReplaceAlp(str)
{
    var a = str.split(',')[0];
    var b = a.replace(/[A-Za-z$-]/g, '');
    var c = b.replace(/[^a-zA-Z0-9]/g,'');
    var d = c.split('')[0];
    return d;
}

function splitOneReplaceAlp(str)
{
    var a = str.split(',')[1];
    var b = a.replace(/[A-Za-z$-]/g, '');
    var c = b.replace(/[^a-zA-Z0-9]/g,'');
    var d = c.split('')[0];
    return d;
}

function spitLocation(str)
{
    var a = str.split(',')[2];
    var b = str.split(',')[3];
    var c = "";
    if(a === undefined) {
        c = b;
    } else if(b === undefined) {
        c = a;
    } else {
        c = a + " + " + b;
    }
    
    return c;
}




function CalDaysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}
function fetchEvents(CurrentDate)
{
    var Sun=new Array();
    var Mon=new Array();
    var Tue=new Array();
    var Wed=new Array();
    var Thu=new Array();
    var Fri=new Array();
    var Sat=new Array();
    var arrayWeekdays=[Sun,Mon,Tue,Wed,Thu,Fri,Sat];
    var weekday=["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
    $("#ArrWeekDays").html("<div class='ArrayWeekDays' id='Sun'></div><div class='ArrayWeekDays' id='Mon'></div><div class='ArrayWeekDays' id='Tue'></div><div class='ArrayWeekDays' id='Wed'></div><div class='ArrayWeekDays' id='Thu'></div><div class='ArrayWeekDays' id='Fri'></div><div class='ArrayWeekDays' id='Sat'></div>");
    
    var k=1;
    tdyDate = new Date(CurrentDate);
    var year=tdyDate.getFullYear();
    var month=tdyDate.getMonth();
    var currDay=tdyDate.getDate();
    var daysInMonth=CalDaysInMonth(month+1,year);
    var PredaysInMonth=CalDaysInMonth(month,year);
    var NextdaysInMonth=CalDaysInMonth(month+2,year);
    daysToNextMonth=daysInMonth+1;
    daysToPrevMonth=currDay;
    //alert(daysInMonth+"  "+currDay+"  "+daysToNextMonth);
    $(".monthYear").html(CurrentMonth[month]+" "+year);
    var firstDayDate=new Date(tdyDate);
    firstDayDate.setDate(1);
    var lastDayDate=new Date(tdyDate);
    lastDayDate.setDate(daysInMonth);
    var day=firstDay=firstDayDate.getDay();
    var lastDay=lastDayDate.getDay();
    
    for(var i=day-1;i>=0;i--){
        $("#"+weekday[i]).append("<div id='"+weekday[i]+""+PredaysInMonth+"' style='background-color:#808080;border:1px solid #336699;text-align:center;height:18px'>"+PredaysInMonth+"<div>");
        PredaysInMonth=PredaysInMonth-1;}
    
    for(var i=1;i<=9;i++){
        if(day<7)
        {
            arrayWeekdays[day].push(i);
            day=day+1;
        }
        else
        {
            day=0;
            arrayWeekdays[day].push(i);
            day=day+1;
        }
    }
    for(var i=10;i<=daysInMonth;i++){
        if(day<7)
        {
            arrayWeekdays[day].push(i);
            day=day+1;
        }
        else
        {
            day=0;
            arrayWeekdays[day].push(i);
            day=day+1;
        }
    }
    console.log(JSON.stringify(arrayWeekdays));
    for(var i=0;i<arrayWeekdays.length;i++)
    {
        for(j=0;j<arrayWeekdays[i].length;j++)
        {
            dayId=weekday[i]+"-"+CurrentMonth[month].substr(0,3)+"-"+arrayWeekdays[i][j]+"-"+year;
            
            var patt=weekday[i]+" "+CurrentMonth[month].substr(0,3)+" "+arrayWeekdays[i][j]+", "+year;

            
            foundDateEvent=false;
            for(var e=0;e<CalEvententries.length;e++)
            {
                var Datepatt=new RegExp(patt,"i");
               // alert(CalEvententries[e].content+" "+patt);
                if(CalEvententries[e].content.match(Datepatt))
                {
                    
                    foundDateEvent=true;
                }
            }
            if(foundDateEvent == true)
            {
                $("#"+weekday[i]).append("<div><div id='"+dayId+"' style='background-color:#ffffff;border:1px solid #336699;text-align:center;height:18px' onclick='getEventDay(\""+patt+"\",this.id)'>"+arrayWeekdays[i][j]+"<span style='position:absolute;margin:1px 1px 0 0;font-size:500;color:#0B941C'>-</span></div>");
            }
            else{
                $("#"+weekday[i]).append("<div id='"+dayId+"' style='background-color:#ffffff;border:1px solid #336699;text-align:center;height:18px' onclick='getEventDay(\""+patt+"\",this.id)'>"+arrayWeekdays[i][j]+"</div>");
            }
        }
    }
    
    for(var i=lastDay+1;i<=6;i++){$("#"+weekday[i]).append("<div id='"+weekday[i]+""+k+"' style='background-color:#808080;border:1px solid #336699;text-align:center;height:18px'>0"+k+"</div>");
        k=k+1;}
    dayId=weekday[tdyDate.getDay()]+"-"+CurrentMonth[month].substr(0,3)+"-"+currDay+"-"+year;
    console.log(CurrentDayID);
    $("#"+dayId).css({'background-color': '#E60000','color': '#ffffff'});
    $("#"+CurrentDayID).css({'background-color': '#0B941C','color':'#ffffff','border':'1px solid #0B941C'});
    var patt=weekday[tdyDate.getDay()]+" "+CurrentMonth[month].substr(0,3)+" "+currDay+", "+year;
    $.mobile.changePage("#CalendarEvents");
    getEventDay(patt,dayId);
}

function getEventDay(patt,id)
{
    prevDayId=activeDayId;
    activeDayId=id;
    var activeDay={'background-color': '#E60000','color': '#ffffff'}
    var prevDay={'background-color': '#ffffff','color': '#000000'}
    $("#"+activeDayId).css(activeDay);
    $("#"+prevDayId).css(prevDay);
    //id=decodeURIComponent(id);
    var Datepatt=new RegExp(patt,"i");
    var html_view ="";
    //var html_view = "<ul id='ul_Calendar_List' data-role='none'>";

    var inputDate = patt.split(" ");
    
    var date;
    switch(inputDate[0]){
        case 'Mon':{
            date="Monday,"
            break;
        }
        case 'Tue':{
            date="Tuesday,"
            break;
        }
        case 'Wed':{
            date="Wednesday,"
            break;
        }
        case 'Thu':{
            date="Thursday,"
            break;
        }
        case 'Fri':{
            date="Friday,"
            break;
        }
        case 'Sat':{
            date="Saturday,"
            break;
        }
        case 'Sun':{
            date="Sunday,"
            break;
        }
    }
    date =date+" "+inputDate[1]+" "+inputDate[2]+" "+inputDate[3];
    var date1=encodeURIComponent(date);
    
    var found=0;
    for(var i=0;i<CalEvententries.length;i++)
    {
        if(CalEvententries[i].content.match(Datepatt)){
            found=found+1;
            var title=encodeURIComponent(CalEvententries[i].title);
            var content=encodeURIComponent(CalEvententries[i].content);
            var link=encodeURIComponent(CalEvententries[i].link);
            
            
            html_view += "<div style='background-image:-webkit-gradient(linear,left top,left bottom,from(#006633),to(#006633));color:#fff;padding:10px 5px;border-bottom:0.5px solid #006633;margin-top:5px;border-radius:2px;' id='"+i+"' onclick=RSSEventsDesc(\""+title+"\",\""+date1+"\",\""+content+"\",\""+link+"\",this.id)><div style='width:90%;float:left;color:#fff; font-size:14px;font-weight:bold;word-wrap:break-word;'>"+CalEvententries[i].title+"<br/>Event starts at: "+date+"</div><div style='float:right' class='ui-icon ui-icon-arrow-r ui-icon-shadow'></div><div style='clear:both;'></div></div>";
            
        }
    }
    
    //html_view += "</ul>";
    // $("#Eventscroller").css({'height':'0'});
    $("#Eventscroller1").html(html_view);
    if(found == 0)
    {
        $("#Eventscroller1").html("<div style='color:#ffffff;text-align:center;text-shadow:none'><b>No events found on "+date+"</b></div>");
    }
    setTimeout("listRefreshHelper('ul_Calendar_List')",100);
    /*if(myScroll){myScroll.destroy();}
    myScroll = new iScroll('Eventwrapper');*/
}


function DisplayDirectory(direntry)
{
    
    $("#DirHeader").html(direntry.header);
    $("#StuTitle").html(direntry.title);
    $("#StuStatus").html(direntry.status);
    var html_view="";
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Holds :</b></legend>";
    
    html_view +="<ul id='holdDirectory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.Hold.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.Hold[i].holdTitle+"</li>";
        html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.Hold[i].holdDate+"</span></li>";
        html_view +="<li onclick=openWebpage('"+direntry.Hold[i].holdUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    
    $("#DirectoryContent").html(html_view);
    setTimeout("listRefreshHelper('holdDirectory')",100);
    
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Admission :</b></legend>";
    html_view +="<ul id='AdmissionDirectory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.Admission.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.Admission[i].admTitle+"</li>";
        html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.Admission[i].admDate+"</span></li>";
        html_view +="<li onclick=openWebpage('"+direntry.Admission[i].admUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    $("#DirectoryContent").append(html_view);
    setTimeout("listRefreshHelper('AdmissionDirectory')",100);
    
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Financial Aid :</b></legend>";
    html_view +="<ul id='FinAid1213Directory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.FinAid1213.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.FinAid1213[i].FinAidTitle+"</li>";
        //html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.FinAid1213[i].admDate+"</span></li>";
        html_view +="<li onclick=openWebpage('"+direntry.FinAid1213[i].FinAidUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    $("#DirectoryContent").append(html_view);
    setTimeout("listRefreshHelper('FinAid1213Directory')",100);
    
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Financial Aid :</b></legend>";
    html_view +="<ul id='FinAid1314Directory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.FinAid1314.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.FinAid1314[i].FinAidTitle+"</li>";
        //html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.FinAid1213[i].admDate+"</span></li>";
        html_view +="<li onclick=openWebpage('"+direntry.FinAid1314[i].FinAidUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    $("#DirectoryContent").append(html_view);
    setTimeout("listRefreshHelper('FinAid1314Directory')",100);
    
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Financial Aid Holds :</b></legend>";
    html_view +="<ul id='FinAidHoldDirectory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.FinAidHold.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.FinAidHold[i].FinAidHoldTitle+"</li>";
        //html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.FinAid1213[i].admDate+"</span></li>";
        //html_view +="<li onclick=openWebpage('"+direntry.FinAidHold[i].FinAidUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    $("#DirectoryContent").append(html_view);
    setTimeout("listRefreshHelper('FinAidHoldDirectory')",100);
    
    html_view ="<fieldset style='border:1px solid #ffffff;float:left;width:280px;margin-top:12px;padding-left:5px;padding-right:5px'><legend style='color:#ffffff;text-shadow: 0px 2px 3px #000000;'><b>Primary Major :</b></legend>";
    html_view +="<ul id='priMajorDirectory' data-theme='c' data-inset='true'>";
    for(var i=0;i<direntry.priMajor.length;i++)
    {
        html_view +="<li  data-role='list-divider' class='ui-btn ui-bar-f'>"+direntry.priMajor[i].recordMajorTitle+"</li>";
        //html_view +="<li>Date:<span class='custom-right-span custom-color-right-span'>"+direntry.FinAid1213[i].admDate+"</span></li>";
        //html_view +="<li onclick=openWebpage('"+direntry.FinAidHold[i].FinAidUrl+"')><a>Open Link:</a></li>"
    }
    html_view +="</ul></fieldset>";
    
    
    $("#DirectoryContent").append(html_view);
    setTimeout("listRefreshHelper('priMajorDirectory')",100);
    
    pageChange("StudentDirectory");
    
}

function displayDemoTwitterPageFeeds(input) {
    setTimeout(stopActivitySpinner(),1500);
    var html_view ="";
    var html_view = "<ul id='demo_feeds_List'  data-role='listview' data-theme='d'>";
    var length = input.length;
    for(i=0;i<length;i++)     {
        var urlIndex=-1;
        var title=input[i].user.name;
        var date=input[i].created_at;
        var content=input[i].text;
        var link=input[i].user.profile_image_url;
        var RpcContent=content.replace(/#/g,' ');
        urlIndex=RpcContent.lastIndexOf('http');
        if(urlIndex == -1)         {
            html_view += "<li data-theme='d' data-icon='false'><a style='white-space:normal'><div style='width:15%;float:left'><img src='"+link+"' width='90%'/></div><div style='width:85%; float:left'><div class='news-articles-heading-left' >"+title+"</div><span class='news-articles-subheading-left'>";         }
        else         {
            //var urlEndIndex=content.indexOf('#',urlIndex);
            var url=RpcContent.substr(urlIndex,22);
            html_view += "<li data-theme='d' data-icon='false' onclick=openWebpage('"+url+"')><a style='white-space:normal'><div style='width:15%;float:left'><img src='"+link+"' width='90%'/></div><div style='width:85%; float:left'><div class='news-articles-heading-left' >"+title+"</div><span class='news-articles-subheading-left'>";         }
        html_view += "Posted on: "+date+"</span>";
        html_view += "<p style='white-space:normal; padding:10px 0 5px 0; color:#333'>"+content+"</p></div><p style='clear:both'></p></a></li>"     }     html_view += "</ul>";     $("#demoTwitterPage_demo").html(html_view);
    setTimeout("listRefreshHelper('demo_feeds_List')" );
    $.mobile.changePage("#demoTwitterPage",{transition:"none"});
}