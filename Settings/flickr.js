var myScroll;
var photoArray=[];
var value=0;
var page;
var total;
var a = {};
getAlbums = function() {
    
    startActivitySpinner();
    var url = 'https://api.flickr.com/services/rest/?&method=flickr.photosets.getList&api_key=107bee6a2410d698a01a2ac77f5ba3e0&user_id=27836742@N03&format=json&jsoncallback=?';
    
    
    html_view = '';
    
    $.getJSON(url, function(data) {
              
              
              
              
              $.each(data.photosets.photoset, function(idx, item) {
                     
                     
                     
                     var photosetid = item.id;
                     
                     
                     var primary = item.primary;
                     Album_title = item.title._content;
                     var total =  item.photos;
                     var secret = item.secret;
                     var server = item.server;
                     var farm = item.farm;
                     
                     var photo_url = 'http://farm' + farm + '.static.flickr.com/' + server + '/' + primary + '_' + secret + '_s.jpg';
                     
                     
                     var photo_urlmediumsize = 'http://farm' + farm + '.static.flickr.com/' + server + '/' + primary + '_' + secret + '_m.jpg';
                     
                     html_view+="<div class='item_con images'>";
                     html_view+="<div class='image_con'><a style='text-decoration:none;color:#3E7E60' onclick=RetrieveAlbumImages('"+photosetid+"','"+encodeURIComponent(item.title._content)+"')><img src='"+ photo_url +"'/></div>";
                     html_view+="<div class='image_title'><div class='title ellipses'>"+ item.title._content +"</div></div></a>";
                     html_view+="</div>";
                     
                     });
              setTimeout(function(){stopActivitySpinner();},2000);
              
              
              $("#albumsdisplay").html(html_view);
              
              $.mobile.changePage("#photosPage");
              
              });
    
}



function RetrieveAlbumImages(photosetid,albumname)
{
    startActivitySpinner();
    var decodedAlbumName = decodeURIComponent(albumname);
    var html_newview;
    var flickr_content="";
    var title;
    var photosurl = 'https://api.flickr.com/services/rest/?&method=flickr.photosets.getPhotos&api_key=2cbbceafd3a1fa8fee4bc082141c0142&photoset_id=' + photosetid+ '&media=photos&format=json&jsoncallback=?';
    
    $.getJSON(photosurl, function(data) {
              var i=0;
              $.each(data.photoset.photo, function(idx, photo) {
                     var photoid = photo.id;
                     var secretkey = photo.secret;
                     var serverkey = photo.server;
                     var farmkey = photo.farm;
                     var photo_url = 'http://farm' + farmkey + '.static.flickr.com/' + serverkey + '/' + photoid + '_' + secretkey + '_s.jpg';
                     var photo_urlOriginalsize = 'http://farm' + farmkey + '.static.flickr.com/' + serverkey + '/' + photoid + '_' + secretkey + '_b.jpg';
                     var ImageTitle = photo.title;
                     total = data.photoset.total;
                     //flickr_content +="<div style='margin:10px 0px 0px 18.5px;float:left;'><a href='"+photo_urlOriginalsize+"' ><img class='gallery' src='"+photo_url+"' alt = '"+ImageTitle+"'></a></div>";
                     flickr_content +="<div style='width:96%;height:auto;margin:0 0 0 4%'><div style='width:29%;height:75px;margin:3% 0 0 3%;float:left;text-align:center;'><a href='"+photo_urlOriginalsize+"' ><img class='gallery' src='"+photo_url+"' alt = '"+ImageTitle+"'></a></div></div>"
                     
                     
                     
                     });
              
              $("#pictureDisplay").html(flickr_content);
              
              
              $("#GalleryImageNames").html(decodedAlbumName);
              $("#totalPictures").html(total); 
              $.mobile.changePage("#PicturesFromAlbumsPage");
              (function(window, $, PhotoSwipe){
               
               $(document).ready(function(){
                                 
                                 var options = {};
                                 $("#pictureDisplay a").photoSwipe(options);
                                 
                                 });
               
               
               }(window, window.jQuery, window.Code.PhotoSwipe));
              
              
              });
    setTimeout(function(){stopActivitySpinner();},4000);
    
    
}
