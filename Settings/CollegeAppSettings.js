/*
 *
 *
 */

var serverName;
var serviceName;
var statusBarTile;
var statusBarMessage;
var serviceNamePreLogin;

var collegeName;
var universityName;
var appVersion="2.0.0";

if(myCampusObject.settings.isCollegeBuild)
{
    
    
    serverName = "https://my.enmu.edu";
    //serviceName = "https://my.enmu.edu";
    
    //serverName = "https://mobile-pprd.campuseai.org";
    serviceName = "https://mobile-pprd.campuseai.org";
    
    serverNameMsg = "https://my.enmu.edu";
    serviceNameMsg = "https://my.enmu.edu";
    
    sportServerName = "http://www.msumustangs.com";
    serviceNamePreLogin = "https://educause2010.campuseai.org";
    collegeName="myENMU";
    universityName="Eastern New Mexico University";
    statusBarTile ="";
    statusBarMessage = "";
    
}
else
{
    /**
     * Generic Build
     */
    serverName = "";
    serviceName = "";
    //serviceNamePreLogin = "https://educause2010.campuseai.org";
    statusBarTile ="";
    statusBarMessage = "";
}
/*
 * JSON for Map locations
 *
 */
var mapList={
    "map00" :{lat:"34.177324" ,long:"-103.360346" ,desc:"<div>West Campus parking for Bldgs. 1-2</div>", imgurl:"red.png"},
    "map01" :{lat:"34.176535" ,long:"-103.360131" ,desc:"<div>West Campus parking for Bldgs. 3-4</div>", imgurl:"red.png"},
    "map02" :{lat:"34.175576" ,long:"-103.359777" ,desc:"<div>West Campus parking for Bldgs. 5-6</div>", imgurl:"red.png"},
    "map03" :{lat:"34.176348" ,long:"-103.359016" ,desc:"<div>West Campus parking for Bldgs. 7-8</div>", imgurl:"red.png"},
    "map04" :{lat:"34.177964" ,long:"-103.359434" ,desc:"<div>West Campus parking for Bldgs. 9-10</div>", imgurl:"red.png"},
    "map05" :{lat:"34.176854" ,long:"-103.359713" ,desc:"<div>West campus apartments</div>", imgurl:"red.png"},
    "map06" :{lat:"34.180911" ,long:"-103.351932" ,desc:"<div>Greyhound Arena parking</div>", imgurl:"red.png"},
    "map07" :{lat:"34.180119" ,long:"-103.351554" ,desc:"<div>Greyhound Arena Athletics</div>", imgurl:"red.png"},
    "map08" :{lat:"34.182721" ,long:"-103.352712" ,desc:"<div>President's House</div>", imgurl:"red.png"},
    
    "map09" :{lat:"34.18269" ,long:"-103.349729" ,desc:"<div>Zeta Tau Alpha House</div>", imgurl:"red.png"},
    "map10" :{lat:"34.180121" ,long:"-103.346333" ,desc:"<div>Kappa Sigma House</div>", imgurl:"red.png"},
    "map11" :{lat:"34.183393" ,long:"-103.342116" ,desc:"<div>Sigma Chi House</div>", imgurl:"red.png"},
    "map12" :{lat:"34.178350" ,long:"-103.348952" ,desc:"<div>Jack Williamson Liberal Arts Building</div>", imgurl:"red.png"},
    "map13" :{lat:"34.178386" ,long:"-103.349006" ,desc:"<div>Music</div>", imgurl:"red.png"},
    "map14" :{lat:"34.178203" ,long:"-103.347439" ,desc:"<div>Golden Library</div>", imgurl:"red.png"},
    "map15" :{lat:"34.178026" ,long:"-103.346471" ,desc:"<div>Golden Library Parking</div>", imgurl:"red.png"},
    
    
    "map16" :{lat:"34.179924" ,long:"-103.354923" ,desc:"<div>Soccer Field</div>", imgurl:"red.png"},
    
    "map17" :{lat:"34.181887" ,long:"-103.351666" ,desc:"<div>Athletic Practice Field2</div>", imgurl:"red.png"},
    
    "map18" :{lat:"34.177697" ,long:"-103.355475" ,desc:"<div>Athletic Practice Field1</div>", imgurl:"red.png"},
    
    "map19" :{lat:"34.175568" ,long:"-103.356926" ,desc:"<div>Football Fieldhouse</div>", imgurl:"red.png"},
    
    "map20" :{lat:"34.178275" ,long:"-103.35801" ,desc:"<div>Baseball Field</div>", imgurl:"red.png"},
    
    "map21" :{lat:"34.173721" ,long:"-103.363329" ,desc:"<div>Rodeo Arena</div>", imgurl:"red.png"},
    
    "map22" :{lat:"34.173898" ,long:"-103.364691" ,desc:"<div>Rodeo Building</div>", imgurl:"red.png"},
    
    "map23" :{lat:"34.250084" ,long:"-103.249544" ,desc:"<div>Blackwater Draw Museum</div>", imgurl:"red.png"},
    
    "map24" :{lat:"34.252045" ,long:"-103.245397" ,desc:"<div>Greyhound Stadium Parking Lot</div>", imgurl:"red.png"},
    
    "map25" :{lat:"34.252458" ,long:"-103.244075" ,desc:"<div>Greyhound Stadium</div>", imgurl:"red.png"},
    
    "map26" :{lat:"34.177307" ,long:"-103.347268" ,desc:"<div>Science Building</div>", imgurl:"red.png"},
    
    
    "map27" :{lat:"34.174946" ,long:"-103.351664" ,desc:"<div>Purchasing Parking</div>", imgurl:"red.png"},
    
    "map28" :{lat:"34.175265" ,long:"-103.351623" ,desc:"<div>Purchasing Building</div>", imgurl:"red.png"},
    
    "map29" :{lat:"34.174636" ,long:"-103.351991" ,desc:"<div>Art Annex Parking</div>", imgurl:"red.png"},
    
    "map30" :{lat:"34.174386" ,long:"-103.351548" ,desc:"<div>Art Annex</div>", imgurl:"red.png"},
    
    "map31" :{lat:"34.173868" ,long:"-103.350526" ,desc:"<div>Agriculture Parking</div>", imgurl:"red.png"},
    
    "map32" :{lat:"34.174156" ,long:"-103.350808" ,desc:"<div>Agriculture</div>", imgurl:"red.png"},
    
    "map33" :{lat:"34.18177" ,long:"-103.349944" ,desc:"<div>Zia Softball Field</div>", imgurl:"red.png"},
    
    "map34" :{lat:"34.181497" ,long:"-103.350998" ,desc:"<div>Athletic Weight Training Facility</div>", imgurl:"red.png"},
    
    "map35" :{lat:"34.181346" ,long:"-103.353489" ,desc:"<div>Golf Driving Range</div>", imgurl:"red.png"},
    
    "map36" :{lat:"34.179959" ,long:"-103.353549" ,desc:"<div>Track and Field</div>", imgurl:"red.png"},
    
    "map37" :{lat:"34.174214" ,long:"-103.349411" ,desc:"<div>Vetville Recreation Area and Tennis Courts</div>", imgurl:"red.png"},
    
    "map38" :{lat:"34.175524" ,long:"-103.349138" ,desc:"<div>College of Business</div>", imgurl:"red.png"},
    
    "map39" :{lat:"34.174777" ,long:"-103.349617" ,desc:"<div>College of Business Parking</div>", imgurl:"red.png"},
    
    "map40" :{lat:"34.172563" ,long:"-103.347399" ,desc:"<div>Bernalillo Hall</div>", imgurl:"red.png"},
    
    "map41" :{lat:"34.171786" ,long:"-103.34697" ,desc:"<div>Bernalillo Hall Parking</div>", imgurl:"red.png"},
    
    "map42" :{lat:"34.173189" ,long:"-103.346932" ,desc:"<div>Guadalupe Hall</div>", imgurl:"red.png"},
    
    
    "map43" :{lat:"34.172754" ,long:"-103.346712" ,desc:"<div>Guadalupe Hall Parking</div>", imgurl:"red.png"},
    
    "map44" :{lat:"34.175075" ,long:"-103.344862" ,desc:"<div>San Juan Village</div>", imgurl:"red.png"},
    
    "map45" :{lat:"34.174964" ,long:"-103.3455" ,desc:"<div>San Juan Village Clubhouse</div>", imgurl:"red.png"},
    
    "map46" :{lat:"34.174977" ,long:"-103.345226" ,desc:"<div>San Juan Village Pool</div>", imgurl:"red.png"},
    
    "map47" :{lat:"34.17528" ,long:"-103.345088" ,desc:"<div>San Juan Village Parking</div>", imgurl:"red.png"},
    
    "map48" :{lat:"34.17429" ,long:"-103.346385" ,desc:"<div>Eddy Hall</div>", imgurl:"red.png"},
    
    "map49" :{lat:"34.174338" ,long:"-103.3458" ,desc:"<div>Eddy Hall Parking</div>", imgurl:"red.png"},
    
    "map84" :{lat:"34.179961" ,long:"-103.34638" ,desc:"<div>ENMU Campus Police</div>", imgurl:"red.png"},
    
    "map85" :{lat:"34.17952" ,long:"-103.346394" ,desc:"<div>ENMU Campus Police Parking</div>", imgurl:"red.png"},
    
    "map50" :{lat:"34.175537" ,long:"-103.348157" ,desc:"<div>Campus Union Building</div>", imgurl:"red.png"},
    
    "map51" :{lat:"34.174076" ,long:"-103.348115" ,desc:"<div>Campus Union Parking</div>", imgurl:"red.png"},
    
    "map52" :{lat:"34.175301" ,long:"-103.350797" ,desc:"<div>Communications Center</div>", imgurl:"red.png"},
    
    "map53" :{lat:"34.175688" ,long:"-103.350746" ,desc:"<div>Communications Center Parking</div>", imgurl:"red.png"},
    
    "map54" :{lat:"34.176243" ,long:"-103.350624" ,desc:"<div>Art and Anthropology Building</div>", imgurl:"red.png"},
    
    "map55" :{lat:"34.176138" ,long:"-103.349771" ,desc:"<div>College of Education and Technology</div>", imgurl:"red.png"},
    
    "map56" :{lat:"34.175617" ,long:"-103.349834" ,desc:"<div>College of Education and Technology Parking</div>", imgurl:"red.png"},
    
    "map57" :{lat:"34.176738" ,long:"-103.350659" ,desc:"<div>Family and Consumer Sciences</div>", imgurl:"red.png"},
    
    "map58" :{lat:"34.176904" ,long:"-103.351153" ,desc:"<div>Child Development Center</div>", imgurl:"red.png"},
    
    "map59" :{lat:"34.176695" ,long:"-103.351878" ,desc:"<div>Child Development Center Parking</div>", imgurl:"red.png"},
    
    "map60" :{lat:"34.178" ,long:"-103.350537" ,desc:"<div>University Theater Center</div>", imgurl:"red.png"},
    
    "map61" :{lat:"34.17744" ,long:"-103.351098" ,desc:"<div>University Theater Center Parking</div>", imgurl:"red.png"},
    
    "map62" :{lat:"34.175443" ,long:"-103.347361" ,desc:"<div>Lea Hall</div>", imgurl:"red.png"},
    
    "map63" :{lat:"34.17583" ,long:"-103.347351" ,desc:"<div>Post Office</div>", imgurl:"red.png"},
    
    "map64" :{lat:"34.175967" ,long:"-103.347764" ,desc:"<div>University Bookstore</div>", imgurl:"red.png"},
    
    "map65" :{lat:"34.176823" ,long:"-103.347421" ,desc:"<div>University Greenhouse</div>", imgurl:"red.png"},
    
    "map66" :{lat:"34.17709" ,long:"-103.347951" ,desc:"<div>Natatorium</div>", imgurl:"red.png"},
    
    "map67" :{lat:"34.176331" ,long:"-103.347238" ,desc:"<div>Roosevelt Hall</div>", imgurl:"red.png"},
    
    "map68" :{lat:"34.176393" ,long:"-103.346498" ,desc:"<div>Roosevelt Hall Parking</div>", imgurl:"red.png"},
    
    "map69" :{lat:"34.177363" ,long:"-103.348612" ,desc:"<div>Greyhound Fountain</div>", imgurl:"red.png"},
    
    "map70" :{lat:"34.177507" ,long:"-103.347726" ,desc:"<div>Ecology Habitat</div>", imgurl:"red.png"},
    "map71" :{lat:"34.178359" ,long:"-103.349883" ,desc:"<div>University Computer Center</div>", imgurl:"red.png"},
    
    "map72" :{lat:"34.178741" ,long:"-103.349872" ,desc:"<div>University Computer Center Parking</div>", imgurl:"red.png"},
    
    "map73" :{lat:"34.178741" ,long:"-103.346567" ,desc:"<div>Baptist Student Union</div>", imgurl:"red.png"},
    
    "map74" :{lat:"34.179034" ,long:"-103.346428" ,desc:"<div>Presbyterian Center</div>", imgurl:"red.png"},
    
    "map75" :{lat:"34.179008" ,long:"-103.348001" ,desc:"<div>Administration Building</div>", imgurl:"red.png"},
    
    "map76" :{lat:"34.179762" ,long:"-103.348241" ,desc:"<div>Administration Building Parking</div>", imgurl:"red.png"},
    
    "map77" :{lat:"34.277713" ,long:"-103.324002" ,desc:"<div>Blackwater Draw Site</div>", imgurl:"red.png"},
    
    "map78" :{lat:"34.179029" ,long:"-103.349067" ,desc:"<div>Student Academic Services</div>", imgurl:"red.png"},
    
    "map79" :{lat:"34.179668" ,long:"-103.347453" ,desc:"<div>Harding Hall</div>", imgurl:"red.png"},
    
    "map80" :{lat:"34.179571" ,long:"-103.346991" ,desc:"<div>Curry Hall</div>", imgurl:"red.png"},
    
    "map81" :{lat:"34.180219" ,long:"-103.346964" ,desc:"<div>Quay Hall</div>", imgurl:"red.png"},
    
    "map82" :{lat:"34.179016" ,long:"-103.34707" ,desc:"<div>Harding/Curry/Quay Hall Parking</div>", imgurl:"red.png"},
    
    "map83" :{lat:"34.179673" ,long:"-103.348542" ,desc:"<div>Roosevelt County Museum</div>", imgurl:"red.png"},
    
    
    
    "map86" :{lat:"34.179975" ,long:"-103.346491" ,desc:"<div>ENMU Foundation</div>", imgurl:"red.png"},
    
    "map87" :{lat:"34.180377" ,long:"-103.346392" ,desc:"<div>Christian Campus House</div>", imgurl:"red.png"},
    
    "map88" :{lat:"34.18053" ,long:"-103.346376" ,desc:"<div>Church of Christ Student Center</div>", imgurl:"red.png"},
    
}
/*
 * JSON for Main Dashboard
 *
 */
var UIPreLoginListObject =
{
    "D300001" : { name: "Directory", title: "Directory",id:"Directory", url:"directory.png", enabled:true, servername:"http://www.enmu.edu/mobile/enmu-directory.xml"},
    //"D300002" : { name: "Courses",title: "Courses", id:"Courses", url:"Course.png", enabled:true, servername:"http://www.simons-rock.edu/np/cg/courses.xml" },
    
    "D300003" : { name: "news", title: "News",id:"News", url:"news.png", enabled:true, servername:"http://enmuweb.wordpress.com/feed/"},
    
    "D300004" : { name: "Events", title: "Events",id:"Events", url:"event-calendar.png", enabled:true, servername:"https://www.google.com/calendar/feeds/4cj37to01kguk75cjso6rr3opg%40group.calendar.google.com/public/basic"},
    
    "D300005" : { name: "Maps", title: "Map",id:"Maps", url:"map.png", enabled:true, servername:"https://educause2010.campuseai.org"},
    
    "D300006" : { name: "Video", title: "Videos", id:"Videos", url:"Video.png", enabled:true, servername:""},
    
    "D300007" : { name: "athletic", title: "Athletics", id:"athletic", url:"Athletics.png", enabled:true, servername:"http://www.goeasternathletics.com/mobile/"},
    
    "D300008" : { name: "Facebook", title: "Facebook",id:"Facebook", url:"facebook.png", enabled:true, servername:"http://www.facebook.com/pages/Portales-NM/Eastern-New-Mexico-University/20827430409"},
    "D300009" : { name: "Twitter", title: "Twitter",id:"Twitter", url:"Twitter.png", enabled:true, servername:"http://www.twitter.com/enmu"},
    "D300010" : { name: "Emergency", title: "Emergency",id:"Emergency", url:"emergency.png", enabled:true, servername:"https://educause2010.campuseai.org"},
    //"D300011" : { name: "ImpEmergency", title: "Important Numbers",id:"ImpEmergency", url:"ImportantNumbers.png", enabled:true, servername:"https://educause2010.campuseai.org"},
    "D300012" : { name: "flickr",title: "Photos", id:"flickr", url:"pictures.png", enabled:true, servername:""},
    "D300013" : { name: "library",title: "Library",id:"Library", url:"library.png", enabled:true, servername:"http://www.enmu.edu/library-mobile"},
    //"D300014" : { name: "donate",title: "Donate",id:"Donate", url:"Donate.png", enabled:true, servername:"https://www.applyweb.com/public/contribute?rockcont"},
    
    
};

/*
 * JSON for Emergency
 *
 */

var UIEmergencyObject =
{
    "E300000" : { name: "Universal Emergency", number:"911", enabled:true},
    "E300001" : { name: "ENMU Police", number:"575.562.2392", enabled:true},
    "E300002" : { name: "ENMU Police, After Hours", number:"575.760.2945", enabled:true},
    "E300003" : { name: "Physical Plant", number:"575.562.2511", enabled:true},
    "E300004" : { name: "Housing/Residence Life", number:"575.562.2632", enabled:true},
    "E300005" : { name: "University Health Services", number:"575.562.2321", enabled:true},
    "E300006" : { name: "Information Desk", number:"575.562.1011", enabled:true},
    "E300007" : { name: "Counseling Services", number:"575.562.2211", enabled:true},
    "E300008" : { name: "Portales Police Department", number:"575.356.4404", enabled:true},
    "E300009" : { name: "Roosevelt General Hospital", number:"575.359.1800", enabled:true},
    
};

var UIImportantObject =
{
    "E300000" : { name: "Main Switchboard", number:"413 644-4400", enabled:true},
    //"E300001" : { name: "A-Z Staff/Faculty Listings", number:"413 528-7247", enabled:true},
    "E300002" : { name: "Academic Affairs", number:"413 528-7247", enabled:true},
    "E300003" : { name: "Admission", number:"413 528-7228", enabled:true},
    "E300004" : { name: "Admission 800", number:"800 235-7186", enabled:true},
    "E300005" : { name: "Book Store (MBS Direct)", number:"800 325-3252", enabled:true},
    "E300006" : { name: "Business Office", number:"413 528-7206", enabled:true},
    "E300007" : { name: "Business Office Alternate", number:"413 528-7391", enabled:true},
    "E300008" : { name: "Campus Rentals", number:"413 528-4220", enabled:true},
    "E300009" : { name: "Communications", number:"413 528-7209", enabled:true},
    "E300010" : { name: "Communications Alternate", number:"413 528-7229", enabled:true},
    "E300011" : { name: "Counseling Services", number:"413 528-7353", enabled:true},
    "E300012" : { name: "Dining Hall (Chartwells)", number:"413 528-7290", enabled:true},
    "E300013" : { name: "Financial Aid", number:"413 528-7297", enabled:true},
    "E300015" : { name: "Health Services", number:"413 528-7353", enabled:true},
    "E300016" : { name: "Information Technology Services (ITS)", number:"413 528-7371", enabled:true},
    "E300017" : { name: "Institutional Advancement", number:"413 528-7266", enabled:true},
    "E300018" : { name: "Kilpatrick Athletic Center", number:"413 528-7777", enabled:true},
    "E300019" : { name: "Library", number:"413 528-7370", enabled:true},
    "E300020" : { name: "Media Contact", number:"413 644-4706", enabled:true},
    "E300021" : { name: "Payroll and Benefits", number:"413 528-7205", enabled:true},
    "E300022" : { name: "Physical Plant", number:"413 528-7208", enabled:true},
    "E300023" : { name: "Provost's Office", number:"413 528-7239", enabled:true},
    "E300024" : { name: "Registrar", number:"413 528-7201", enabled:true},
    "E300025" : { name: "Security", number:"413 528-7291", enabled:true},
    "E300026" : { name: "Student Accounts", number:"413 528-7206", enabled:true},
    "E300027" : { name: "Student Affairs", number:"413 528-7693", enabled:true},
    "E300028" : { name: "Win Student Resource Commons", number:"413 528-7444", enabled:true},
    
};
/*
 * JSON for Feedback
 *
 */
var UIFeedbackObject = { FeedbackTo : "Web.Master@enmu.edu", FeedbackCc: "support@getmycampusmobile.com" };

/*
 * JSON for LoggedIn DashBoard
 *
 */
var ThreeDLoggedListObject =

{
    //"LOGGED00" : { name: "inbox", description:"My Messages", id:"21", imageUrl:"Settings/images/MyMessages.png", enabled:true,roles:"redirect.via.cas=https://educause2010.campuseai.org",rolesAllowed:"Public"},
    "LOGGED01" : { name: "sendmsg", description:"Send Messages", id:"23", imageUrl:"Settings/images/send-message.png", enabled:true,servername:"redirect.via.cas=https://educause2010.campuseai.org",rolesAllowed:"MessageAdmin"},
    
    //"LOGGED16" : { name: "Banner", description:"Banner", id:"banner", imageUrl:"Settings/images/banner.png", enabled:true,roles:"redirect.via.cas=https://educause2010.campuseai.org"},
    "LOGGED02": {
    name: "Holds",
    description: "Holds",
    id: "Holds",
    imageUrl: "Settings/images/Holds.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    /* "LOGGED03": {
     name: "Midterm Grades",
     description: "Midterm Grades",
     id: "MidtermGrades",
     imageUrl: "Settings/images/MidTerm.png",
     enabled: true,
     roles: "",
     rolesAllowed:"Student,Administrator"
     },*/
    "LOGGED04": {
    name: "Final Grades",
    description: "Final Grades",
    id: "FinalGrades",
    imageUrl: "Settings/images/Grades.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    "LOGGED05": {
    name: "Look Up Classes",
    description: "Look Up<div /> Classes",
    id: "LookUpClasses",
    imageUrl: "Settings/images/LookupClasses.png",
    enabled: true,
    servername: "",
    rolesAllowed:"Public"
    },
    "LOGGED06": {
    name: "Registration Status",
    description: "Registration<div>Status</div>",
    id: "registration",
    imageUrl: "Settings/images/Registration.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    "LOGGED07": {
    name: "Account Summary",
    description: "Account<div>Summary</div>",
    id: "AccountSummary",
    imageUrl: "Settings/images/account_summary.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    "LOGGED08": {
    name: "Week at Glance",
    description: "Week At<div />a Glance",
    id: "WAG",
    imageUrl: "Settings/images/WeekAtGlance.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    "LOGGED09": {
    name: "Transcript",
    description: "Academic<div>Transcript</div>",
    id: "transcript",
    imageUrl: "Settings/images/Academic.png",
    enabled: true,
    servername: "",
    rolesAllowed:"public"
    },
    "LOGGED10" : { name: "Student Registration",description:"Student<div>Registration</div>", id:"StudentRegistration", imageUrl:"Settings/images/StudentRegistration.png", enabled:true, servername:"redirect.via.cas=",rolesAllowed:"public"},
    
    
    
    "LOGGED12" : { name: "Emergency",description:"Emergency Contacts Update", id:"EmergencyContact", imageUrl:"Settings/images/EmergencyContact.png", enabled:true, servername:"redirect.via.cas=",rolesAllowed:"public"},
    
    "LOGGED13" : { name: "Address",description: "Address Update", id:"Address", imageUrl:"Settings/images/AddressUpdate.png", enabled:true, servername:"redirect.via.cas=",rolesAllowed:"public"},
    
    "LOGGED14" : { name: "Directory",description:"Directory", id:"Directory", imageUrl:"Settings/images/directory.png", enabled:true, servername:"redirect.via.cas=http://www.enmu.edu/mobile/enmu-directory.xml", roles:"", rolesAllowed:"Public"},
    
    // "LOGGED26" : { name: "Courses",description: "Courses", id:"Courses", imageUrl:"Settings/images/Course.png", enabled:true, servername:"redirect.via.cas=http://www.simons-rock.edu/np/cg/courses.xml",roles:"",rolesAllowed:"Public" },
    
    "LOGGED23" : { name: "news",description:"News", id:"News", imageUrl:"Settings/images/news.png", enabled:true, servername:"redirect.via.cas=http://enmuweb.wordpress.com/feed/", roles:"", rolesAllowed:"Public"},
    
    "LOGGED15" : { name: "Events",description:"Events", id:"Events", imageUrl:"Settings/images/event-calendar.png", enabled:true, servername:"redirect.via.cas=https://www.google.com/calendar/feeds/4cj37to01kguk75cjso6rr3opg%40group.calendar.google.com/public/basic", roles:"", rolesAllowed:"Public"},
    
    
    "LOGGED16" : { name: "Maps",description:"Map", id:"Maps", imageUrl:"Settings/images/map.png", enabled:true, servername:"redirect.via.cas=https://educause2010.campuseai.org", roles:"", rolesAllowed:"Public"},
    
    "LOGGED22" : { name: "facebook",description:"Facebook", id:"facebook", imageUrl:"Settings/images/facebook.png", enabled:true, servername:"redirect.via.cas=http://www.facebook.com/pages/Portales-NM/Eastern-New-Mexico-University/20827430409", rolesAllowed:"Public"},
    
    "LOGGED17" : { name: "twitter",description:"Twitter", id:"Twitter", imageUrl:"Settings/images/Twitter.png", enabled:true, servername:"redirect.via.cas=http://www.twitter.com/enmu",roles:"", rolesAllowed:"Public"},
    
    "LOGGED18" : { name: "Video",description:"Videos", id:"Videos", imageUrl:"Settings/images/Video.png", enabled:true, servername:"redirect.via.cas=http://www.youtube.com/enmu", roles:"", rolesAllowed:"Public"},
    
    "LOGGED24" : { name: "athletic", description: "Athletics", id:"athletic", imageUrl:"Settings/images/Athletics.png", enabled:true, servername:"redirect.via.cas=http://www.goeasternathletics.com/mobile/",roles:"", rolesAllowed:"Public"},
    
    "LOGGED19" : { name: "emergency",description:"Emergency", id:"Emergency", imageUrl:"Settings/images/emergency.png", enabled:true, servername:"redirect.via.cas=http://www.youtube.com/LipscombUniversity", roles:"", rolesAllowed:"Public"},
    
    //"LOGGED22" : { name: "impemergency",description:"Important Numbers", id:"ImpEmergency", imageUrl:"Settings/images/ImportantNumbers.png", enabled:true, servername:"redirect.via.cas=", roles:"", rolesAllowed:"Public"},
    
    "LOGGED20" : { name: "photos",description:"Photos", id:"flickr", imageUrl:"Settings/images/pictures.png", enabled:true, servername:"redirect.via.cas=http://www.youtube.com/LipscombUniversity", roles:"", rolesAllowed:"Public"},
    
    "LOGGED21" : { name: "library",description:"Library", id:"library", imageUrl:"Settings/images/library.png", enabled:true, servername:"redirect.via.cas=http://www.enmu.edu/library-mobile", roles:"", rolesAllowed:"Public"},
    
    // "LOGGED25" : { name: "donate",description:"Donate", id:"donate", imageUrl:"Settings/images/Donate.png", enabled:true, servername:"redirect.via.cas=https://www.applyweb.com/public/contribute?rockcont", roles:"", rolesAllowed:"Public"},
    
};

/**
 * <generateServiceTicket>
 * <generates service ticket>
 * @param {string} _userNameEnc contains encrypted username.
 * @param {string} _passwordEnc contains encrypted password.
 * @param {string} service_Request contains url of the requested service.
 * @param {string} _return_Where name of the function to call after success.
 * @param {string} _silent controls whether to show error msg or not.
 * @return {string}
 */
function generateST(_userNameEnc, _passwordEnc,service_Request,_return_Where,_silent)
{
    service_Request = serviceName + service_Request;
    console.log("generating service ticket for="+service_Request);
    var return_Where = (_return_Where || false);
    tgtTicket=window.sessionStorage.getItem("LS_tgtTicket");
    if (tgtTicket != null)
    {
        url_server = serverName+'/cas/v1/tickets'+'/'+tgtTicket;
        
        var xhr = $.ajax(
                         {
                         url:url_server,
                         type:"POST",
                         async:true,
                         cache:false,
                         data:{service:service_Request,math:Math.random()},
                         beforeSend:function (){
                         setTimeout(function()
                                    {
                                    xhr.abort();
                                    },globalAjaxTimer);
                         },
                         
                         success:function(data)
                         {
                         console.log("D:Service Ticket "+data);
                         var _serviceTicket = data;
                         if(return_Where != false)
                         {
                         return_Where(_serviceTicket, _silent,service_Request)
                         window.localStorage.setItem("LS_serviceTicket",_serviceTicket);
                         }
                         },
                         error:function(response)
                         {
                         // alert(response);
                         console.log("D: Inside Error of generateST "+response.status+"  "+response.statusText);
                         generateTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent);
                         }
                         });
    }
    else
    {
        generateTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent);
    }
    
    
}



function generateMsgST(_userNameEnc, _passwordEnc,service_Request,_return_Where,_silent)
{
    
    service_Request = serviceNameMsg+service_Request;
    //service_Request = service_Request;
    console.log("msgTGT="+window.sessionStorage.getItem("LS_MsgtgtTicket"));
    var return_Where = (_return_Where || false);
    MsgtgtTicket=window.sessionStorage.getItem("LS_MsgtgtTicket");
    if (MsgtgtTicket != null)
    {
        url_server = serverNameMsg+'/cas/v1/tickets'+'/'+MsgtgtTicket;
        
        var xhr = $.ajax(
                         {
                         url:url_server,
                         type:"POST",
                         async:true,
                         data:{service:service_Request,math:Math.random()},
                         beforeSend:function (){
                         setTimeout(function()
                                    {
                                    xhr.abort();
                                    },globalAjaxTimer);
                         },
                         
                         success:function(data)
                         {
                         console.log("MSg="+data);
                         var _serviceTicket = data;
                         if(return_Where != false)
                         {
                         return_Where(_serviceTicket, _silent,service_Request)
                         window.localStorage.setItem("LS_serviceTicket",_serviceTicket);
                         }
                         },
                         error:function(response)
                         {
                         generateMsgTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent);
                         }
                         });
    }
    else
    {
        generateMsgTGT(_userNameEnc,_passwordEnc,service_Request,return_Where,_silent);
    }
    
    
}

/**
 * <LoggedinHomePageChange>
 * <Controlles the onclick event of the icons>
 * @param {string} _servername name of the server.
 * @param {string} _id contains id of the requested app.
 */

var msgClick=false;
function LoggedinHomePageChange(_servername,_id)
{
    try{ if(taphold == false)
    {
        var indexPos=_servername.indexOf("redirect.via.cas");
        var url_service=_servername.substr(indexPos+17);
        
        if(_id == "Emergency")
        {
            displayEmergencyPage(_servername);
        }
        else{
            
            var networkState = navigator.connection.type;
            if(networkState !="none" && networkState !="unknown")
                
                
            {
                switch(_id)
                {
                        
                    case "ImpEmergency":{
                        displayImpEmergencyPage(url_service);
                        break;}
                    case "Holds":{
                        url_service="/enmu/services/student/bannerhold";
                        return_Where=BannerHold;
                        request_ST(url_service,return_Where,false);
                        $("#HoldsInfo").html("");
                        break;}
                    case "MidtermGrades":{
                        $("#MidtrmGrdshow").html("");
                        // alert("here");
                        url_service="/enmu/services/student/bannermidtermgrade";
                        return_Where=bannerMidtermGrade;
                        request_ST(url_service,return_Where,false);
                        break;}
                    case "FinalGrades":{
                        $("#FinalGRDshow").html("");
                        url_service="/enmu/services/student/bannerfinalgradetermdropdown";
                        return_Where=BannerTermDownFinalGrades;
                        //return_Where=bannerFinalGrade;
                        request_ST(url_service,return_Where,false);
                        break;}
                    case "LookUpClasses":{
                        $("#LUCdrop").show();
                        $("#lookupclassdisp").html("");
                        url_service="/enmu/services/student/bannertermsForClassLookup";
                        return_Where=LUClas;
                        request_ST(url_service,return_Where,false);
                        
                        break;}
                    case "registration":{
                        startActivitySpinner();
                        // $("#LUCdrop").show();
                        $("#registrationInfo").html("");
                        url_service="/enmu/services/student/bannertermsForClassLookup";
                        return_Where=registrationClasDropDown;
                        request_ST(url_service,return_Where,false);
                        $("#displayRegFinal").html("");
                        $("#displayRegFinalInfo").html("");
                        break;
                    }
                    case "AccountSummary":{
                        startActivitySpinner();
                        // $("#LUCdrop").show();
                        $("#accountInfo").html("");
                        url_service="/enmu/services/student/bannertermswithaccountsummary";
                        return_Where=AccountSummaryTermDown;
                        request_ST(url_service,return_Where,false);
                        $("#accountInfo").html("");
                        break;
                    }
                    case "WAG":{
                        startActivitySpinner();
                        $("#nextButton").show();
                        $("#backButton").show();
                        url_service="/enmu/services/student/bannertermsStudentHasClasses";
                        return_Where=weekAtGlanceTermDown;
                        request_ST(url_service,return_Where,false);
                        break;}
                    case "transcript":{
                        startActivitySpinner();
                        $("#bfreatdrop_selectAca").hide();
                        url_service="/enmu/services/student/bannertranscriptdropdown";
                        return_Where=AcademicTrans;
                        request_ST(url_service,return_Where,false);
                        $("#AcadefinaShowS").html("");
                        break;}
                    case "StudentRegistration":{
                        startActivitySpinner();
                        $("#studentRegistrationInfo").html("");
                        url_service="/enmu/services/student/bannertermsForRegistrationLookup";
                        return_Where=studentRegistration;
                        request_ST(url_service,return_Where,false);
                        break;}
                        
                    case "ClassroomAvailability":{
                        startActivitySpinner();
                        url_service="/enmu/services/student/bannertermsForRegistrationLookup";
                        return_Where=getStudentTermCode;
                        request_ST(url_service,return_Where,false);
                        break;} //return_Where=getClassroomAvailDataTicket;
                        
                    case "Address":{
                        startActivitySpinner();
                        url_service = "/enmu/services/student/banneraddresscodes";
                        return_Where = getAddressContactCodes;
                        request_ST(url_service,return_Where,false);
                        break;}
                        
                    case "EmergencyContact":{
                        startActivitySpinner();
                        startActivitySpinner();
                        url_service = "/enmu/services/student/banneremergencycontactcodes";
                        return_Where = getEmergencyContactCodes;
                        request_ST(url_service,return_Where,false);
                        break;
                        
                    }
                    case "21":{
                        startActivitySpinner();
                        msgClick=true;
                        myMsgs=new Array();
                        window.sessionStorage.setItem("_checkMsgCount",false);
                        url_service="/Alerts/AlertsService/AlertsInfoService/inbox";
                        return_Where=getAlertsJSON;
                        request_MsgST(url_service,return_Where,false);
                        break;
                    }
                    case "23":{
                        startActivitySpinner();
                        url_service="/Alerts/AlertsService/SendersLookup/groups";
                        return_Where=getCommunities;
                        request_MsgST(url_service,return_Where,false);
                        break;
                    }
                    case "Courses":{
                        CoursesList(url_service);
                        $("#coursesmiddlebox").show();
                        $("#courses").hide();
                        break;
                    }
                    case "Events":{
                        getCalender(url_service);
                        break;
                    }
                    case "News":{
                        GetNewsList(url_service);
                        break;
                    }
                    case "Maps":{
                        mapinitialize();
                        break;
                    }
                    case "Videos":{
                        //  alert(url_service);
                        startActivitySpinner();
                        youtubeInit();
                        break;
                    }
                    case "athletic":{
                        openWebpage(url_service);
                        break;
                    }
                        
                    case "library":{
                        openWebpage(url_service);
                        break;
                    }
                    case "Emergency":{
                        displayEmergencyPage(_servername);
                        break;
                    }
                    case "facebook":{
                        //Facebook.init();
                        openWebpage(url_service);
                        
                        break;
                    }
                    case "catalog":{
                        openWebpage(url_service);
                        break;
                    }
                    case "Twitter":{
                        openWebpage(url_service);
                        break;}
                    case "Directory":{
                        $("#searchdiectory").val();
                        collegeDirectory(url_service);
                        $("#directorymiddlebox").show();
                        $("#directory").hide();
                        break;
                    }
                        
                    case "flickr":{
                        getAlbums();
                        break;
                    }
                    case "donate":{
                        openWebpage(url_service);
                    }
                    default :{
                        //pageChange("demoPage");
                    }
                        
                }
            }
            else
            {
                navigator.notification.alert("Oops! You are not connected to Internet. Please check your settings and try again",doNothing,'No Network','Ok');
                
            }
        }
    }
    }catch(e){}
}




/**
 *<IndexPageChanges>
 *<contols all the changes that has to be made in the index page>
 */

function IndexPageChanges()
{
    $(".ClgName").html(universityName);
    $("#forgotPassClick").live("click", function(){
                               var url="https://my.enmu.edu/web/mycampus/password-reset";
                               openWebpage(url);
                               });
    $("#versionNumber").html(appVersion);
    
}


var getFormattedTime = function (fourDigitTime){
    var hours24 = parseInt(fourDigitTime.substring(0,2));
    var hours = ((hours24 + 11) % 12) + 1;
    var amPm = hours24 > 11 ? 'pm': 'am';
    var minutes = fourDigitTime.substring(2);
    
    return hours + ':' + minutes + amPm;
};